﻿using UnityEngine;
using System.Collections;

public class VisitorController : MonoBehaviour {
    public enum VisitorType{
        KROSH,
        SOVUNYA,
        LOSYASH,
        STEPANIDA,
        IGOGOSHA,
        KOPATICH
    }

    public enum VisitorSex{
        FEMALE,
        MALE
    }

    [SerializeField] private VisitorType visitorType;
    [SerializeField] private VisitorSex visitorSex;
    [SerializeField] private WishController.WishType[] availableDesire;
    [SerializeField] private WishController.WishType[] availableDesireRightRoom;
    [SerializeField] private WishController.WishType[] availableDesireLeftRoom;
    [SerializeField] private WishController.WishType[] availableDesireTopRoom;
    private int pointPath;
    private Vector3[] pathMotionTargets;
    private bool pathMotion;
    private bool move;
    private Vector3 target;
    private float speed = 5f;
    private float dinamicSpeed = 0f;
    private WishController wishController;
    private static bool leftRoomOccupied;
    private static bool rightRoomOccupied;
    private static bool topRoomOccupied;
    private bool service;
    private Vector3[] leftRoomTarget = {new Vector3(-23f, -4.2f, 0f),new Vector3(-23f, -2.0f, 0f)};
    private Vector3[] rightRoomTarget = {new Vector3(15.3f, -4.2f, 0f), new Vector3(15.3f, -2.5f, 0f)};
    private Vector3[] topRoomTarget = { new Vector3(-3.8f, 6.5f, 0f), new Vector3(-3.8f, 8.3f, 0f) };
    private Vector3 escapePosition = new Vector3(0f,-1f,0f);
    private CharacterAnimationController animatorController;
    private bool locatedInRightRoom;
    private bool locatedInLeftRoom;
    private bool locatedInTopRoom;
    private bool escape;
    private bool vibroChair;
    private SpriteRenderer[] spr;

    private void ChangeSortingLayerName(string layerName) {
        int childCount = transform.childCount;
        for (int i = 0; i < childCount; i++) {
            Transform childTransform = transform.GetChild(i);
            SpriteRenderer childSpriteRenderer = childTransform.gameObject.GetComponent<SpriteRenderer>();
            if (childSpriteRenderer != null) {
                childSpriteRenderer.sortingLayerName = layerName;
            }
            int childCount_1 = childTransform.childCount;
            if (childCount_1 != 0) {
                for (int j = 0; j < childCount_1; j++) {
                    Transform childTransform_1 = childTransform.GetChild(j);
                    SpriteRenderer childSpriteRenderer_1 = childTransform_1.gameObject.GetComponent<SpriteRenderer>();
                    if (childSpriteRenderer_1 != null) {
                        childSpriteRenderer_1.sortingLayerName = layerName;
                    }
                    int childCount_2 = childTransform_1.childCount;
                    if (childCount_2 != 0){
                        for (int k = 0; k < childCount_2; k++){
                            Transform childTransform_2 = childTransform_1.GetChild(k);
                            SpriteRenderer childSpriteRenderer_2 = childTransform_2.gameObject.GetComponent<SpriteRenderer>();
                            if (childSpriteRenderer_2 != null) {
                                transform.GetChild(i).GetChild(j).GetChild(k).gameObject.GetComponent<SpriteRenderer>().sortingLayerName = layerName;
                            }
                        }
                    }
                }
            }
        }
    }

    public WishController.WishType[] GetavailableDesire() {
        return this.availableDesire;
    }

    public WishController.WishType[] GetavailableDesireRightRoom() {
        return this.availableDesireRightRoom;
    }

    public WishController.WishType[] GetavailableDesireLeftRoom() {
        return this.availableDesireLeftRoom;
    }

    public WishController.WishType[] GetavailableDesireTopRoom() {
        return this.availableDesireTopRoom;
    }
    private void Start() {
        this.vibroChair = false;
        this.dinamicSpeed = this.speed;
        this.pathMotion = false;
        this.wishCloudShowed = false;
        this.locatedInLeftRoom = false;
        this.locatedInRightRoom = false;
        this.service = false;
        this.wishController = GetComponent<WishController>();
        this.animatorController = GetComponent<CharacterAnimationController>();
        this.escape = false;
    }

    private void OnDisable() {
        CancelInvoke("WishCloud");
    }

    private void OnEnable() {
        if ((this.transform.position.Equals(leftRoomTarget[this.leftRoomTarget.Length-1]) || this.transform.parent != null) ){
            Vector3 pos = SystemInfo.deviceModel.Contains("iPad") ? new Vector3(-18f, -4f, 0f) : new Vector3(-17f, -4f, 0f);
            this.AfterInteraction(pos,true);
        }

        if (this.transform.position.Equals(rightRoomTarget[this.rightRoomTarget.Length-1])) {
            Vector3 pos = SystemInfo.deviceModel.Contains("iPad") ? new Vector3(10f, -4f, 0f) : new Vector3(9.3f, -3.7f, 0f);

            this.AfterInteraction(pos);
        }

        if (this.transform.position.Equals(topRoomTarget[this.topRoomTarget.Length-1])) {
            Vector3 pos = SystemInfo.deviceModel.Contains("iPad") ? new Vector3(-8f, 6.5f, 0f) : new Vector3(-10f, 6.5f, 0f);
            this.AfterInteraction(pos);
        }
    }

    private void OnDestroy() {
        if (this.locatedInLeftRoom) {
            leftRoomOccupied = false;
            TurnController.instance.GetLeftRoom().busy = false;
            ChangingRoom.instance.SwipeToRight();
        }
        if (this.locatedInRightRoom) {
            rightRoomOccupied = false;
            TurnController.instance.GetRightRoom().busy = false;
            ChangingRoom.instance.SwipeToLeft();
        }
        if (this.locatedInTopRoom) {
            topRoomOccupied = false;
            TurnController.instance.GetTopRoom().busy = false;
            ChangingRoom.instance.SwipeToDown();
        }
        InteractionManager.instance.SwipeManagerActiveSelf(true);
        TurnController.instance.RemoveVisitorTypeList(this.GetVisitorType());
    }

    private float timeWait = 0;
   private void Update() {
        if (this.escape && this.transform.position.Equals(this.escapePosition)) {
            Destroy(this.gameObject);
        }
        if (this.transform.position.Equals(this.target) && this.move && !this.pathMotion) {
            this.move = false;
            ChangeSortingLayerName("Character");
            if (this.animatorController != null) {
                this.animatorController.PlayWait();
                this.timeWait = Time.time;
            }
            if (!this.wishCloudShowed) {
                Invoke("WishCloud", 2f);
            }
        }
       if (!this.move && !this.vibroChair && !this.escape && !timeWait.Equals(0) && (timeWait + 5f < Time.time)) {
           this.timeWait = Time.time;
           Wait();
       }
       if (this.pathMotionTargets !=null && this.transform.position.Equals(this.pathMotionTargets[this.pointPath]) && this.move && this.pathMotion)
        {
            if (this.pointPath < (this.pathMotionTargets.Length-1)) {
                this.pointPath++;
                this.target = this.pathMotionTargets[this.pointPath];
            }
            else {
                this.pathMotion = false;
            }
        }

        if (this.move) {
            this.transform.position = Vector3.MoveTowards(this.transform.position, this.target,this.dinamicSpeed*Time.deltaTime);
        }
    }

    public void MoveTo(Vector3 target,float speed = 0f) {
        if (speed != 0) {
            this.dinamicSpeed = speed;
        }
        if (this.animatorController != null) {
            this.animatorController.PlayWalk();
        }
        this.pathMotion = false;
        this.target = target;
        this.move = true;
        ChangeSortingLayerName("Character_1");
    }

    public void MoveTo(Vector3[] targets) {
        if (this.animatorController != null) {
            this.animatorController.PlayWalk();
        }
        this.dinamicSpeed = this.speed;
        this.pointPath = 0;
        this.pathMotion = true;
        this.pathMotionTargets = targets;
        this.target = targets[this.pointPath];
        this.move = true;
        ChangeSortingLayerName("Character_1");
    }

    public bool IfMoving() {
        return this.move;
    }

    private Vector3 placePosition;
    private void OnMouseDown() {
        if (this.move || this.escape || InteractionManager.instance.InteractionActivated()) return;
        // *************************
        if (this.service) {
            InteractionManager.instance.ActivateInteraction(this);
            return;
        }
        // *************************

        Vector3[] target = new Vector3[0];
        this.placePosition = transform.position;
        if (wishController.GetWishType().Equals(WishController.WishType.CLEAN_FACE) ||
            wishController.GetWishType().Equals(WishController.WishType.HAIR_PULLED)){
            if (!leftRoomOccupied) {
                target = this.leftRoomTarget;
                ChangingRoom.instance.SwipeToLeft();
                leftRoomOccupied = true;
                locatedInLeftRoom = true;
                if (!TurnController.instance.GetLeftRoom().busy) {
                    TurnController.instance.GetLeftRoom().busy = true;
                }
                TurnController.instance.SetVisitorTypeInLeftRoom(this.GetVisitorType());
                this.transform.position = new Vector3(-11f,-4.2f,0f);
            }
            else {
                TurnController.instance.ShowIcon(TurnController.instance.GetVisitorTypeInLeftRoom(),false);
                Debug.Log("Комната занята");
                return;
            }
        }
        else if (wishController.GetWishType().Equals(WishController.WishType.SHARPEN_NAILS) || 
            wishController.GetWishType().Equals(WishController.WishType.LIPSTICK)){
            if (!rightRoomOccupied) {
                target = this.rightRoomTarget;
                ChangingRoom.instance.SwipeToRight();
                rightRoomOccupied = true;
                this.locatedInRightRoom = true;
                if (!TurnController.instance.GetRightRoom().busy) {
                    TurnController.instance.GetRightRoom().busy = true;
                }
                TurnController.instance.SetVisitorTypeInRightRoom(this.GetVisitorType());
                this.transform.position = new Vector3(3f, -4.2f, 0f);
            }
            else {
                TurnController.instance.ShowIcon(TurnController.instance.GetVisitorTypeInRightRoom(), true);
                Debug.Log("Комната занята");
                return;
            }
        }
        else if(wishController.GetWishType().Equals(WishController.WishType.PARFUM_DRESS_UP) ||
            wishController.GetWishType().Equals(WishController.WishType.PARFUM_ROUGE) || 
            wishController.GetWishType().Equals(WishController.WishType.MASK)) {
            if (!topRoomOccupied) {
                target = this.topRoomTarget;
                ChangingRoom.instance.SwipeToTop();
                topRoomOccupied = true;
                this.locatedInTopRoom = true;
                if (!TurnController.instance.GetTopRoom().busy) {
                    TurnController.instance.GetTopRoom().busy = true;
                }
                TurnController.instance.SetVisitorTypeInTopRoom(this.GetVisitorType());
                this.transform.position = new Vector3(-15.3f, 6.5f, 0f);
            }
            else {
                TurnController.instance.ShowIcon(TurnController.instance.GetVisitorTypeInTopRoom(), false,true);
                Debug.Log("Комната занята");
                return;
            }
        }

        TurnController.instance.RemoveVisitorList(this.gameObject);
        TurnController.instance.FreeSpace(this.placePosition,GetWishController().GetWishType());
        this.service = true;
        MoveTo(target);
    }

    private void AfterInteraction(Vector3 position,bool islocal = false) {
        if (islocal) {
            this.transform.parent = null;
            this.transform.localPosition = position;
        }
        else {
            this.transform.position = position;
        }

        if (this.animatorController != null){
            this.animatorController.PlayGreet();
        }
        Invoke("Escape", 1.5f);
        this.escape = true;
    }

    public VisitorType GetVisitorType() {
        return this.visitorType;
    }

    public WishController GetWishController() {
        return this.wishController;
    }

    public VisitorSex GetVisitorSex() {
        return this.visitorSex;
    }

    private void Escape() {
        Destroy(this.gameObject);
        //MoveTo(this.escapePosition,this.speed*1f);
    }

    private bool wishCloudShowed;
    public void WishCloud() {
        if (this.escape) return;
        StartCoroutine(this.wishController.WishCloudIenumerator(3.5f,this.GetWishController().GetWishType(),this));
        this.wishCloudShowed = true;
    }

    private void Wait() {
        this.animatorController.PlayBlink();
        int i = Random.Range(1, 3);
        StartCoroutine(this.PlayRandomWaitAnimation(i));
    }

    private IEnumerator PlayRandomWaitAnimation(float delay) {
        yield return new WaitForSeconds(delay);
        if(this.move) yield break;
        int rnd = Random.Range(0, 2);
        switch (rnd) {
            case 0:
                this.animatorController.PlayOtherWait();
            break;
            case 1:
                this.animatorController.PlayWait();
            break;
            default:
            break;
        }
    }

    public void SetVibroChair(bool set) {
        this.vibroChair = set;
    }
}
