﻿using UnityEngine;
using System.Collections;

public class MaskBubbleController : MonoBehaviour {
    private Vector3 scaleTarget = new Vector3(0.2f,0.2f,0.2f);
    private Transform childTransform;
    private float speed = 0.5f;
	void Start () {
	    this.childTransform = this.transform.GetChild(0);
        this.childTransform.localScale = Vector3.zero;
        if (ParfumRoomController.instance.GetVisitor().GetComponent<VisitorsTypes>().GetVisitorType()
	        .Equals(VisitorController.VisitorType.STEPANIDA)) {
            this.speed *= 2;
	        this.scaleTarget *= 2f;
	    }
	}
	
	void Update () {
	    if (this.childTransform.localScale != scaleTarget) {
	        this.childTransform.localScale = Vector3.MoveTowards(this.childTransform.localScale, this.scaleTarget, this.speed*Time.deltaTime);
	    }
	    if (this.childTransform.localScale == Vector3.zero) {
            Destroy(this.gameObject);
        }
	}

    private void OnTriggerEnter(Collider col) {
        if (col.GetComponent<MaskFacialTissueController>() != null) {
            this.scaleTarget = Vector3.zero;
        }
    }

    private void OnDestroy() {
        ParfumRoomController.instance.RemoveBubble(this.gameObject);
    }
}
