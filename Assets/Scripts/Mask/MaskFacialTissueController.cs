﻿using UnityEngine;
using System.Collections;

public class MaskFacialTissueController : MonoBehaviour {
    public delegate void MaskFacialTissueEvents();

    public event MaskFacialTissueEvents RemoveTheMask;
    private bool pressed;
    private bool removeTheMask;
    private bool firstTap;
    private bool move;
    private Vector3 target;
    public static MaskFacialTissueController instance;
    private Vector3 endPosition = new Vector3(17f,0f,0f);
    private void OnEnable() {
        this.pressed = false;
        this.removeTheMask = false;
        this.firstTap = false;
        if (instance == null) {
            instance = this;
        }
        this.transform.localPosition = this.endPosition;
    }

    private void OnTriggerEnter(Collider col) {
//        if (col.transform.gameObject.GetComponent<MaskBubbleController>() != null) {
//            Destroy(col.gameObject);
//            ParfumRoomController.instance.RemoveBubble(col.gameObject);
//        }
    }

    private void Update() {
        if (this.pressed && !this.move) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f)) {
                transform.position = new Vector3(hit.point.x, hit.point.y, -3f);
            }
        }else {
	        if (!firstTap) return;

            if(!this.removeTheMask){
                if (SystemInfo.deviceModel.Contains("iPad")){
                    MoveTo(new Vector3(5f, -3.5f, -3f));
                }else{
                    MoveTo(new Vector3(7f, 0f, -3f));
                }
	        }
	    }
        
        if (this.firstTap && ParfumRoomController.instance.GetMaskBubbleList().Count == 0 && !this.removeTheMask) {
            if (this.RemoveTheMask != null) {
                this.RemoveTheMask();
            }
            this.removeTheMask = true;
        }

        if (this.move) {
            if (this.transform.localPosition != this.target) {
                this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                50f * Time.deltaTime);
            }
            else {
                this.move = false;
            }
        }
    }

    private void OnMouseDown() {
        if (!this.firstTap) {
            ResetParent();
            this.firstTap = true;
        }
        this.pressed = true;
    }

    private void OnMouseUp() {
        this.pressed = false;
    }

    private void ResetParent(){
        if (this.transform.parent.Equals(ParfumRoomController.instance.gameObject.transform)){
            return;
        }
        this.transform.SetParent(ParfumRoomController.instance.gameObject.transform);
    }

    public void MoveTo(Vector3 v) {
        this.pressed = false;
        this.move = true;
        this.target = v;
    }

    public void MoveToEndPosition() {
        this.MoveTo(this.endPosition);
    }
}
