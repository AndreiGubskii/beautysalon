﻿using UnityEngine;
using System.Collections;

public class MaskController : MonoBehaviour {
    private Animator _animator;
    private Vector3 saveFingerPosition = Vector3.zero;
    private void Start() {
        this._animator = this.GetComponent<Animator>();
        if (this._animator != null) {
            this._animator.speed = 0f;
        }
    }

    private void OnMouseDown() {
        StopAllCoroutines();
        if (!InteractionManager.instance.GetInteractionWas()) {
            InteractionManager.instance.SetInteractionWas(true);
        }
    }

    private void OnMouseDrag() {
        if (Input.mousePosition != this.saveFingerPosition) {
            this._animator.speed = 1f;
            this.saveFingerPosition = Input.mousePosition;
        }
        else {
            this._animator.speed = 0f;
        }
    }

    private void OnMouseUp() {
        this._animator.speed = 0f;
        AnimationEndListener();
    }

    private void AnimationEndListener() {
        StartCoroutine(this.AnimationEndListenerIenumerator(1f));
    }

    private IEnumerator AnimationEndListenerIenumerator(float delay) {
        yield return new WaitForSeconds(delay);
        ChangingRoom.instance.SetTarget(new Vector3(-4, 10.5f, -10));
    }
}
