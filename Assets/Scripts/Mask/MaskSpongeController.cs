﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class MaskSpongeController : MonoBehaviour {
    public delegate void MaskSpongeEvents();

    public event MaskSpongeEvents ApplyMask;
    public event MaskSpongeEvents DrawMask;
    public event MaskSpongeEvents StopDrawMask;
    private bool pressed;
    private bool move;
    private Vector3 target;
    private float lastBubbleTime;
    [SerializeField] private GameObject maskBubble;
    private bool makeBubble;
    private bool firstTap;
    private bool applyMask;
    private Vector3 endPosition = new Vector3(17f,0f,0f);

	void OnEnable () {
	    this.firstTap = false;
	    this.makeBubble = true;
	    this.applyMask = false;
	    this.pressed = false;
	    this.transform.localPosition = this.endPosition;
	}
	
	void Update () {
	    if (this.pressed && !this.move) {
	        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	        RaycastHit hit;
	        if (Physics.Raycast(ray, out hit, 100f)) {
	            transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
	            //if (!SystemInfo.deviceModel.Contains("iPad") && transform.position.x > -7.5f && transform.position.x < -0.7f &&
	            //    transform.position.y > 8f && transform.position.y < 13f) return;
	            if (hit.transform.gameObject.GetComponent<VisitorsTypes>() != null && this.makeBubble &&
	                ParfumRoomController.instance.GetMaskBubbleList().Count <
	                ParfumRoomController.instance.GetBubbleCount()) {
	                if (this.lastBubbleTime + 0.1f > Time.time) return;
	                this.lastBubbleTime = Time.time;
	                GameObject bub = (GameObject) Instantiate(this.maskBubble, transform.position, Quaternion.identity);
	                bub.transform.SetParent(ParfumRoomController.instance.GetVisitor().transform.GetChild(0).GetChild(0));
	                bub.transform.localScale = new Vector3(1f, 1f, 1f);
	                ParfumRoomController.instance.AddBubble(bub);
	                if (this.DrawMask != null) {
	                    this.DrawMask();
	                }
	            }
	            
	            if (!this.applyMask && ParfumRoomController.instance.GetMaskBubbleList().Count 
                    >= ParfumRoomController.instance.GetBubbleCount()) {
	                if (this.ApplyMask != null) {
	                    this.ApplyMask();
	                }
	                this.applyMask = true;
	            }
            }
	    }
	    else {
	        if (!firstTap) return;
	        if (ParfumRoomController.instance.GetMaskBubbleList().Count > (ParfumRoomController.instance.GetBubbleCount()/2) && !this.applyMask) {
	            if (this.ApplyMask != null) {
	                this.ApplyMask();
	            }
	            this.applyMask = true;
	        }
	        else if(!this.applyMask){
	            if (SystemInfo.deviceModel.Contains("iPad")) {
	                MoveTo(new Vector3(5f, -3.5f, -3f));
                }
	            else {
	                MoveTo(new Vector3(7f, 0f, -3f));
                }
	            
	            if (this.StopDrawMask != null) {
	                this.StopDrawMask();
	            }
	        }
	    }

        if (this.move) {
            if (this.transform.localPosition != this.target) {
                this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                50f * Time.deltaTime);
            }
            else {
                this.move = false;
            }
        }
	}

    private void OnMouseDown() {
        if (!this.firstTap) {
            this.ResetParent();
            this.firstTap = true;
        }
        
        this.pressed = true;
    }

    private void OnMouseUp() {
        this.pressed = false;
    }

    private void ResetParent(){
        if (this.transform.parent.Equals(ParfumRoomController.instance.gameObject.transform)){
            return;
        }
        this.transform.SetParent(ParfumRoomController.instance.gameObject.transform);
    }

    public void MoveTo(Vector3 v) {
        this.move = true;
        this.target = v;
        this.pressed = false;
    }

    public void MoveToEndPosition() {
        this.MoveTo(this.endPosition);
    }

    private void OnTriggerStay(Collider col) {
        if (col.transform.gameObject.GetComponent<MaskBubbleController>() == null){
            this.makeBubble = true;
        }
        else {
            this.makeBubble = false;
        }
    }
}
