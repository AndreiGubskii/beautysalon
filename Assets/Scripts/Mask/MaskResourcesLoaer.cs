﻿using UnityEngine;
using System.Collections;

public class MaskResourcesLoaer : MonoBehaviour {
    private SpriteRenderer sprite;
    private GameObject mask;
    private void OnEnable() {
        sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = Resources.Load<Sprite>("Art/ParfumRoom/Mska_back");
        mask = InstanceResources("Maska", new Vector3(-3.3f, -1.8f, 0f), new Vector3(0.9f, 0.9f, 0.9f));
    }

    private void OnBecameInvisible() {
        sprite.sprite = null;
        Destroy(mask);
        Resources.UnloadUnusedAssets();
    }

    private GameObject InstanceResources(string name, Vector3 position , Vector3 scale){
        GameObject obj = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/ParfumRoom/" + name));
        obj.transform.SetParent(this.transform);
        obj.transform.localScale = scale;
        obj.transform.localPosition = position;
        return obj;
    }
}
