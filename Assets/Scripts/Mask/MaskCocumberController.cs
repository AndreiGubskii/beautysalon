﻿using UnityEngine;
using System.Collections;

public class MaskCocumberController : MonoBehaviour {
    public enum Cocumber {
        LEFT,
        RIGHT
    }
    private bool scale;
    private Vector3 scaleTarget;
    private bool move;
    private Vector3 moveTarget;
    private float moveSpeed = 20f;
    private bool firstTap;
    private bool pressed;

    [SerializeField] private Vector3 cocumberTarget;
    [SerializeField] private Cocumber cocumberType;
	void Start () {
	    this.firstTap = false;
	}
	

	void Update () {
	    if (this.pressed && !this.move) {
	        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	        RaycastHit hit;
	        if (Physics.Raycast(ray, out hit, 100f)) {
	            transform.position = new Vector3(hit.point.x, hit.point.y, -3f);
	        }
	    }
	    if (this.move) {
            if (this.transform.localPosition != this.moveTarget) {
                this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition,
                    this.moveTarget, this.moveSpeed*Time.deltaTime);
            }
            else {
                this.move = false;
                if (this.cocumberType == Cocumber.LEFT){
                    ParfumRoomController.instance.LeftCocumberInPlace();
                }
                else if (this.cocumberType == Cocumber.RIGHT){
                    ParfumRoomController.instance.RightCocumberInPlace();
                }
            }
        }

        if (this.scale) {
            if (this.transform.localScale != this.scaleTarget) {
                this.transform.localScale = Vector3.MoveTowards(this.transform.localScale,
                    this.scaleTarget, 2f*Time.deltaTime);
            }
            else {
                this.scale = false;
                if (this.transform.localScale == Vector3.zero) {
                    Destroy(this.gameObject);
                }
            }
        }
	}
    private void OnTriggerEnter(Collider col) {
        if (col.GetComponent<InteractionCharacterController>() != null) {
            this.pressed = false;
            transform.SetParent(col.transform.GetChild(0).GetChild(0));
            if (col.GetComponent<VisitorsTypes>().GetVisitorType() == VisitorController.VisitorType.STEPANIDA) {
                if (this.cocumberType == Cocumber.LEFT) {
                    MoveTo(new Vector3(-1.5f,5f,-3f));
                }
                else if (this.cocumberType == Cocumber.RIGHT) {
                    MoveTo(new Vector3(1.5f,5f,-3f));
                }
                
                ScaleTo(new Vector3(0.9f, 0.9f, 1f));
            }
            else {
                MoveTo(this.cocumberTarget);
                ScaleTo(new Vector3(0.4f, 0.4f, 1f));
            }
            
        }

        if (col.GetComponent<MaskFacialTissueController>() != null) {
            ScaleTo(new Vector3(0f, 0f, 0f));
        }
    }

    private void MoveTo(Vector3 target) {
        this.moveTarget = target;
        this.move = true;
    }

    private void ScaleTo(Vector3 target) {
        this.scaleTarget = target;
        this.scale = true;
    }

    private void OnMouseDown() {
        if (!this.firstTap) {
            this.ResetParent();
            this.firstTap = true;
        }
        
        this.pressed = true;
    }

    private void OnMouseUp() {
        this.pressed = false;
    }

    private void ResetParent(){
        if (this.transform.parent.Equals(ParfumRoomController.instance.gameObject.transform)){
            return;
        }
        this.transform.SetParent(ParfumRoomController.instance.gameObject.transform);
    }
}
