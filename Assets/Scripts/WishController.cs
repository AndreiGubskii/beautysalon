﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class WishController : MonoBehaviour {
    public enum WishType : int {
        SHARPEN_NAILS,
        CLEAN_FACE,
        HAIR_PULLED,
        SHOES_GAME,
        LIPSTICK,
        PARFUM_DRESS_UP,
        PARFUM_ROUGE,
        MASK,
        DEFAULT
    }

    [SerializeField]private WishType wishType;
    [SerializeField]private GameObject wishCloud;
    private Transform cloud;
    private Animator _animatorWishCloud;

    private void Awake() {
        
    }

    private void Start() {
        this.wishCloud = Instantiate(Resources.Load<GameObject>("Prefabs/WishCloud"));
        this.wishCloud.transform.SetParent(this.transform);
        this.wishCloud.transform.localPosition = new Vector3(0.9f,4f,0f);
        this.wishCloud.transform.localScale = Vector3.one;
        this.cloud = this.wishCloud.GetComponent<WishCloudController>().GetCloud().transform;
        if (this.wishCloud != null) {
            this._animatorWishCloud = this.wishCloud.GetComponent<Animator>();
        }
        this.SetWishType();
    }


    private bool chooseRoom = false;
    public void SetWishType() {
    VisitorController visitorController = GetComponent<VisitorController>();
        if (!TurnController.instance.GetRightRoom().busy && !this.chooseRoom) {
            WishType[] availableDesireRightRoom = visitorController.GetavailableDesireRightRoom();
            this.chooseRoom = true;
            wishType = availableDesireRightRoom[Random.Range(0, availableDesireRightRoom.Length)];
            TurnController.instance.GetRightRoom().busy = true;
        } else if (!TurnController.instance.GetLeftRoom().busy && !this.chooseRoom) {
            WishType[] availableDesireLeftRoom = visitorController.GetavailableDesireLeftRoom();
            this.chooseRoom = true;
            wishType = availableDesireLeftRoom[Random.Range(0, availableDesireLeftRoom.Length)];
            TurnController.instance.GetLeftRoom().busy = true;
        } else if (!TurnController.instance.GetTopRoom().busy && !this.chooseRoom) {
            WishType[] availableDesireTopRoom = visitorController.GetavailableDesireTopRoom();
            this.chooseRoom = true;
            wishType = availableDesireTopRoom[Random.Range(0, availableDesireTopRoom.Length)];
            TurnController.instance.GetTopRoom().busy = true;
        } else {
            WishType[] availableDesire = visitorController.GetavailableDesire();
            wishType = availableDesire[Random.Range(0, availableDesire.Length)];
            this.chooseRoom = false;
        }

        if (visitorController.GetVisitorType()!=VisitorController.VisitorType.KROSH && (TurnController.instance.GetWishTypeInDictionary(visitorController.GetVisitorType()) == wishType 
            || TurnController.instance.IsActiveWishType(wishType) || TurnController.instance.IsActiveRoomWish(
            TurnController.instance.GetActiveRoomByWish(wishType)) || TurnController.instance.GetPreviousWish() == wishType)) {
                this.SetWishType();
                return;
        }

        if (wishType.Equals(WishType.MASK) && visitorController != null &&
            (visitorController.GetVisitorSex().Equals(VisitorController.VisitorSex.MALE))) {
            this.SetWishType();
            return;
        }

        TurnController.instance.AddVisitorWishesDictionary(visitorController.GetVisitorType(),wishType);
        TurnController.instance.SetPreviousWish(wishType);
        Debug.Log("I want " + wishType);
    }

    public WishType GetWishType() {
        return this.wishType;
    }

    public void ShowWishCloud() {
        if (this._animatorWishCloud != null) {
            this._animatorWishCloud.SetTrigger("show");
        }
    }

    public void HideWishCloud() {
        if (this._animatorWishCloud != null) {
            this._animatorWishCloud.SetTrigger("hide");
        }
    }

    
    public IEnumerator WishCloudIenumerator(float delay,WishType type,VisitorController visitor) {
        PasteSpriteInCloud(type,visitor);
        this.ShowWishCloud();
        yield return new WaitForSeconds(delay);
        this.HideWishCloud();
        yield return new WaitForSeconds(delay);
        GetComponentInParent<VisitorController>().WishCloud();//Рекурсия
    }

    private void PasteSpriteInCloud(WishType type,VisitorController visitor) {
        
        switch (type) {
            case WishType.CLEAN_FACE:
                
                this.cloud.GetComponent<SpriteRenderer>().sprite = 
                    Settings.instance.GetWishCloudSprite().cleanFace.GetComponent<SpriteRenderer>().sprite;
                break;
            case WishType.SHARPEN_NAILS:
                if (visitor.GetVisitorSex().Equals(VisitorController.VisitorSex.MALE)) {
                    this.cloud.GetComponent<SpriteRenderer>().sprite =
                        Settings.instance.GetWishCloudSprite().sharpenNailsMale.GetComponent<SpriteRenderer>().sprite;
                }
                else {
                    this.cloud.GetComponent<SpriteRenderer>().sprite =
                    Settings.instance.GetWishCloudSprite().sharpenNailsFamale.GetComponent<SpriteRenderer>().sprite;
                }

                break;
                case WishType.HAIR_PULLED:
                    this.cloud.GetComponent<SpriteRenderer>().sprite =
                    Settings.instance.GetWishCloudSprite().nose.GetComponent<SpriteRenderer>().sprite;
                break;

                case WishType.LIPSTICK:
                    this.cloud.GetComponent<SpriteRenderer>().sprite =
                    Settings.instance.GetWishCloudSprite().lipstick.GetComponent<SpriteRenderer>().sprite;
                break;
                case WishType.PARFUM_DRESS_UP:
                    this.cloud.GetComponent<SpriteRenderer>().sprite =
                    Settings.instance.GetWishCloudSprite().parfume.GetComponent<SpriteRenderer>().sprite;
                break;

                case WishType.PARFUM_ROUGE:
                    this.cloud.GetComponent<SpriteRenderer>().sprite =
                    Settings.instance.GetWishCloudSprite().parfume.GetComponent<SpriteRenderer>().sprite;
                break;

                case WishType.MASK:
                    this.cloud.GetComponent<SpriteRenderer>().sprite =
                    Settings.instance.GetWishCloudSprite().mask.GetComponent<SpriteRenderer>().sprite;
                break;

                default:
                break;
        }
    }
}