﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IpadSettings : MonoBehaviour {
    [SerializeField] private Vector3 position;
    [SerializeField] private Vector3 rotation;
    [SerializeField] private Vector3 scale = new Vector3(1f,1f,1f);
    
    [SerializeField] private bool changeSize;
    [SerializeField] private Vector2 _size;
    private void OnEnable() {
        if (SystemInfo.deviceModel.Contains("iPad")) {
            
            RectTransform rectTransform = GetComponent<RectTransform>();
            if (rectTransform != null) {
                rectTransform.anchoredPosition3D = position;
                rectTransform.rotation = Quaternion.Euler(rotation);
                rectTransform.localScale = scale;
                if (changeSize) {
                    rectTransform.sizeDelta = _size;
                }
            }
            else {
                Transform transform = GetComponent<Transform>();
                transform.localPosition = position;
                transform.rotation = Quaternion.Euler(rotation);
                transform.localScale = scale;
            }
            

        }
    }
}
