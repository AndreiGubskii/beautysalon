﻿using UnityEngine;
using System.Collections;

public class InteractionManager : MonoBehaviour {
    public enum InteractionName {
        DEFAULT,
        NAILS_STICKER
    }
    [System.Serializable]
    public class Rooms {
        [SerializeField] private GameObject leftRoom;
        [SerializeField] private GameObject rightRoom;
        [SerializeField] private GameObject topRoom;

        public void ActivateRightRoom() {
            if (!this.rightRoom.activeSelf) {
                this.rightRoom.SetActive(true);
            }
        }
        public void DeactivateRightRoom() {
            if (this.rightRoom.activeSelf) {
                this.rightRoom.SetActive(false);
            }
        }

        public void ActivateLeftRoom() {
            if (!this.leftRoom.activeSelf) {
                this.leftRoom.SetActive(true);
            }
        }
        public void DeactivateLeftRoom() {
            if (this.leftRoom.activeSelf) {
                this.leftRoom.SetActive(false);
            }
        }

        public void ActivateTopRoom() {
            if (!this.topRoom.activeSelf) {
                this.topRoom.SetActive(true);
            }
        }
        public void DeactivateTopRoom() {
            if (this.topRoom.activeSelf) {
                this.topRoom.SetActive(false);
            }
        }
    }
    public static InteractionManager instance;
    [SerializeField] private GameObject bathroom;
    [SerializeField] private GameObject manicureroom;
    [SerializeField] private GameObject hairPulled;
    [SerializeField] private GameObject shoes;
    [SerializeField] private GameObject lipstick;
    [SerializeField] private GameObject parfum;
    [SerializeField] private GameObject mask;
    [SerializeField] private GameObject[] visitors;
    [SerializeField] private GameObject[] visitorsInteractionPulledNouse;
    [SerializeField] private Rooms rooms;
    [SerializeField] private GameObject completionInteractionPrefab;
    private VisitorController visitorController;
    private GameObject obj;
    private bool interactionActivated;
    private SwipeManager swipeManager;
    private GameObject visitorGameObject;
    private GameObject visitorInteraction;
    private bool interactionWas;
    private float delayBeforeDeactivateInteraction = 4f;
    private int interactionMelodyPrev = 0;
    private string[] interactionSounds = new[] {"interaction_1", "interaction_2", "interaction_3"};
    private void Awake() {
        instance = this;
    }

    private void Start() {
        this.swipeManager = SwipeManager.instance;
    }

    public void ActivateInteraction(VisitorController v) {
        //Проигрываем случайную музыку при запуске взаимодействия
        int interactionMelody = Random.Range(0, interactionSounds.Length);
        while (interactionMelodyPrev == interactionMelody) {
            interactionMelody = Random.Range(0, interactionSounds.Length);
        }
        interactionMelodyPrev = interactionMelody;
        AudioManager.instance.PlayMusic(interactionSounds[interactionMelody]);

        this.SetInteractionWas(false);
        this.visitorController = v;
        visitorGameObject = v.gameObject;
        visitorGameObject.SetActive(false);
        this.SetInteractionActivated(true);
        this.SwipeManagerActiveSelf(false);
        CanvasController.instance.ExitInteractionBtnsActiveSelf(true);
        switch (v.GetWishController().GetWishType()) {
            case WishController.WishType.CLEAN_FACE:
                this.bathroom.SetActive(true);
                this.rooms.DeactivateLeftRoom();
                this.obj = (GameObject)Instantiate(GetVisitorByType(v.GetVisitorType()), new Vector3(-18f, -1f, 0f), Quaternion.identity);

                if (SystemInfo.deviceModel.Contains("iPad")){
                    if (v.GetVisitorType() == VisitorController.VisitorType.STEPANIDA || v.GetVisitorType() == VisitorController.VisitorType.KROSH)
                    {
                        obj.transform.localScale = new Vector3(0.8f, 0.8f, 0f);
                    }
                    else{
                        obj.transform.localScale = new Vector3(1.8f, 1.8f, 0f);
                    }
                    obj.transform.localPosition = new Vector3(-19.5f, -2.4f, 0);
                }

                BathRoomController.instance.ActivateCleaningFace(obj);
            break;

            case WishController.WishType.SHARPEN_NAILS:
                this.manicureroom.SetActive(true);
                this.rooms.DeactivateRightRoom();
                ManicureroomController.instance.ActivateService(v);
            break;

            case WishController.WishType.HAIR_PULLED:
                this.hairPulled.SetActive(true);
                this.rooms.DeactivateLeftRoom();
                this.obj = (GameObject)Instantiate(GetVisitorByTypeInHairPulledInteraction(v.GetVisitorType()), 
                    new Vector3(-23f, 0.3f, 0f), Quaternion.identity);
                HairPulledController.instance.ActivateHairPulled(v);
            break;

            case WishController.WishType.LIPSTICK:
                this.lipstick.SetActive(true);
                this.rooms.DeactivateRightRoom();
                LipsInteractionController.instance.Activate(v);
            break;

            case WishController.WishType.PARFUM_DRESS_UP:
                this.parfum.SetActive(true);
                this.rooms.DeactivateTopRoom();
                ParfumRoomController.instance.ActivateParfumeDressUP(v.GetVisitorSex(),0.5f);
                this.obj = (GameObject)Instantiate(GetVisitorByType(v.GetVisitorType()), new Vector3(0.8f, 8.7f, 0f), Quaternion.identity);
                if (SystemInfo.deviceModel.Contains("iPad")) {
                    if (v.GetVisitorType() == VisitorController.VisitorType.STEPANIDA) {
                        obj.transform.localScale = new Vector3(0.8f, 0.8f, 0f);
                    }
                    else {
                        obj.transform.localScale = new Vector3(2f, 2f, 0f);
                    }
                    obj.transform.localPosition = new Vector3(-0.5f, 8.25f, 0);
                }
                
                ParfumRoomController.instance.SetVisitor(obj);
            break;

            case WishController.WishType.PARFUM_ROUGE:
                this.parfum.SetActive(true);
                this.rooms.DeactivateTopRoom();
                this.obj = (GameObject)Instantiate(GetVisitorByType(v.GetVisitorType()), new Vector3(0.8f, 8.7f, 0f), Quaternion.identity);
                ParfumRoomController.instance.ActivateParfumRouge(obj,v.GetVisitorType(),v.GetVisitorSex(), 0.5f);
                if (SystemInfo.deviceModel.Contains("iPad")) {
                    if (v.GetVisitorType() == VisitorController.VisitorType.STEPANIDA) {
                        obj.transform.localScale = new Vector3(0.8f, 0.8f, 0f);
                    }
                    else {
                        obj.transform.localScale = new Vector3(1.8f, 1.8f, 0f);
                    }
                    obj.transform.localPosition = new Vector3(-0.5f, 7.9f, 0);
                }

                ParfumRoomController.instance.SetVisitor(obj);
            break;

            case WishController.WishType.MASK:
                this.mask.SetActive(true);
                this.parfum.SetActive(true);
                this.rooms.DeactivateTopRoom();
                ChangingRoom.instance.SetTarget(new Vector3(-23.2f, 10.5f, -10f), true);
                this.obj = (GameObject)Instantiate(GetVisitorByType(v.GetVisitorType()), new Vector3(0.8f, 8.7f, 0f), Quaternion.identity);
                ParfumRoomController.instance.ActivateMask();

                if (SystemInfo.deviceModel.Contains("iPad")) {
                    if (v.GetVisitorType() == VisitorController.VisitorType.STEPANIDA) {
                        obj.transform.localScale = new Vector3(0.8f, 0.8f, 0f);
                    }
                    else {
                        obj.transform.localScale = new Vector3(1.8f, 1.8f, 0f);
                    }
                    obj.transform.localPosition = new Vector3(-0.5f, 7.9f, 0);
                }

                ParfumRoomController.instance.SetVisitor(obj);
            break;
        }
        if (this.obj != null) {
            this.SetVisitorInteraction(obj);
        }
    }

    public void ActivateShoesGame() {
        this.SetInteractionWas(false);
        this.SetInteractionActivated(true);
        this.SwipeManagerActiveSelf(false);
        this.shoes.SetActive(true);
        ChangingRoom.instance.SetPosition(new Vector3(-4f,-11f,-10f));
        if (SystemInfo.deviceModel.Contains("iPad")) {
            ChangingRoom.instance.SetPosition(new Vector3(-4f, -4.9f, -10f));
        }
        CanvasController.instance.ExitShoesInteractionBtnsActiveSelf(true);
    }

    public void DeactivateShoesGame() {
        StartCoroutine(this.DeactivateShoesGameIenumerator());
    }

    private IEnumerator DeactivateShoesGameIenumerator() {
        float delay = 0.2f;//this.delayBeforeDeactivateInteraction / 2f;
        if (InteractionManager.instance.GetInteractionWas()) {
            this.completionInteractionPrefab.SetActive(true);
            AudioManager.instance.PlaySound("win");
            delay = AudioManager.instance.GetSoundByName("win").length;
        }

        yield return new WaitForSeconds(delay);
        ChangingRoom.instance.SetStartPosition();
        this.completionInteractionPrefab.SetActive(false);
        CanvasController.instance.ExitShoesInteractionBtnsActiveSelf(false);
        this.SetInteractionActivated(false);
        this.SwipeManagerActiveSelf(true);
        this.shoes.SetActive(false);
    }

    private bool deactivateInteraction;
    public void DeactivateInteraction() {
        if(deactivateInteraction) return;
        StartCoroutine(DeactivateInteractionIEnumerator());
    }

    private IEnumerator DeactivateInteractionIEnumerator() {
        this.deactivateInteraction = true;
        float delay = 0.2f;//this.delayBeforeDeactivateInteraction / 2f;
        if (this.GetInteractionWas()) {
            this.completionInteractionPrefab.SetActive(true);
        
            if (this.GetVisitorInteraction()!=null && this.GetVisitorInteraction().GetComponent<InteractionCharacterAnimationController>() != null) {
                this.GetVisitorInteraction().GetComponentInChildren<InteractionCharacterAnimationController>().PlayHappy();
            }
            AudioManager.instance.PlaySound("win");
            delay = AudioManager.instance.GetSoundByName("win").length; //this.delayBeforeDeactivateInteraction;
        }
        
        if (this.visitorController.GetWishController().GetWishType().Equals(WishController.WishType.LIPSTICK)) {
            LipsInteractionController.instance.GetLips().PlayAnimation();
        }
        
        yield return new WaitForSeconds(delay);
        this.completionInteractionPrefab.SetActive(false);
        this.SetInteractionActivated(false);
        visitorGameObject.SetActive(true);
        if (this.obj != null) {
            Destroy(this.obj);
        }
        switch (this.visitorController.GetWishController().GetWishType()) {
            case WishController.WishType.CLEAN_FACE:
                this.bathroom.SetActive(false);
                this.rooms.ActivateLeftRoom();
            break;
            case WishController.WishType.SHARPEN_NAILS:
                this.manicureroom.SetActive(false);
                this.rooms.ActivateRightRoom();
            break;
            case WishController.WishType.HAIR_PULLED:
                this.hairPulled.SetActive(false);
                this.rooms.ActivateLeftRoom();
            break;

            case WishController.WishType.LIPSTICK:
                this.lipstick.SetActive(false);
                this.rooms.ActivateRightRoom();
            break;

            case WishController.WishType.PARFUM_DRESS_UP:
                this.parfum.SetActive(false);
                this.rooms.ActivateTopRoom();
            break;

            case WishController.WishType.PARFUM_ROUGE:
                this.parfum.SetActive(false);
                this.rooms.ActivateTopRoom();
            break;
            case WishController.WishType.MASK:
                this.mask.SetActive(false);
                this.parfum.SetActive(false);
                this.rooms.ActivateTopRoom();
                ChangingRoom.instance.SetTarget(new Vector3(-4f,10.5f,-10f),true);
            break;
        }
        CanvasController.instance.ExitInteractionBtnsActiveSelf(false);
        this.deactivateInteraction = false;
        AudioManager.instance.PlayMusic("gamePlay");
        AdsManager.instance.ShowAds();
        Resources.UnloadUnusedAssets();
    }

    private GameObject GetVisitorByType(VisitorController.VisitorType type) {
        foreach (GameObject visitor in visitors) {
            if (visitor.GetComponent<VisitorsTypes>().GetVisitorType().Equals(type)) {
                return visitor;
            }
        }
        return null;
    }

    private GameObject GetVisitorByTypeInHairPulledInteraction(VisitorController.VisitorType type) {
        foreach (GameObject visitor in visitorsInteractionPulledNouse) {
            if (visitor.GetComponent<VisitorsTypes>().GetVisitorType().Equals(type)) {
                return visitor;
            }
        }
        return visitorsInteractionPulledNouse[0];
    }

    public bool InteractionActivated() {
        return this.interactionActivated;
    }

    private void SetInteractionActivated(bool b) {
        this.interactionActivated = b;
    }

    public void SwipeManagerActiveSelf(bool active) {
        if (this.swipeManager != null) {
            this.swipeManager.enabled = active;
        }
    }

    public void ActivateNextInteraction(InteractionName interaction, float delay = 1f){
        switch (interaction) {
            case InteractionName.NAILS_STICKER:
                NailPolishController.instance.Deactivate();
                ManicureroomController.instance.ActivateInteractionWithDelay("ActivateNailSticker",delay);
            break;
            default:
            break;
        }
    }

    public GameObject GetCompletionInteractionPrefab() {
        return this.completionInteractionPrefab;
    }
    
    private void SetVisitorInteraction(GameObject visitor) {
        this.visitorInteraction = visitor;
    }

    public GameObject GetVisitorInteraction() {
        return this.visitorInteraction;
    }

    public void SetInteractionWas(bool b) {
        this.interactionWas = b;
    }

    public bool GetInteractionWas() {
        return this.interactionWas;
    }
}
