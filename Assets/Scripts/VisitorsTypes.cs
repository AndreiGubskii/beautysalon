﻿using UnityEngine;
using System.Collections;

public class VisitorsTypes : MonoBehaviour {
    [SerializeField]private VisitorController.VisitorType type;
    [SerializeField]private GameObject wetPrefab;

    public GameObject GetWetPrefab() {
        return this.wetPrefab;
    }

    public VisitorController.VisitorType GetVisitorType() {
        return this.type;
    }
}
