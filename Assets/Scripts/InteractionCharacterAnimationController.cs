﻿using UnityEngine;
using System.Collections;

public class InteractionCharacterAnimationController : MonoBehaviour {
    private Animator _animator;
    private int hashBlink;
    private int hashScratch;
    private int hashSad;
    private int hashHappy;
    private int hashHappyWashSoap;
    private int hashHappyWash;
    private int hashContinueHappyWashSoap;
    private int hashHappyWashEnd;
    private int hashHappySoapEnd;
    private void Awake() {
        this.hashBlink = Animator.StringToHash("blink");
        this.hashScratch = Animator.StringToHash("scratch");
        this.hashSad = Animator.StringToHash("sad");
        this.hashHappy = Animator.StringToHash("happy");
        this.hashHappyWashSoap = Animator.StringToHash("happyWashSoap");
        this.hashHappyWash = Animator.StringToHash("happyWash");
        this.hashHappyWashEnd = Animator.StringToHash("happyWashEnd");
        this.hashHappySoapEnd = Animator.StringToHash("happySoapEnd");
        this.hashContinueHappyWashSoap = Animator.StringToHash("happyWashContinue");
        this._animator = GetComponentInChildren<Animator>();
    }

    private void OnEnable() {
        //InvokeRepeating("PlayBlink",2f,7f);
    }

    private void OnDisable() {
        //CancelInvoke("PlayBlink");
    }

    public void PlayHappy() {
        if (_animator != null) {
            if (this._animator.speed == 0) {
                this._animator.speed = 1;
            }
            this._animator.SetTrigger(this.hashHappy);
        }
    }

    public void PlaySad() {
        if (_animator != null) {
            this._animator.SetTrigger(this.hashSad);
        }
    }

    public void PlayScratch() {
        if (_animator != null) {
            this._animator.SetTrigger(this.hashScratch);
        }
    }

    public void PlayBlink() {
        if (_animator != null) {
            this._animator.SetTrigger(this.hashBlink);
        }
    }

    public void PlayHappyWash() {
        if (_animator != null) {
            this._animator.SetTrigger(this.hashHappyWash);
        }
    }

    public void PlayHappyWashSoap() {
        if (_animator != null) {
            this._animator.SetTrigger(this.hashHappyWashSoap);
        }
    }

    public void ContinueHappyWashSoap() {
        if (_animator != null) {
            this.Play();
            this._animator.SetTrigger(this.hashContinueHappyWashSoap);
        }
    }

    public void StopHappyWash() {
        if (_animator != null) {
            this._animator.SetTrigger(this.hashHappyWashEnd);
        }
    }

    public void StopHappySoap() {
        if (_animator != null) {
            this._animator.SetTrigger(this.hashHappySoapEnd);
        }
    }

    public void Pause() {
        _animator.speed = 0f;
    }

    public void Play() {
        _animator.speed = 1f;
    }

    public Animator GetAnimator() {
        return this._animator;
    }
}
