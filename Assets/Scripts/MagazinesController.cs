﻿using UnityEngine;
using System.Collections;

public class MagazinesController : MonoBehaviour {
    private Animator _animator;

    private void Start() {
        this._animator = GetComponent<Animator>();
    }

    private void OnMouseDown() {
        if (this._animator != null) {
            this._animator.SetTrigger("play");
        }
        AudioManager.instance.GetSoundSource().
            PlayOneShot(AudioManager.instance.GetSoundByName("click"));
    }
}
