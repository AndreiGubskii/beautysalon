﻿using UnityEngine;
using System.Collections;

public class BigFaceNoseAnimationController : MonoBehaviour {
    private Animator _animator;
    private int hashBlink;
    private int hashPain;
    private int hashPain2;
    private int hashPainEnd;
    private int hashDragged;
    private float blinkingTimer = 0f;
    private void Start() {
        this._animator = GetComponentInChildren<Animator>();
        this.hashBlink = Animator.StringToHash("blink");
        this.hashPain = Animator.StringToHash("pain");
        this.hashPainEnd = Animator.StringToHash("painEnd");
        this.hashDragged = Animator.StringToHash("dragged");
        this.hashPain2 = Animator.StringToHash("pain2");
    }

    private void Update() {
        if (this.blinkingTimer > 0) {
            this.blinkingTimer -= 1*Time.deltaTime;
        }
        if (this.blinkingTimer <= 0 ) {
            this.blinkingTimer = Random.Range(1f,5f);
            this.PlayBlink();
        }
    }

    public void PlayBlink() {
        this._animator.SetTrigger(this.hashBlink);
    }

    public void PlayPainRight() {
        this._animator.SetTrigger(this.hashPain);
    }

    public void PlayPainLeft() {
        this._animator.SetTrigger(this.hashPain2);
    }

    public void PlayPainEnd() {
        this._animator.SetTrigger(this.hashPainEnd);
    }

    public void PlayDragged() {
        this._animator.SetTrigger(this.hashDragged);
    }
}
