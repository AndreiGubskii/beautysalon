﻿using UnityEngine;
using System.Collections;

public class HairPulledController : MonoBehaviour {
    
    public static HairPulledController instance;
    private int pulledHairCounter;
    private bool endInteraction;
    [SerializeField] private Camera camera;
    private float defaultSizeCamera;

    private void OnEnable() {
        instance = this;
        this.endInteraction = false;
        this.pulledHairCounter = 0;
        this.defaultSizeCamera = this.camera.orthographicSize;
    }

    private void OnDisable() {
        this.UnloadResources();
        if (this.camera != null) {
            this.camera.orthographicSize = this.defaultSizeCamera;
        }
    }

    public void ActivateHairPulled(VisitorController visitor){
        this.LoadResources();
        if (visitor.GetVisitorType() == VisitorController.VisitorType.KOPATICH) {
            this.camera.orthographicSize = 3f;
        }else if (visitor.GetVisitorType() == VisitorController.VisitorType.LOSYASH) {
            this.camera.orthographicSize = 3.6f;
        }
        this.camera.transform.position = new Vector3(
            this.camera.transform.position.x,
            -0.5f,
            this.camera.transform.position.z);
    }

    private void Update() {
        if (this.GetPulledHairCount() >= 6 && !this.endInteraction) {
            InteractionManager.instance.DeactivateInteraction();
            this.endInteraction = true;
        }
    }

    public void PulledHairCounter() {
        this.pulledHairCounter++;
    }

    public int GetPulledHairCount() {
        return this.pulledHairCounter;
    }

    private void LoadResources() {
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Art/Bathroom/Background_bathroom");
    }

    private void UnloadResources() {
        GetComponent<SpriteRenderer>().sprite = null;
        Resources.UnloadUnusedAssets();
    }
}
