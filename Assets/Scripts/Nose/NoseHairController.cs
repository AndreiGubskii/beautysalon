﻿using UnityEngine;
using System.Collections;

public class NoseHairController : MonoBehaviour {
    public enum HairSide {
        LEFT,
        RIGHT
    }
    private float y = 0f;
    [SerializeField] private Vector3 targetPosition;
    [SerializeField] private HairSide hairSide;
    private bool pullOut;
    private bool throwOut;
    private BigFaceNoseAnimationController _animationController;
    void Start () {
        this.pullOut = false;
        this.throwOut = false;
        this._animationController = transform.parent.parent.parent.parent.GetComponent<BigFaceNoseAnimationController>();
    }
	

	void Update () {
	    if (this.transform.localPosition.Equals(this.targetPosition) && !this.pullOut) {
	        this.pullOut = true;
            this._animationController.PlayDragged();
            if (!InteractionManager.instance.GetInteractionWas()) {
                InteractionManager.instance.SetInteractionWas(true);
            }
	    }
	    if (this.transform.localPosition.y < -7) {
            HairPulledController.instance.PulledHairCounter();
	        Destroy(this.gameObject);
        }
	    if (Input.GetMouseButton(0) && this.pullOut && !this.throwOut) {
	        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	        RaycastHit hit;
	        if (Physics.Raycast(ray, out hit, 100f)) {
	            transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
	        }
	    }
	    else if(this.pullOut){
	        GetComponent<Rigidbody>().isKinematic = false;
	    }
	}

    private void OnMouseDown() {
        this.y = Input.mousePosition.y;
        switch (this.hairSide){
            case HairSide.RIGHT:
                this._animationController.PlayPainRight();
            break;
            case HairSide.LEFT:
                this._animationController.PlayPainLeft();
            break;
        }
    }

    private void OnMouseUp() {
        if (!this.pullOut) {
            this._animationController.PlayPainEnd();
        }
        else {
            this.throwOut = true;
        }
    }

    private void OnMouseDrag() {
        if (this.y > Input.mousePosition.y) {
            this.y = Input.mousePosition.y;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, this.targetPosition, 
                Time.deltaTime*6f);
        }
    }
}
