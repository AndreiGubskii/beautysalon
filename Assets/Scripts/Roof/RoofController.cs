﻿using UnityEngine;
using System.Collections;

public class RoofController : MonoBehaviour {
    [SerializeField] private SparrowAnimationController[] birds;
    [SerializeField] private SparrowBtnController button;
    [SerializeField] private float setInactivityTimer;
    private float inactivityTimer;
    private bool timer;

    private void Start () {
	    if (this.button != null) {
	        this.button.Click += SparrowBtnClickListener;
	    }
        this.inactivityTimer = this.setInactivityTimer;
        this.timer = false;
    }

    private void Update() {
        if (this.timer) {
            if (this.inactivityTimer > 0) {
                this.inactivityTimer -= 1*Time.deltaTime;
            }
            if (this.inactivityTimer <= 0) {
                this.timer = false;
                this.inactivityTimer = this.setInactivityTimer;
                if (ChangingRoom.instance.GetInRoom() == ChangingRoom.Rooms.ROOF) {
                    ChangingRoom.instance.SwipeToDown();
                }
            }
        }
    }

    private void OnDisable() {
        if (this.button != null) {
	        this.button.Click -= SparrowBtnClickListener;
	    }
    }

    private void BirdsFly() {
        foreach (SparrowAnimationController bird in birds) {
            bird.PlayFlyAnimation();
        }
    }

    private void SparrowBtnClickListener() {
        this.BirdsFly();
        this.timer = true;
    }
}
