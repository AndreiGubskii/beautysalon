﻿using UnityEngine;
using System.Collections;

public class SparrowAnimationController : MonoBehaviour {
    private Animator _animator;
    private float peckTimer;
    private float emptyTimer;
    private bool reset;
    private float timer;
    private float setTimer = 2f;
	void Start () {
	    this._animator = GetComponent<Animator>();
	    this.timer = this.setTimer;
	    this.emptyTimer = 2f;
	}

    private void Update() {
        if (this.reset && !ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.ROOF)){
            if (this.timer > 0) {
                this.timer -= 1*Time.deltaTime;
            }
            if(this.timer <= 0f) {
                this.ResetAnimation();
                this.reset = false;
                this.timer = this.setTimer;
                this.emptyTimer = 2f;
            }
            
        }
        
        if (!this.reset && this.emptyTimer > 0f) {
            this.emptyTimer-=1*Time.deltaTime;
            if (this.emptyTimer <= 0f) {
                this.emptyTimer = 0f;
                this.PlayPeckAnimation();
                this.peckTimer = Random.Range(1f, 7f);
            }
        }
        if (!this.reset && this.peckTimer > 0f) {
            this.peckTimer -= 1*Time.deltaTime;
            if (this.peckTimer <= 0f) {
                this.peckTimer = 0f;
                this.PlayEmptyAnimation();
                this.emptyTimer = Random.Range(1f, 3f);
            }
        }
    }

    public void PlayFlyAnimation() {
        if (this.reset) return;
        this.reset = true;
        this._animator.SetTrigger("fly");
    }

    public void PlayPeckAnimation() {
        this._animator.SetTrigger("peck");
    }

    public void PlayEmptyAnimation() {
        this._animator.SetTrigger("empty");
    }

    public void ResetAnimation() {
        this._animator.SetTrigger("reset");
    }
}
