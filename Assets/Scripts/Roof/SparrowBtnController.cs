﻿using UnityEngine;
using System.Collections;

public class SparrowBtnController : MonoBehaviour {
    public delegate void SparrowBtnEvents();

    public event SparrowBtnEvents Click;

    
    private void OnMouseDown() {
        if (this.Click != null) {
            this.Click();
        }
    }
}
