﻿using UnityEngine;
using System.Collections;

public class ParfumHandController : MonoBehaviour {
    private bool move;
    private Vector3 target;
    private Vector3 startPosition = new Vector3(17f,-3.5f,0);
    private Vector3 endPosition = new Vector3(8.8f, -3.5f, 0f);
    public static ParfumHandController instance;
    private Transform childObject;
    private GameObject perfume;
    private GameObject rougeBrush;
    private GameObject mask;
    private GameObject maskFacialTissue;
    [SerializeField] private GameObject cocumberLeft;
    [SerializeField] private GameObject cocumberRight;

    private void OnEnable() {
        if (instance == null) {
            instance = this;
        }
        this.transform.localPosition = this.startPosition;
        if (this.perfume == null) {
            this.perfume = ParfumRoomController.instance.GetPerfumeController().gameObject;
        }
        else {
            if (this.perfume.transform.parent == this.transform) {
                this.perfume.transform.SetParent(this.transform.parent);
            }
        }
        if (this.rougeBrush == null) {
            this.rougeBrush = ParfumRoomController.instance.GetRougeBrush().gameObject;

        }
        else {
            if (this.rougeBrush.transform.parent == this.transform) {
                this.rougeBrush.transform.SetParent(this.transform.parent);
            }
        }

        if (this.mask == null) {
            this.mask = ParfumRoomController.instance.GetMask().gameObject;
        }
        else {
            if (this.mask.transform.parent == this.transform) {
                this.mask.transform.SetParent(this.transform.parent);
            }
        }

        if (this.maskFacialTissue == null) {
            this.maskFacialTissue = ParfumRoomController.instance.GetMaskFacialTissue().gameObject;
        }
        else {
            if (this.maskFacialTissue.transform.parent == this.transform) {
                this.maskFacialTissue.transform.SetParent(this.transform.parent);
            }
        }

        if (SystemInfo.deviceModel.Contains("iPad")) {
            endPosition = new Vector3(7f, -3.5f, 0);
        }
    }

    private void OnDisable() {
        if (this.childObject != null) {
            this.childObject = null;
        }
    }

    public void BringPerfume() {
        if (!this.perfume.activeSelf) {
            this.perfume.SetActive(true);
        }
        this.perfume.transform.SetParent(this.transform);
        this.perfume.transform.localPosition = new Vector3(2.5f, 2.5f, 0f);
        MoveTo(this.endPosition);
        this.childObject = transform.Find("Perfume");
    }

    public void BringRougeBrush() {
        if (!this.rougeBrush.activeSelf) {
            this.rougeBrush.SetActive(true);
        }
        this.rougeBrush.transform.SetParent(this.transform);
        this.rougeBrush.transform.localPosition = new Vector3(1.3f, 2.5f, 0f);
        MoveTo(this.endPosition);
        this.childObject = transform.Find("RougeBrush");
    }

    public void BringMask() {
        if (!this.mask.activeSelf) {
            this.mask.SetActive(true);
        }
        this.mask.transform.SetParent(this.transform);
        this.mask.transform.localPosition = new Vector3(2.5f, 2.5f, 0f);
        MoveTo(this.endPosition);
        this.childObject = transform.Find("MaskSponge");
    }

    public void BringMaskFacialTissue() {
        if (!this.maskFacialTissue.activeSelf) {
            this.maskFacialTissue.SetActive(true);
        }
        this.maskFacialTissue.transform.SetParent(this.transform);
        this.maskFacialTissue.transform.localPosition = new Vector3(2.5f, 2.5f, 0f);
        MoveTo(this.endPosition);
        this.childObject = transform.Find("FacialTissue");
    }

    public void BringCocumberLeft() {
        if (this.cocumberLeft != null) {
            GameObject obj = Instantiate(this.cocumberLeft);
            obj.transform.SetParent(this.transform);
            obj.transform.localPosition = new Vector3(0.8f, 2.3f, 0f);
        }
        MoveTo(this.endPosition);
        this.childObject = transform.Find("cocumberLeft(Clone)");
    }

    public void BringCocumberRight() {
        if (this.cocumberRight != null) {
            GameObject obj = Instantiate(this.cocumberRight);
            obj.transform.SetParent(this.transform);
            obj.transform.localPosition = new Vector3(0.8f, 2.3f, 0f);
        }
        MoveTo(this.endPosition);
        this.childObject = transform.Find("cocumberRight(Clone)");
    }

    private void Update() {
        if (this.childObject != null && !this.childObject.parent.Equals(this.transform) && !this.target.Equals(startPosition)) {
            this.MoveToStartPosition();
        }
        if (this.move) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                20f*Time.deltaTime);
        }

        if (this.transform.localPosition.Equals(this.target)) {
            this.move = false;
        }
    }

    public void MoveToStartPosition() {
        this.target = this.startPosition;
        this.move = true;
    }

    public void MoveToEndPosition() {
        this.target = this.endPosition;
        this.move = true;
    }

    public void MoveTo(Vector3 v) {
        this.target = v;
        this.move = true;
    }

    public bool IsMoved() {
        return this.move;
    }
}
