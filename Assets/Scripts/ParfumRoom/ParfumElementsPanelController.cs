﻿using UnityEngine;
using System.Collections;

public class ParfumElementsPanelController : MonoBehaviour {
    private bool move;
    private Vector3 target;
    private float speed = 20f;
    private Vector3 startPosition = new Vector3(7.5f, 3.4f, 0f);
    private Vector3 endPosition = new Vector3(11f,3.4f,0f);

    private void OnEnable() {
        this.transform.localPosition = this.endPosition;
        if (Helper.GetAspectRatio(new Vector2(Screen.width, Screen.height)) == "16:10") {
            startPosition = new Vector3(6.9f, 3.4f, 0f);
            //transform.localScale = new Vector3(0.8f,0.8f,0.8f);
        }

        if (SystemInfo.deviceModel.Contains("iPad")) {
            startPosition = new Vector3(5.3f, 3.4f, 0f);
        }
    }

    private void Update() {
        if (this.move && this.transform.localPosition != this.target) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                this.speed * Time.deltaTime);
        }
        if (this.move && this.transform.localPosition.Equals(this.target)) {
            this.move = false;
            if (this.target.Equals(this.startPosition)) {
                CanvasController.instance.SetScaleTimer();
            }
        }
    }

    public void MoveTo(Vector3 target) {
        this.target = target;
        this.move = true;
    }

    public void MoveToStartPosition() {
        this.target = this.startPosition;
        this.move = true;
    }

    public void MoveToEndPosition() {
        this.target = this.endPosition;
        this.move = true;
    }
}
