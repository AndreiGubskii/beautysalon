﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ParfumElementController : MonoBehaviour {
    [System.Serializable]
    public class DuplicateTransform {
        [SerializeField] private bool activated = false;
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale = new Vector3(1, 1, 1);

        public bool IsActivate() {
            return this.activated;
        }
    }

    [System.Serializable]
    public class IndividualDuplicateTransform {
        [SerializeField] private DuplicateTransform kroshDuplicateTransform;
        [SerializeField] private DuplicateTransform stepanidaDuplicateTransform;
        [SerializeField] private DuplicateTransform losyashDuplicateTransform;
        [SerializeField] private DuplicateTransform sovunyaDuplicateTransform;
        [SerializeField] private DuplicateTransform kopatichDuplicateTransform;
        [SerializeField] private DuplicateTransform igogoshaDuplicateTransform;

        public DuplicateTransform GetKroshDuplicateTransform() {
            return this.kroshDuplicateTransform;
        }
        public DuplicateTransform GetStepanidaDuplicateTransform() {
            return this.stepanidaDuplicateTransform;
        }
        public DuplicateTransform GetLosyashDuplicateTransform() {
            return this.losyashDuplicateTransform;
        }
        public DuplicateTransform GetSovunyaDuplicateTransform() {
            return this.sovunyaDuplicateTransform;
        }
        public DuplicateTransform GetKopatichDuplicateTransform() {
            return this.kopatichDuplicateTransform;
        }
        public DuplicateTransform GetIgogoshaDuplicateTransform() {
            return this.igogoshaDuplicateTransform;
        }
    }
    private DuplicateTransform duplicateTransform;
    [SerializeField] private DuplicateTransform defaultDuplicateTransform;
    [SerializeField] private IndividualDuplicateTransform individualDuplicateTransform;
    [SerializeField] private Transform parent;

    private void OnMouseDown() {
        if (!this.GetComponent<ParfumElementController>().enabled) return;
        this.SetDuplicateTransform();
        GameObject obj = Instantiate(this.gameObject);
        obj.transform.SetParent(transform.parent);
        obj.transform.localPosition = this.gameObject.transform.localPosition;

        this.gameObject.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
        this.gameObject.AddComponent<ParfumeDuplicateElmentController>().
            SetDuplicateTransform(this.duplicateTransform);
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder+=10;
        this.GetComponent<ParfumElementController>().enabled = false;
    }

    private void SetDuplicateTransform() {
        switch (ParfumRoomController.instance.GetVisitorType()) {
            case VisitorController.VisitorType.IGOGOSHA:
                if (this.individualDuplicateTransform.GetIgogoshaDuplicateTransform().IsActivate()) {
                    this.duplicateTransform = this.individualDuplicateTransform.
                            GetIgogoshaDuplicateTransform();
                }else {
                    this.duplicateTransform = this.defaultDuplicateTransform;
                }
            break;
            case VisitorController.VisitorType.KOPATICH:
                if (this.individualDuplicateTransform.GetKopatichDuplicateTransform().IsActivate()) {
                    this.duplicateTransform = this.individualDuplicateTransform.
                        GetKopatichDuplicateTransform();
                }else {
                    this.duplicateTransform = this.defaultDuplicateTransform;
                }
            break;
            case VisitorController.VisitorType.KROSH:
                if (this.individualDuplicateTransform.GetKroshDuplicateTransform().IsActivate()) {
                    this.duplicateTransform = this.individualDuplicateTransform.
                        GetKroshDuplicateTransform();
                }else {
                    this.duplicateTransform = this.defaultDuplicateTransform;
                }
            break;
            case VisitorController.VisitorType.LOSYASH:
                if (this.individualDuplicateTransform.GetLosyashDuplicateTransform().IsActivate()) {
                    this.duplicateTransform = this.individualDuplicateTransform.
                        GetLosyashDuplicateTransform();
                }else {
                    this.duplicateTransform = this.defaultDuplicateTransform;
                }
            break;
            case VisitorController.VisitorType.SOVUNYA:
                if (this.individualDuplicateTransform.GetSovunyaDuplicateTransform().IsActivate()) {
                    this.duplicateTransform = this.individualDuplicateTransform.
                        GetSovunyaDuplicateTransform();
                }else {
                    this.duplicateTransform = this.defaultDuplicateTransform;
                }
            break;
            case VisitorController.VisitorType.STEPANIDA:
                if (this.individualDuplicateTransform.GetStepanidaDuplicateTransform().IsActivate()) {
                    this.duplicateTransform = this.individualDuplicateTransform.
                        GetStepanidaDuplicateTransform();
                }else {
                    this.duplicateTransform = this.defaultDuplicateTransform;
                }
            break;
            default:
            break;
        }
    }
}

