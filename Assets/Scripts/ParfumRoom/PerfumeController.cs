﻿using UnityEngine;
using System.Collections;

public class PerfumeController : MonoBehaviour {
    [System.Serializable]
    private class PerfumeTransform {
        public Vector3 position = new Vector3();
        public Vector3 rotate = new Vector3();
        public Vector3 scale = new Vector3();
    }

    public delegate void PerfumeEvents();

    public event PerfumeEvents Perfumed;
    public event PerfumeEvents DownOnPerfume;
    public event PerfumeEvents UpOnPerfume;
    private bool perfumed;
    private bool move;
    private int positionId;
    private Animator _animator;
    private Vector3 target;
    private float delayBeforeMove;
    private float timeBeforeMove;
    private Vector3 endPosition = new Vector3(17f, -2.5f, 0f);
    public static PerfumeController instance;
    private bool canClick;
    [SerializeField] private PerfumeTransform[] perfumeTransform;
    [SerializeField] private PerfumeTransform[] IPADperfumeTransform;
    [SerializeField] private GameObject blue;
    [SerializeField] private GameObject pink;
    [SerializeField] private float speed;
    private float delayBeforeChangePosition = 1f;
    private void OnEnable() {
        if (instance == null) {
            instance = this;
        }
        this.positionId = 0;
        this.perfumed = false;
        this.transform.localPosition = this.endPosition;
        this.canClick = true;
        if (SystemInfo.deviceModel.Contains("iPad")) {
            perfumeTransform = IPADperfumeTransform;
        }
    }

    private void Update() {
        if (this.move && (this.timeBeforeMove + this.delayBeforeMove) <= Time.time) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                this.speed*Time.deltaTime);
        }

        if (this.transform.localPosition.Equals(this.target) && this.move) {
            this.move = false;
            this.delayBeforeMove = 0f;
            this.canClick = true;
        }
        if (this.transform.localPosition.Equals(this.endPosition) && !move && this.perfumed) {
            if (this.Perfumed != null) {
                this.Perfumed();
            }
            this.perfumed = false;
        }
    }

    private void PlayAnimation() {
        if (this._animator != null) {
            this._animator.SetTrigger("play");
        }
        if (this.DownOnPerfume != null) {
            this.DownOnPerfume();
            StartCoroutine(this.UpOnPerfumeIenumerator(this.delayBeforeChangePosition));
        }
    }

    private IEnumerator UpOnPerfumeIenumerator(float delay) {
        yield return new WaitForSeconds(delay);
        if (this.UpOnPerfume != null) {
            this.UpOnPerfume();
        }
    }

    private void OnMouseDown() {
        if (this.move || !this.canClick) return;
        this.canClick = false;
        if (!InteractionManager.instance.GetInteractionWas()) {
            InteractionManager.instance.SetInteractionWas(true);
        }
        if (this.positionId.Equals(0)) {
            StartCoroutine(this.ChangeIEnumerator(this.positionId, 0.1f));
            ResetParent();
            this.positionId++;
            return;
        }

        if (this.positionId.Equals(this.perfumeTransform.Length)) {
            this.PlayAnimation();
            this.MoveTo(this.endPosition, this.delayBeforeChangePosition);
            this.perfumed = true;
            return;
        }

        if (this.transform.localPosition != this.perfumeTransform[this.positionId].position) {
            this.PlayAnimation();
            StartCoroutine(ChangeIEnumerator((this.positionId), this.delayBeforeChangePosition));
            this.positionId++;
        }
    }

    public void MoveTo(Vector3 v) {
        this.move = true;
        this.target = v;
    }

    public void MoveTo(Vector3 v, float delay){
        this.move = true;
        this.target = v;
        this.delayBeforeMove = delay;
        this.timeBeforeMove = Time.time;
    }

    private IEnumerator ChangeIEnumerator(int i,float delay) {
        yield return new WaitForSeconds(delay);
        if (i >= this.perfumeTransform.Length) yield break;
        if (this.perfumeTransform[i] != null) {
            MoveTo(this.perfumeTransform[i].position);
            this.transform.localRotation = Quaternion.Euler(this.perfumeTransform[i].rotate);
            this.transform.localScale = this.perfumeTransform[i].scale;
        }
    }

    private void ResetParent() {
        if (this.transform.parent.Equals(ParfumRoomController.instance.gameObject.transform)) {
            return;
        }
        this.transform.SetParent(ParfumRoomController.instance.gameObject.transform);
    }

    public void SetBlueOrPink(VisitorController.VisitorSex sex) {
        if (sex.Equals(VisitorController.VisitorSex.FEMALE)) {
            this.pink.SetActive(true);
            this.blue.SetActive(false);
            this.SetAnimator(this.pink);
        }
        else {
            this.pink.SetActive(false);
            this.blue.SetActive(true);
            this.SetAnimator(this.blue);
        }
    }

    private void SetAnimator(GameObject obj) {
        this._animator = obj.GetComponent<Animator>();
    }
}
