﻿using TouchScript.Gestures;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using TouchScript;

public class VerticalScrolling : MonoBehaviour {
    [System.Serializable]
    public class PanelBorder {
        public float top;
        public float bottom;
    }
    [SerializeField] private PanelBorder boreder;
    private Touch touch;
    private Vector2 scrollPosition;

    private void Start() {
        planeHit = new Plane(Vector3.forward, transform.position);
    }
    public float fResistanceFactor = 0.98f;
    public float fStopThreashold = 0.01f;

    private Plane planeHit;
    private Vector3 v3StartPos;
    private Vector3 v3LastPos;
    private Vector3 v3Delta;
    private float fStartTime;

    private bool bTranslating = false;

    private void OnMouseDown() {
        bTranslating = false;
        v3StartPos = new Vector3(this.transform.localPosition.x,GetHitPoint().y,this.transform.localPosition.z);
        v3LastPos = v3StartPos;
        fStartTime = Time.time;
    }

    private void OnMouseDrag() {
        Vector3 v3T = new Vector3(this.transform.localPosition.x, GetHitPoint().y, this.transform.localPosition.z);
        transform.Translate(v3T - v3LastPos);
        v3LastPos = v3T;            
    }

    private void OnMouseUp() {
        v3Delta = new Vector3(this.transform.localPosition.x, GetHitPoint().y, this.transform.localPosition.z);
        v3Delta = (v3Delta - v3StartPos)/(Time.time - fStartTime)*Time.deltaTime;
        bTranslating = true;
    }

    private Vector3 GetHitPoint() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float fDist;
        if (planeHit.Raycast(ray, out fDist))
            return ray.GetPoint(fDist);
        else
            return Vector3.zero;
    }

    private void Update() {
        float y = Mathf.Clamp(this.transform.localPosition.y, this.boreder.top, this.boreder.bottom);
        this.transform.localPosition = new Vector3(this.transform.localPosition.x, y, this.transform.localPosition.z);
        if (bTranslating) {
            transform.position += v3Delta;
            v3Delta = v3Delta*fResistanceFactor;
            if (v3Delta.magnitude < fStopThreashold)
                bTranslating = false;
        }
    }
}