﻿using UnityEngine;
using System.Collections;

public class ParfumRougeController : MonoBehaviour {
    private Color defaultColor;
    private SpriteRenderer spriteRenderer;
    private string colliderName;
    private Transform colliderTransform;
    private Vector3 savePosition;
    private bool isComplete;
    private bool isCompleteOnHalf;
    private void OnEnable() {
        this.isComplete = false;
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        if (this.spriteRenderer != null) {
            this.defaultColor = this.spriteRenderer.color;
            this.spriteRenderer.color = Color.clear;
        }
        if (this.GetComponent<Rigidbody>() == null) {
            this.gameObject.AddComponent<Rigidbody>().isKinematic = true;
        }
    }

    private void Update() {
        if (this.spriteRenderer.color.r > 0.8f && !this.isCompleteOnHalf) {
            ParfumRoomController.instance.RougeHalfCounter();
            this.isCompleteOnHalf = true;
        }

        if (this.spriteRenderer.color == defaultColor && !this.isComplete) {
            ParfumRoomController.instance.RougeCompleteCounter();
            this.isComplete = true;
        }
    }

    private void OnTriggerEnter(Collider col) {
        this.colliderName = col.name;
        this.colliderTransform = col.GetComponent<Transform>();
        if (this.colliderName.Equals("tipRougeBrush")) {
            if (!InteractionManager.instance.GetInteractionWas()) {
                InteractionManager.instance.SetInteractionWas(true);
            }
        }
    }

    private void OnTriggerStay(Collider col){
        if (!this.colliderName.Equals("tipRougeBrush")) return;
        if (!this.colliderTransform.position.Equals(this.savePosition)) {
            this.spriteRenderer.color = Color.Lerp(this.spriteRenderer.color, this.defaultColor, Time.deltaTime*10f);
            this.savePosition = this.colliderTransform.position;
        }
    }

    private void OnTriggerExit(Collider col) {
        
    }
}
