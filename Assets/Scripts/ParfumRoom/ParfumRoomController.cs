﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ParfumRoomController : MonoBehaviour {
    public enum ParfumRoomInteraction {
        PERFUME_ROUGE,
        PERFUME_DRESS_UP
    }

    public delegate void ParfumRoomEvents();
    public event ParfumRoomEvents ParfumRoomMaskDraw;
    public event ParfumRoomEvents ParfumRoomMaskDrawStop;
    public event ParfumRoomEvents ParfumeRoomMaskApply;

    public event ParfumRoomEvents ParfumRoomPowder;
    public event ParfumRoomEvents ParfumRoomPowderStop;
    public event ParfumRoomEvents ParfumeRoomPowdered;

    
    public static ParfumRoomController instance;
    private List<GameObject> maskBubbleList = new List<GameObject>(); 
    private GameObject hairdo;
    private GameObject glasses;
    private GameObject bow_tie;
    private GameObject hat;
    private GameObject earrings;
    private GameObject scarf;
    private GameObject visitor;
    private int rougeCompletedCounter;
    private int rougeHalfCounter;
    private int maskBubbleCount = 70;
    private ParfumRoomInteraction activeInteraction;
    private VisitorController.VisitorSex visitorSex;
    private InteractionCharacterAnimationController characterAnimationController;
    [SerializeField] private ParfumElementsPanelController elementsPanel;
    [SerializeField] private PerfumeController perfumeController;
    [SerializeField] private GameObject rouge;
    [SerializeField] private ParfumRougeBrushController rougeBrush;
    [SerializeField] private GameObject mask;
    [SerializeField] private GameObject maskFacialTissue;

    private void OnEnable() {
        if (instance == null) {
            instance = this;
        }
        this.LoadResources();
        this.rougeCompletedCounter = 0;
        this.rougeHalfCounter = 0;
    }

    private void OnDisable() {
        this.UnloadResources();

        PerfumeController.instance.Perfumed -= this.PerfumedListener;
        PerfumeController.instance.DownOnPerfume -= DownOnPerfumeListener;
        PerfumeController.instance.UpOnPerfume -= UpOnPerfumeListener;
        if (this.maskActive) {
            this.DeactivateMask();
        }
        this.DeactivateElementsPanel();
    }

    private void Update() {
        if (this.GetRougeHalfCounter() >= 2 && this.rougeBrush.GetIsActive() && !this.rougeBrush.IsPressed()) {
            this.DeactivateRouge();
            if (this.ParfumeRoomPowdered != null) {
                this.ParfumeRoomPowdered();
            }
        }
        if (this.GetRougeCompleteCounter() >= 2 && this.rougeBrush.GetIsActive()) {
            this.DeactivateRouge();
            if (this.ParfumeRoomPowdered != null) {
                this.ParfumeRoomPowdered();
            }
        }
    }

#region ActivateParfumeDressUP
    public void ActivateParfumeDressUP(VisitorController.VisitorSex sex,float delay = 0f) {
        this.SetActiveInteraction(ParfumRoomInteraction.PERFUME_DRESS_UP);
        this.SetVisitorSex(sex);
        StartCoroutine(this.ActivateParfumeDressUPIEnumerator(delay));
    }

    private IEnumerator ActivateParfumeDressUPIEnumerator(float delay = 0f) {
        yield return new WaitForSeconds(delay);
        this.ActivatePerfume();
    }

    public void ActivatePerfume() {
        if (ParfumHandController.instance != null) {
            ParfumHandController.instance.BringPerfume();
        }
        this.GetPerfumeController().SetBlueOrPink(this.visitorSex);
        PerfumeController.instance.Perfumed += this.PerfumedListener;
        PerfumeController.instance.DownOnPerfume += DownOnPerfumeListener;
        PerfumeController.instance.UpOnPerfume += UpOnPerfumeListener;
    }
#endregion

#region ActivateParfumRouge
    public void ActivateParfumRouge(GameObject visitor,VisitorController.VisitorType type,VisitorController.VisitorSex sex, float delay = 0f) {
        this.SetActiveInteraction(ParfumRoomInteraction.PERFUME_ROUGE);
        this.SetVisitorSex(sex);
        StartCoroutine(this.ActivateParfumRougeIenumerator(delay));
        Vector3 scale;
        Vector3 position;
        if (type.Equals(VisitorController.VisitorType.STEPANIDA)) {
            position = new Vector3(-2.25f, 1.8f, 0f);
            scale = new Vector3(1f,1f,1f);
        }
        else {
            position = new Vector3(1.25f, 0.9f, 0f);
            scale = new Vector3(0.6f, 0.6f, 0.6f);
        }
        GameObject rouge_right = Instantiate(this.rouge);
        rouge_right.transform.SetParent(visitor.transform.GetChild(0).GetChild(0));
        rouge_right.transform.localScale = scale;
        rouge_right.transform.localPosition = new Vector3(position.x, position.y, position.z);
        
        GameObject rouge_left = Instantiate(this.rouge);
        rouge_left.transform.SetParent(visitor.transform.GetChild(0).GetChild(0));
        rouge_left.transform.localScale = scale;
        rouge_left.transform.localPosition = new Vector3(-position.x, position.y, position.z);
    }

    private IEnumerator ActivateParfumRougeIenumerator(float delay) {
        yield return new WaitForSeconds(delay);
        this.ActivateRouge();
    }

    public void ActivateRouge() {
        if (ParfumHandController.instance != null) {
            ParfumHandController.instance.BringRougeBrush();
            this.GetRougeBrush().Powder += PowderListener;
            this.GetRougeBrush().PowderStop += PowderStopListener;
        }
    }
#endregion

#region RougeListeners
    private void PowderListener() {
        if (this.ParfumRoomPowder != null) {
            this.ParfumRoomPowder();
        }
    }

    private void PowderStopListener() {
        if (this.ParfumRoomPowderStop != null) {
            this.ParfumRoomPowderStop();
        }
    }
#endregion

#region PerfumeListeners
    private void PerfumedListener() {
        if (this.GetActiveInteraction() == ParfumRoomInteraction.PERFUME_DRESS_UP) {
            this.ActivateElementsPanel();
        }
        else if (this.GetActiveInteraction() == ParfumRoomInteraction.PERFUME_ROUGE) {
            InteractionManager.instance.DeactivateInteraction();
        }
    }

    private void DownOnPerfumeListener() {
        this.characterAnimationController.ContinueHappyWashSoap();
    }

    private void UpOnPerfumeListener() {
        this.characterAnimationController.StopHappyWash();
    }
#endregion

    private void LoadResources() {
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Art/ParfumRoom/Salon_p_fon");
    }

    private void UnloadResources() {
        GetComponent<SpriteRenderer>().sprite = null;
        Resources.UnloadUnusedAssets();
    }

    public GameObject GetHairdo() {
        return this.hairdo;
    }

    public void SetHairdo(GameObject obj) {
        this.hairdo = obj;
    }

    public GameObject GetGlasses() {
        return this.glasses;
    }

    public void SetGlasses(GameObject obj) {
        this.glasses = obj;
    }

    public GameObject GetBowTie() {
        return this.bow_tie;
    }

    public void SetBowTie(GameObject obj) {
        this.bow_tie = obj;
    }

    public GameObject GetHat() {
        return this.hat;
    }

    public void SetHat(GameObject obj) {
        this.hat = obj;
    }

    public GameObject GetScarf() {
        return this.scarf;
    }
    
    public void SetScarf(GameObject obj) {
        this.scarf = obj;
    }

    public GameObject GetEarrings() {
        return this.earrings;
    }
    
    public void SetEarrings(GameObject obj) {
        this.earrings = obj;
    }

    public void ActivateElementsPanel() {
        if (this.elementsPanel != null) {
            if (!this.elementsPanel.gameObject.activeSelf) {
                this.elementsPanel.gameObject.SetActive(true);
            }
            this.elementsPanel.MoveToStartPosition();
        }
    }

    public void DeactivateElementsPanel() {
        if (this.elementsPanel != null) {
            if (this.elementsPanel.gameObject.activeSelf) {
                this.elementsPanel.gameObject.SetActive(false);
            }
        }
    }

    public void DeactivateRouge() {
        this.rougeBrush.SetIsActive(false);
        this.rougeBrush.MoveToEndPosition();
        this.ActivatePerfume();
    }

    public ParfumRougeBrushController GetRougeBrush() {
        return this.rougeBrush;
    }

    public PerfumeController GetPerfumeController() {
        return this.perfumeController;
    }

    public void RougeCompleteCounter() {
        this.rougeCompletedCounter ++;
    }

    public int GetRougeCompleteCounter() {
        return this.rougeCompletedCounter;
    }

    public int GetRougeHalfCounter() {
        return this.rougeHalfCounter;
    }

    public void RougeHalfCounter() {
        this.rougeHalfCounter ++;
    }

    public ParfumRoomInteraction GetActiveInteraction() {
        return this.activeInteraction;
    }

    private void SetActiveInteraction(ParfumRoomInteraction interaction) {
        this.activeInteraction = interaction;
    }

    private void SetVisitorSex(VisitorController.VisitorSex sex) {
        this.visitorSex = sex;
    }

    private VisitorController.VisitorSex GetVisitorSex() {
        return this.visitorSex;
    }

    public void SetVisitor(GameObject visitor) {
        this.visitor = visitor;
        this.characterAnimationController = visitor.GetComponent<InteractionCharacterAnimationController>();
    }

    public GameObject GetVisitor() {
        return this.visitor;
    }

    public VisitorController.VisitorType GetVisitorType() {
        return this.visitor.GetComponent<VisitorsTypes>().GetVisitorType();
    }

    #region Mask
    public void AddBubble(GameObject bubble) {
        this.maskBubbleList.Add(bubble);
    }

    public void RemoveBubble(GameObject bubble) {
        this.maskBubbleList.Remove(bubble);
    }

    public List<GameObject> GetMaskBubbleList() {
        return this.maskBubbleList;
    }

    public int GetBubbleCount() {
        return this.maskBubbleCount;
    }

    private bool maskActive;
    public void ActivateMask() {
        this.maskActive = true;
        if (!this.mask.activeSelf) {
            this.mask.SetActive(true);
        }
        if (!this.maskFacialTissue.activeSelf) {
            this.maskFacialTissue.SetActive(true);
        }
        if (this.mask != null) {
            this.mask.GetComponent<MaskSpongeController>().ApplyMask += ApplyMaskListener;
            this.mask.GetComponent<MaskSpongeController>().DrawMask += DrawMaskListener;
            this.mask.GetComponent<MaskSpongeController>().StopDrawMask += StopDrawMaskListener;
        }

        if (this.maskFacialTissue != null) {
            this.maskFacialTissue.GetComponent<MaskFacialTissueController>().RemoveTheMask += RemoveMaskListener;
        }
        ParfumHandController.instance.BringMask();
    }

    public void DeactivateMask() {
        this.maskActive = false;
        if (this.maskFacialTissue != null) {
            this.maskFacialTissue.GetComponent<MaskFacialTissueController>().RemoveTheMask -= RemoveMaskListener;
        }
        if (this.mask.activeSelf) {
            this.mask.SetActive(false);
        }
        if (this.maskFacialTissue.activeSelf) {
            this.maskFacialTissue.SetActive(false);
        }
        if (this.mask != null) {
            this.mask.GetComponent<MaskSpongeController>().ApplyMask -= ApplyMaskListener;
            this.mask.GetComponent<MaskSpongeController>().DrawMask -= DrawMaskListener;
            this.mask.GetComponent<MaskSpongeController>().StopDrawMask -= StopDrawMaskListener;
        }
    }

    public GameObject GetMask() {
        return this.mask;
    }

    public GameObject GetMaskFacialTissue() {
        return this.maskFacialTissue;
    }

    // ***** Listeners *****
    private void ApplyMaskListener() {
        ParfumHandController.instance.BringCocumberLeft();//BringMaskFacialTissue();
        this.mask.SetActive(false);
        if (this.ParfumeRoomMaskApply != null) {
            this.ParfumeRoomMaskApply();
        }
    }

    private void DrawMaskListener(){
        if (this.ParfumRoomMaskDraw != null) {
            this.ParfumRoomMaskDraw();
        }
    }

    private void StopDrawMaskListener() {
        if (this.ParfumRoomMaskDrawStop != null) {
            this.ParfumRoomMaskDrawStop();
        }
    }

    private void RemoveMaskListener() {
        MaskFacialTissueController.instance.MoveToEndPosition();
        InteractionManager.instance.DeactivateInteraction();
    }

    public void LeftCocumberInPlace() {
        ParfumHandController.instance.BringCocumberRight();
    }

    public void RightCocumberInPlace() {
        StartCoroutine(this.ShowZzzIenumerator());
    }

    private IEnumerator ShowZzzIenumerator() {
        Debug.Log("zZzZz!!!");
        yield return new WaitForSeconds(0.1f);
        ParfumHandController.instance.BringMaskFacialTissue();
    }

    #endregion

}
