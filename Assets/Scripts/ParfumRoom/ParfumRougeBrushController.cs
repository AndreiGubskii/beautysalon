﻿using UnityEngine;
using System.Collections;

public class ParfumRougeBrushController : MonoBehaviour {
    public delegate void ParfumRougeBrushEvents();

    public event ParfumRougeBrushEvents Powder;
    public event ParfumRougeBrushEvents PowderStop;

    private bool pressed;
    private bool isActive;
    private Vector3 target;
    private bool move;
    private Vector3 endPosition = new Vector3(17f,0f,0f);
    private Vector3 startPosition = new Vector3(7f, 0f, 0f);
    private bool firstTap;
    private void OnEnable() {
        if (SystemInfo.deviceModel.Contains("iPad")) {
            startPosition = new Vector3(5.5f, -3.5f, 0f);
        }
        this.firstTap = false;
        this.isActive = true;
        this.pressed = false;
        this.move = false;
        this.transform.localPosition = this.endPosition;
    }

    void Update () {
	    if (this.pressed && this.isActive) {
	        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
	        if (Physics.Raycast(ray, out hit, 100f)) {
	            transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
	        }
	    }

        if (this.firstTap && !pressed && !move && !this.transform.localPosition.Equals(this.startPosition) && this.isActive) {
            MoveTo(this.startPosition);
        }

        if (this.move) {
            if (this.transform.localPosition != this.target) {
                this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                    20f*Time.deltaTime);
            }

            if (this.transform.localPosition == this.target) {
                this.move = false;
            }
        }
    }

    
    private void OnMouseDown() {
        if (!this.firstTap) {
            this.firstTap = true;
        }
        if (this.Powder != null) {
	        this.Powder();
	    }
        this.pressed = true;
        this.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 120f));
        this.ResetParent();
    }

    private void OnMouseUp() {
        this.pressed = false;
        this.transform.rotation = Quaternion.Euler(Vector3.zero);
        if (this.PowderStop != null) {
	        this.PowderStop();
	    }
    }

    public void SetIsActive(bool active) {
        this.isActive = active;
    }

    public bool GetIsActive() {
        return this.isActive;
    }

    public void MoveTo(Vector3 target) {
        this.target = target;
        this.move = true;
    }

    public void MoveToEndPosition() {
        this.MoveTo(this.endPosition);
    }

    private void ResetParent() {
        if (this.transform.parent.Equals(ParfumRoomController.instance.gameObject.transform)) {
            return;
        }
        this.transform.SetParent(ParfumRoomController.instance.gameObject.transform);
    }

    public bool IsPressed() {
        return this.pressed;
    }
}
