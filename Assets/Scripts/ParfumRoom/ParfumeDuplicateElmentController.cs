﻿using TouchScript.InputSources;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ParfumeDuplicateElmentController : MonoBehaviour{
    private ParfumElementController.DuplicateTransform duplicateTransform;
    private bool drag;
    private bool down;
    private bool scale;
    private bool inCharacterCollider;
    private Vector3 scaleTarget;
    private Vector3 dragPosition;
	
	void Start () {
	    this.drag = false;
	    this.scale = false;
	    this.inCharacterCollider = false;
	}
	
	void Update () {
	    if (this.drag && this.down) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
	        if (Physics.Raycast(ray, out hit, 100f)) {
	            transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
	        }
	    }
	    if (this.scale) {
	        this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, this.scaleTarget, 15f*Time.deltaTime);
	    }
	    if (this.transform.localScale == this.scaleTarget) {
	        this.scale = false;
	        if (scaleTarget == Vector3.zero) {
                Destroy(this.gameObject);
            }
	    }
	}

    private void OnMouseDrag() {
        if (Input.mousePosition != this.dragPosition) {
            drag = true;
            this.dragPosition = Input.mousePosition;
        }
        else {
            drag = false;
        }
    }

    private void OnMouseDown() {
        this.down = true;
    }

    private bool x;
    private void OnMouseUp() {
        this.down = false;
        GetComponent<SpriteRenderer>().sortingOrder -= 15;
        if (!this.inCharacterCollider || x) {
            SetScale(Vector3.zero);
            //Destroy(this.gameObject);
        }
        else {
            x = true;
            this.transform.SetParent(c.transform.GetChild(0).GetChild(0));
            this.transform.localPosition = this.duplicateTransform.position;
            SetScale(this.duplicateTransform.scale);
            this.ReplaceElement(GetComponent<ParfumElementsType>().GetElementType());
        }
    }

    private Collider c;
    private void OnTriggerStay(Collider col) {
        if (col.GetComponent<InteractionCharacterController>() != null && !this.down) {
            /*this.transform.SetParent(col.transform.GetChild(0).GetChild(0));
            this.transform.localPosition = this.duplicateTransform.position;
            SetScale(this.duplicateTransform.scale);
           
            
            this.ReplaceElement(GetComponent<ParfumElementsType>().GetElementType());*/
        }
    }

    private Rigidbody rb;
    private void OnTriggerEnter(Collider col) {
        if (col.GetComponent<InteractionCharacterController>() != null) {
            c = col;
            this.inCharacterCollider = true;
            if (rb == null) {
                rb = gameObject.AddComponent<Rigidbody>();
                rb.isKinematic = true;
                rb.useGravity = false;
                BoxCollider bc = gameObject.GetComponent<BoxCollider>();
                bc.size = new Vector3(bc.size.x, bc.size.y, 15f);
            }
        }
    }

    private void OnTriggerExit(Collider col) {
        if (col.GetComponent<InteractionCharacterController>() != null) {
            this.inCharacterCollider = false;
        }
    }

    private void SetScale(Vector3 scale) {
        this.scaleTarget = scale;
        this.scale = true;
    }

    public void SetDuplicateTransform(ParfumElementController.DuplicateTransform var) {
        this.duplicateTransform = var;
    }

    private void ReplaceElement(ElementsType type){
        switch (type) {
            case ElementsType.HAIRDO:
                if (ParfumRoomController.instance.GetHairdo() != null) {
                    ParfumRoomController.instance.GetHairdo().
                        GetComponent<ParfumeDuplicateElmentController>().SetScale(Vector3.zero);
                    //Destroy(ParfumRoomController.instance.GetHairdo());
                }
                ParfumRoomController.instance.SetHairdo(this.gameObject);
            break;
            case ElementsType.GLASSES:
                if (ParfumRoomController.instance.GetGlasses() != null) {
                    //Destroy(ParfumRoomController.instance.GetGlasses());
                    ParfumRoomController.instance.GetGlasses().
                        GetComponent<ParfumeDuplicateElmentController>().SetScale(Vector3.zero);
                }
                ParfumRoomController.instance.SetGlasses(this.gameObject);
            break;
            case ElementsType.BOW_TIE:
                if (ParfumRoomController.instance.GetBowTie() != null) {
                   // Destroy(ParfumRoomController.instance.GetBowTie());
                   ParfumRoomController.instance.GetBowTie().
                        GetComponent<ParfumeDuplicateElmentController>().SetScale(Vector3.zero);
                }
                ParfumRoomController.instance.SetBowTie(this.gameObject);
            break;
            case ElementsType.HAT:
                if (ParfumRoomController.instance.GetHat() != null) {
                    //Destroy(ParfumRoomController.instance.GetHat());
                    ParfumRoomController.instance.GetHat().
                        GetComponent<ParfumeDuplicateElmentController>().SetScale(Vector3.zero);
                }
                ParfumRoomController.instance.SetHat(this.gameObject);
            break;
            case ElementsType.SCARF:
                if (ParfumRoomController.instance.GetScarf() != null) {
                    // Destroy(ParfumRoomController.instance.GetScarf());
                    ParfumRoomController.instance.GetScarf().
                        GetComponent<ParfumeDuplicateElmentController>().SetScale(Vector3.zero);;
                }
                ParfumRoomController.instance.SetScarf(this.gameObject);
            break;
            case ElementsType.EARRINGS:
                if (ParfumRoomController.instance.GetEarrings() != null) {
                    //Destroy(ParfumRoomController.instance.GetEarrings());
                    ParfumRoomController.instance.GetEarrings().
                        GetComponent<ParfumeDuplicateElmentController>().SetScale(Vector3.zero);
                }
                ParfumRoomController.instance.SetEarrings(this.gameObject);
            break;
        }
    }
}
