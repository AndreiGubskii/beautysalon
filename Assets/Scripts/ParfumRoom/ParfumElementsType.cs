﻿using UnityEngine;

public enum ElementsType {
    HAIRDO,
    GLASSES,
    HAT,
    SCARF,
    EARRINGS,
    BOW_TIE
}

public class ParfumElementsType : MonoBehaviour {
    [SerializeField] private ElementsType type;
    private SpriteRenderer spriteRenderer;

    private void Start() {
        if (this.spriteRenderer == null) {
            this.spriteRenderer = GetComponent<SpriteRenderer>();
        }
        this.spriteRenderer.sortingOrder = transform.parent.GetComponent<SpriteRenderer>().sortingOrder + 1;
    }

    public ElementsType GetElementType() {
        return this.type;
    }
}