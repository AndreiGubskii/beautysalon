﻿using UnityEngine;
using System.Collections;

public class CharacterAnimationController : MonoBehaviour {
    private Animator _animator;
    private int hashWalk;
    private int hashWait;
    private int hashGreet;
    private int hashVibratingChairOn;
    private int hashVibratingChairOff;
    private int hashBlink;
    private int hashOther_Wait;
    private bool vibratingChair;
    private void Start() {
        this.vibratingChair = false;
        this.hashGreet = Animator.StringToHash("greet");
        this.hashVibratingChairOff = Animator.StringToHash("vibratingChairOff");
        this.hashVibratingChairOn = Animator.StringToHash("vibratingChairOn");
        this.hashWait = Animator.StringToHash("wait");
        this.hashWalk = Animator.StringToHash("walk");
        this.hashBlink = Animator.StringToHash("blink");
        this.hashOther_Wait = Animator.StringToHash("other_wait");
        this._animator = GetComponent<Animator>();
    }

    public void PlayWalk() {
        this._animator.SetTrigger(this.hashWalk);
    }

    public void PlayWait() {
        if (GetVibratingChair()) return;
        this._animator.SetTrigger(this.hashWait);
    }

    public void PlayGreet() {
        this._animator.SetTrigger(this.hashGreet);
    }

    public void PlayVibratingChairOn() {
        this._animator.SetTrigger(this.hashVibratingChairOn);
        this.vibratingChair = true;
    }

    public void PlayVibratingChairOff() {
        this._animator.SetTrigger(this.hashVibratingChairOff);
        this.vibratingChair = false;
    }

    public void PlayBlink() {
        this._animator.SetTrigger(this.hashBlink);
    }

    public void PlayOtherWait() {
        if(GetVibratingChair()) return;
        this._animator.SetTrigger(this.hashOther_Wait);
    }

    public bool GetVibratingChair() {
        return this.vibratingChair;
    }
}
