﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class NailScript : MonoBehaviour {
    private List<int> stickersPosition;
    private Vector3[] targetsDuplicateStickers;

    private void OnEnable() {
        stickersPosition = new List<int>();
        this.targetsDuplicateStickers = NailStickerController.instance.GetTargetsDuplicateStickers();
    }

    public Vector3 Target() {
        if (this.stickersPosition.Count >= this.targetsDuplicateStickers.Length) {
            return Vector3.zero;
        }
        int id = Random.Range(0, this.targetsDuplicateStickers.Length);
        if (this.stickersPosition.Count == 0) {
            this.stickersPosition.Add(id);
            return this.targetsDuplicateStickers[id];
        }
        for (int i = 0; i < stickersPosition.Count; i++) {
            if (id == stickersPosition[i]) {
                id = Random.Range(0, this.targetsDuplicateStickers.Length);
                i = 0;
                continue;
            }
        }
        this.stickersPosition.Add(id);
        return this.targetsDuplicateStickers[id];
    }
}
