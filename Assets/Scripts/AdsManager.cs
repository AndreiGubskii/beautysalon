﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour {
    [System.Serializable] public class AdsSettings {
        public bool enable = true;
        public int maximumImpressions;
        public int showEvery;
        private int counterImpressions;
        private bool adsShowed;
        private int queryCounter = 0;

        public void CountQuery() {
            queryCounter ++;
        }

        public int GetCountQuery() {
            return queryCounter;
        }

        public bool GetAdsShowed() {
            return this.adsShowed;
        }

        public void ResetAdsShowed() {
            this.adsShowed = false;
        }

        public void CountImpressions() {
            this.counterImpressions++;
            this.adsShowed = true;
        }

        public void ResetCounterImpressions() {
            this.counterImpressions = 0;
        }

        public int GetCountImpressions() {
            return this.counterImpressions;
        }

        public int GetShowEvery() {
            return showEvery;
            
        }
    }

    [SerializeField] private AdsSettings adsSettings;
    public static AdsManager instance;

    private void Start() {
        instance = this;

    }

#if UNITY_ANDROID || UNITY_EDITOR
    private IEnumerator ShowAdWhenReady() {
        while (!Advertisement.IsReady())
            yield return null;

        Advertisement.Show();
        this.adsSettings.CountImpressions();
    }
#endif

    public void ShowAds() {
        if (this.adsSettings.enable) {
            if (adsSettings.GetCountImpressions() >= adsSettings.maximumImpressions) {
                return;
            }

            if (this.adsSettings.GetCountQuery() % this.adsSettings.GetShowEvery() == 0) {
#if UNITY_ANDROID || UNITY_EDITOR
                StartCoroutine(this.ShowAdWhenReady());
#endif
            }
            else {
                this.adsSettings.ResetAdsShowed();
            }
            this.adsSettings.CountQuery();
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus) {
            this.adsSettings.ResetCounterImpressions();
            this.adsSettings.ResetAdsShowed();
        }
    }
}
