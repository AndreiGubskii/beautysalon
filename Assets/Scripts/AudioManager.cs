﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {
    [SerializeField] private MyAudioClip[] musicList;
    [SerializeField] private MyAudioClip[] soundList;
    [SerializeField] private AudioSources audioSources;
    public static AudioManager instance;
    private void Awake() {
        instance = this;
        DontDestroyOnLoad(this);
    }

    public void PlayMusic(string nameClip) {
        audioSources.music.clip = GetMusicByName(nameClip);
        audioSources.music.Play();
    }

    public void PlaySound(string nameClip) {
        audioSources.sound.clip = GetSoundByName(nameClip);
        audioSources.sound.Play();
    }

    public void PlaySound(string nameClip,float delay) {
        audioSources.sound.clip = GetSoundByName(nameClip);
        audioSources.sound.PlayDelayed(delay);
    }

    public AudioSource GetSoundSource() {
        return audioSources.sound;
    }

    public AudioClip GetMusicByName(string name) {
        foreach (MyAudioClip myAudioClip in musicList) {
            if (myAudioClip.name == name) {
                return myAudioClip.clip;
            }
        }
        return null;
    }

    public AudioClip GetSoundByName(string name) {
        foreach (MyAudioClip mySound in soundList) {
            if (mySound.name == name) {
                return mySound.clip;
            }
        }
        return null;
    }

    [System.Serializable]
    public class AudioSources {
        public AudioSource music;
        public AudioSource sound;
    }

    [System.Serializable]
    public class MyAudioClip {
        public string name;
        public AudioClip clip;
    }


}
