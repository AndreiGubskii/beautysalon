﻿using UnityEngine;
using System.Collections;

public class VaseController : MonoBehaviour {
    private Animator _animator;
    private int hashTap;
    private void Start() {
        this.hashTap = Animator.StringToHash("tap");
        this._animator = GetComponent<Animator>();
    }

    private void OnMouseDown() {
        this._animator.SetTrigger(this.hashTap);
        AudioManager.instance.GetSoundSource()
            .PlayOneShot(AudioManager.instance.GetSoundByName("click"));
    }
}
