﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class BathRoomController : MonoBehaviour {
    public delegate void BathRoomEvents ();

    public event BathRoomEvents OnActivateBathRoom;
    public event BathRoomEvents OnDeactivateBathRoom;

    public event BathRoomEvents OnLathered;
    public event BathRoomEvents OnWasheded;
    public event BathRoomEvents OnWiped;
    public event BathRoomEvents OnSpreadFoam;
    public event BathRoomEvents OnSpreadFoamStoped;
    public event BathRoomEvents OnWashFoam;
    public event BathRoomEvents OnWashFoamStoped;

    private List<GameObject> bubblesList;
    private int bubbleCount = 80;
    public static BathRoomController instance;
    private GameObject visitor;
    private GameObject showerHead;
    private GameObject sponge;
    private GameObject facialTissue;
    [SerializeField] private GameObject wetPrefab;
    private SpongeController spongeController;
    private void OnEnable() {
        if (instance == null) {
            instance = this;
        }
    }
    
    private void OnDisable() {
        if (this.OnDeactivateBathRoom != null) {
            this.OnDeactivateBathRoom();
        }
        if (instance != null) {
            instance = null;
        }
        this.DeactivateCleaningFace();
    }

    public void ActivateCleaningFace(GameObject visitor) {
        this.LoadResources();

        this.sponge.SetActive(true);
        this.showerHead.SetActive(false);
        this.bubblesList = new List<GameObject>();
        this.SetVisitor(visitor);

        this.sponge.GetComponent<SpongeController>().SpongeActivate += SpongeActivateListener;
        this.sponge.GetComponent<SpongeController>().Lathered += LatheredListener;
        this.sponge.GetComponent<SpongeController>().SpreadFoam += SpreadFoamListener;
        this.sponge.GetComponent<SpongeController>().SpreadFoamStopped += SpreadFoamStoppedListener;
        this.showerHead.GetComponent<ShowerheadController>().Washed += WashedListener;
        this.showerHead.GetComponent<ShowerheadController>().WashOff += WashFoamListener;
        this.showerHead.GetComponent<ShowerheadController>().WashStopped += WashFoamStoppedListener;
        this.facialTissue.GetComponent<FacialTissueController>().Wiped += WipedListener;
        this.facialTissue.GetComponent<FacialTissueController>().PreWiped += PreWipedListener;


    }

    public void DeactivateCleaningFace() {
        this.sponge.GetComponent<SpongeController>().SpongeActivate -= SpongeActivateListener;
        this.sponge.GetComponent<SpongeController>().SpreadFoam -= SpreadFoamListener;
        this.sponge.GetComponent<SpongeController>().Lathered -= LatheredListener;
        this.sponge.GetComponent<SpongeController>().SpreadFoamStopped -= SpreadFoamStoppedListener;
        this.showerHead.GetComponent<ShowerheadController>().Washed -= WashedListener;
        this.showerHead.GetComponent<ShowerheadController>().WashStopped -= WashFoamStoppedListener;
        this.facialTissue.GetComponent<FacialTissueController>().Wiped -= WipedListener;
        this.facialTissue.GetComponent<FacialTissueController>().PreWiped -= PreWipedListener;

        this.UnloadResources();
    }

    public void AddBubble(GameObject bubble) {
        this.bubblesList.Add(bubble);
    }

    public void RemoveBubble(GameObject bubble){
        this.bubblesList.Remove(bubble);
    }

    public List<GameObject> GetBubbleList() {
        return this.bubblesList;
    }

    public GameObject GetSponge() {
        return this.sponge;
    }

    public GameObject GetShowerHead() {
        return this.showerHead;
    }

    public GameObject GetFacialTissueHead() {
        return this.facialTissue;
    }

    public int GetBubbleCount() {
        return this.bubbleCount;
    }


    //************* Listeners *****************

    private void SpongeActivateListener() {
       if (this.OnActivateBathRoom != null) {
            this.OnActivateBathRoom();
       }
    }
    //Во время намыливания
    private void SpreadFoamListener(){
        if (this.OnSpreadFoam != null) {
            this.OnSpreadFoam();
        }
    }

    private void SpreadFoamStoppedListener() {
        if (this.OnSpreadFoamStoped != null) {
            this.OnSpreadFoamStoped();
        }
    }

    //Намазали пеной
    private void LatheredListener() {
        if (this.OnLathered != null) {
            this.OnLathered();
        }
        GetShowerHead().SetActive(true);
        GetSponge().SetActive(false);
        GetWetPrefab().SetActive(true);
    }
    //Во время смывания пены
    private void WashFoamListener() {
        if (this.OnWashFoam != null) {
            this.OnWashFoam();
        }
    }

    private void WashFoamStoppedListener() {
        if (this.OnWashFoamStoped != null) {
            this.OnWashFoamStoped();
        }
    }

    //Смыли пену
    private void WashedListener() {
        if (this.OnWasheded != null) {
            this.OnWasheded();
        }
        GetFacialTissueHead().SetActive(true);
        GetShowerHead().SetActive(false);
    }
    //Вытерли полотенцем
    private void WipedListener() {
        if (this.OnWiped != null) {
            this.OnWiped();
        }
        GetFacialTissueHead().SetActive(false);
        InteractionManager.instance.DeactivateInteraction();
    }

    private void PreWipedListener() {
        wetPrefab.SetActive(false);
    }

    public GameObject GetWetPrefab() {
        return this.wetPrefab;
    }

    private void SetVisitor(GameObject visitor) {
        this.visitor = visitor;
    }

    public GameObject GetVisitor() {
        return this.visitor;
    }

    private void LoadResources() {
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Art/Bathroom/Background_bathroom");

        this.sponge = InstanceResources("sponge",new Vector3(16.5f,0f,0f));
        this.showerHead = InstanceResources("showerHead", new Vector3(14f, 0f, 0f));
        this.facialTissue = InstanceResources("facialTissue", new Vector3(16f, 0f, 0f));
    }

    private void UnloadResources() {
        GetComponent<SpriteRenderer>().sprite = null;
        Destroy(this.sponge);
        Destroy(this.showerHead);
        Destroy(this.facialTissue);

        Resources.UnloadUnusedAssets();
    }

    private GameObject InstanceResources(string name,Vector3 position) {
        GameObject obj = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Bathroom/"+name));
        obj.transform.SetParent(this.transform);
        obj.transform.localScale = Vector3.one;
        obj.transform.localPosition = position;
        return obj;
    }

}
