﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class SpongeController : MonoBehaviour {
    public delegate void SpongeEvents();
    public event SpongeEvents Lathered;
    public event SpongeEvents SpreadFoam;
    public event SpongeEvents SpreadFoamStopped;
    public event SpongeEvents SpongeActivate;
    [SerializeField] private GameObject bubble;
    [SerializeField] private Sprite crumpledSponge;
    private SpriteRenderer sponge;
    private Sprite normalSponge;
    private Vector3 startPosition = new Vector3(6.5f,0f,0f);
    private Vector3 endPosition = new Vector3(16.5f, 0f, 0f);
    private Vector3 target;
    private bool _activate;
    private bool foamStopped;
    private float lastBubbleTime;

    private void OnEnable() {
        if (SystemInfo.deviceModel.Contains("iPad")) {
            startPosition = new Vector3(4.6f,-2.6f,0f);
        }
        this.sponge = GetComponent<SpriteRenderer>();
        this.normalSponge = this.sponge.sprite;
        this.transform.localPosition = this.endPosition;
        this.target = this.startPosition;
        this._activate = false;
        this.foamStopped = false;
    }
    private void OnDisable() {
        this.sponge.sprite = this.normalSponge;
    }
    private void Update() {
        if (this.transform.localPosition != this.target && !IsActivate()) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target, 20 * Time.deltaTime);
            if (this.transform.localPosition == this.endPosition) {
                if (this.Lathered != null) {
                    this.Lathered();
                }
            }
        }
        if (this.transform.localPosition.Equals(this.startPosition) && !IsActivate()) {
            this.SetActivate(true);
            if (this.SpongeActivate != null) {
                this.SpongeActivate();
            }
        }
        
        if (Input.GetMouseButton(0) && IsActivate() && 
            BathRoomController.instance.GetBubbleList().Count < BathRoomController.instance.GetBubbleCount()) {
            if ((Input.mousePosition.x / Screen.width) < 0.2f && (Input.mousePosition.y / Screen.height) > 0.8f) return;

            if (!this.sponge.sprite.Equals(this.crumpledSponge)) {
                this.sponge.sprite = this.crumpledSponge;
            }
            if (this.SpreadFoam != null) {
                this.SpreadFoam();
            }
            this.foamStopped = false;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f)) {
                transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
                if (hit.transform.gameObject.GetComponent<VisitorsTypes>() != null && this.makeBubble &&
                    BathRoomController.instance.GetBubbleList().Count < BathRoomController.instance.GetBubbleCount()) {
                    if (this.lastBubbleTime + 0.05f > Time.time) return;
                    this.lastBubbleTime = Time.time;
                    GameObject bub = (GameObject) Instantiate(this.bubble, transform.position, Quaternion.identity);
                    bub.transform.SetParent(BathRoomController.instance.GetVisitor().transform.GetChild(0).GetChild(0));
                    BathRoomController.instance.AddBubble(bub);
                    if (!InteractionManager.instance.GetInteractionWas()) {
                        InteractionManager.instance.SetInteractionWas(true);
                    }
                }
            }
        }
        else if (this.IsActivate() && BathRoomController.instance.GetBubbleList().Count > BathRoomController.instance.GetBubbleCount()-1)
        {
            this.SetActivate(false);
            this.target = this.endPosition;
        }
        else {
            if (this.IsActivate() && BathRoomController.instance.GetBubbleList().Count > 
                BathRoomController.instance.GetBubbleCount()/2) {
                this.SetActivate(false);
                this.target = this.endPosition;
            }
            if (this.foamStopped) return;
            if (this.SpreadFoamStopped != null) {
                this.SpreadFoamStopped();
            }
            this.foamStopped = true;
        }
    }

    public bool IsActivate() {
        return _activate;
    }

    private void SetActivate(bool b) {
        this._activate = b;
    }

    private bool makeBubble;
    private void OnTriggerStay(Collider col) {
        if (col.transform.gameObject.GetComponent<BubbleController>() == null){
            this.makeBubble = true;
        }
        else {
            this.makeBubble = false;
        }
    }
}
