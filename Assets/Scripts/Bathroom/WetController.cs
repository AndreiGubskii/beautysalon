﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class WetController : MonoBehaviour {
    [SerializeField] private Transform[] dropSpawns;
    [SerializeField] private GameObject drop;

    private void OnEnable() {
        InvokeRepeating("CreatDrop",0.1f,0.5f);
    }

    private void OnDisable() {
        this.Deactivate();
    }

    public void Deactivate() {
        CancelInvoke("CreatDrop");
        if (this.gameObject.activeSelf) {
            this.gameObject.SetActive(false);
        }
    }

    private void CreatDrop() {
        GameObject obj = (GameObject)Instantiate(this.drop, this.dropSpawns[Random.Range(0, dropSpawns.Length)].position, Quaternion.identity);
        obj.transform.parent = transform;
        Destroy(obj,3f);
    }
}
