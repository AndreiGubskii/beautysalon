﻿using UnityEngine;
using System.Collections;

public class DropController : MonoBehaviour {
    private void Start() {
        Destroy(this.gameObject, 1f);
    }

    private void Update() {
        transform.Translate(Vector3.left*Time.deltaTime*15f);
    }

    private void OnTriggerEnter(Collider col) {
        BubbleController bubble = col.gameObject.GetComponent<BubbleController>();
        if (bubble != null) {
            BathRoomController.instance.RemoveBubble(bubble.gameObject);
            Destroy(bubble.gameObject);
            Destroy(this.gameObject, 0.1f);
        }
    }
}
