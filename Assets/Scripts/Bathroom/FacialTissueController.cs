﻿using UnityEngine;
using System.Collections;

public class FacialTissueController : MonoBehaviour {
    public delegate void FacialTissueEvents();
    public event FacialTissueEvents Wiped;
    public event FacialTissueEvents PreWiped;
    private Vector3 startPosition = new Vector3(6.5f, 0f, 0f);
    private Vector3 endPosition = new Vector3(16.5f, 0f, 0f);
    private Vector3 target;
    private bool _activate;
    private float actionTimer;
    private bool preWiped;
    private SpriteRenderer facialTissues;
    [SerializeField] private Sprite crumpledFacialTissues;
    private Sprite normalFacialTissues;

    private void OnEnable() {
        if (SystemInfo.deviceModel.Contains("iPad"))
        {
            startPosition = new Vector3(4.6f, -2.6f, 0f);
        }
        this.transform.localPosition = this.endPosition;
        this.target = this.endPosition;
        this.preWiped = false;
        this.facialTissues = GetComponent<SpriteRenderer>();
        this.normalFacialTissues = this.facialTissues.sprite;
        this._activate = false;
        this.actionTimer = 5f;
        this.MoveToStartPosition();
    }

    private void OnDisable() {
        this.facialTissues.sprite = this.normalFacialTissues;
        this.gameObject.SetActive(false);
    }

    public void MoveToStartPosition() {
        this.target = this.startPosition;
    }

    private void Update() {
        if (this.transform.localPosition != this.target && !IsActivate()) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target, 20 * Time.deltaTime);
            if (this.transform.localPosition == this.endPosition) {
                if (this.Wiped != null) {
                    this.Wiped();
                }
            }
        }
        if (this.transform.localPosition.Equals(this.target) && !IsActivate()) {
            this.SetActivate(true);
        }

        if (Input.GetMouseButton(0) && this.IsActivate()) {
            if ((Input.mousePosition.x / Screen.width) < 0.2f && (Input.mousePosition.y / Screen.height) > 0.8f) return;

            if (!this.facialTissues.sprite.Equals(this.crumpledFacialTissues)) {
                this.facialTissues.sprite = this.crumpledFacialTissues;
            }
            this.Timer();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f)) {
                transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
            }
        }

    }

    public bool IsActivate() {
        return _activate;
    }

    private void SetActivate(bool b) {
        this._activate = b;
    }

    private void Timer() {
        if (this.actionTimer > 0) {
            this.actionTimer-=1*Time.deltaTime;
        }
        if (this.actionTimer < 0.5f && this.actionTimer > 0.2f && !this.preWiped) {
            this.preWiped = true;
            if (this.PreWiped != null) {
                this.PreWiped();
            }
        }
        if (this.actionTimer < 0) {
            this.actionTimer = 0;
        }
        if (this.actionTimer.Equals(0)) {
            this.SetActivate(false);
            this.target = this.endPosition;
        }

    }

    private void OnTriggerEnter(Collider col) {
        DropWetController dwc = col.gameObject.GetComponent<DropWetController>();
        if (dwc != null) {
            Destroy(dwc.gameObject);
        }
    }
}
