﻿using UnityEngine;
using System.Collections;

public class ShowerheadController : MonoBehaviour {
    public delegate void ShowerheadEvents();
    public event ShowerheadEvents Washed;
    public event ShowerheadEvents WashOff;
    public event ShowerheadEvents WashStopped;
    [SerializeField] private Transform[] waterSpawns;
    [SerializeField] private GameObject waterDrop;
    private Vector3 startPosition = new Vector3(6.5f, 0f, 0f);
    private Vector3 endPosition = new Vector3(16.5f, 0f, 0f);
    private Vector3 target;
    private bool _activate;
    private bool washStopped;

    private void OnEnable() {
        if (SystemInfo.deviceModel.Contains("iPad"))
        {
            startPosition = new Vector3(4.6f, -2.6f, 0f);
        }
        this.transform.localPosition = this.endPosition;
        this.target = this.startPosition;
        this._activate = false;
        this.washStopped = true;
    }
    private void Update(){
        if (this.transform.localPosition != this.target && !IsActivate()){
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target, 20 * Time.deltaTime);
            if (this.transform.localPosition == this.endPosition){
                if (this.Washed != null) {
                    this.Washed();
                }
                
            }
        }
        if (this.transform.localPosition.Equals(this.target) && !IsActivate()){
            this.SetActivate(true);
        }

        if (Input.GetMouseButton(0) && IsActivate() && BathRoomController.instance.GetBubbleList().Count > 0) {
            if ((Input.mousePosition.x / Screen.width) < 0.2f && (Input.mousePosition.y / Screen.height) > 0.8f) return;

            this.Water();
            if (this.WashOff != null) {
                this.WashOff();
            }
            this.washStopped = false;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f)){
                transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
                BubbleController bubble = hit.transform.gameObject.GetComponent<BubbleController>();
                if (bubble != null){
                    BathRoomController.instance.RemoveBubble(bubble.gameObject);
                    Destroy(bubble.gameObject);
                }
            }
        }
        else if (this.IsActivate() && BathRoomController.instance.GetBubbleList().Count == 0) {
            this.SetActivate(false);
            this.target = this.endPosition;
        }
        else {
            if (this.washStopped) return;
            if (this.WashStopped != null) {
                this.WashStopped();
            }
            this.washStopped = true;
        }
    }

    public bool IsActivate(){
        return _activate;
    }

    private void SetActivate(bool b){
        this._activate = b;
    }

    private float waterTime = 0;
    private void Water() {
        if ((this.waterTime + 0.1f) > Time.time) return;
        for (int i = 0; i < 5; i++) {
            int spawnID = Random.Range(0, waterSpawns.Length);
            GameObject drop = (GameObject)Instantiate(this.waterDrop, this.waterSpawns[spawnID].position, Quaternion.identity);
            drop.transform.SetParent(this.transform);
        }
        waterTime = Time.time;
    }
}
