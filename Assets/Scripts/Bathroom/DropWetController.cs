﻿using UnityEngine;
using System.Collections;

public class DropWetController : MonoBehaviour {
    private SpriteRenderer spriteRenderer;
    private float i = 1;
    private void Start() {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update() {
        this.spriteRenderer.color = new Color(this.spriteRenderer.color.r,
            this.spriteRenderer.color.g, this.spriteRenderer.color.b, i-=0.4f*Time.deltaTime);//Color.Lerp(, Color.clear, 5 * Time.deltaTime);
        if (this.spriteRenderer.color.Equals(Color.clear)) {
            Destroy(gameObject);
        }
    }
}
