﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectMover : MonoBehaviour {
    private Vector3 target;
    private bool move;
    private float speed;
    private RectTransform rectTransform;
    private bool anchorPosition;

    private void OnEnable() {
        this.rectTransform = GetComponent<RectTransform>();
    }

    private void Update () {
	    if (move && this.anchorPosition) {
            this.rectTransform.anchoredPosition = Vector3.MoveTowards(this.rectTransform.anchoredPosition, this.target,
                this.speed * Time.deltaTime);
        }

        if (this.rectTransform.anchoredPosition.Equals(this.target) && this.move && this.anchorPosition) {
            this.move = false;
        }

   	    if (move && !this.anchorPosition) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                this.speed * Time.deltaTime);
        }

        if (this.transform.localPosition.Equals(this.target) && this.move && !this.anchorPosition) {
            this.move = false;
        }
        
	}

    public void MoveTo(Vector3 v,float speed = 20f,bool anchorPosition = true) {
        this.speed = speed;
        this.move = true;
        this.target = v;
        this.anchorPosition = anchorPosition;
    }
}
