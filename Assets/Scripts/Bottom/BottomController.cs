﻿using UnityEngine;
using System.Collections;

public class BottomController : MonoBehaviour {
    [SerializeField] private GameObject fish;
    [SerializeField] private GameObject mole;
    [SerializeField] private Vector3[] bottomCenterPosition;
    [SerializeField] private Vector3[] bottomRightPosition;
    [SerializeField] private Vector3[] bottomLeftPosition;
    private bool isActivateObjects;
    void Update(){
        if (!this.isActivateObjects && (ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_CENTER) ||
                                        ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_RIGHT) ||
                                        ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_LEFT))) {
            ActivateObject();

        }else if (this.isActivateObjects &&
                  (ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.CENTER) ||
                   ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.RIGHT) ||
                   ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.LEFT))) {
            this.DeactivateObjects();
        }
    }

    private void ActivateObject() {
        int i = Random.Range(0, 2);
        int scaleRandom = Random.Range(0, 2);
        int scale;
        if (scaleRandom.Equals(0)) {
            scale = -1;
        }
        else {
            scale = 1;
        }
        this.isActivateObjects = true;
        switch (i) {
            case 0:
                fish.SetActive(true);
                fish.transform.localScale = new Vector3(scale, fish.transform.localScale.y, fish.transform.localScale.z);
                if (ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_CENTER)){
                    this.fish.transform.localPosition = this.bottomCenterPosition[Random.Range(0, this.bottomCenterPosition.Length)];
                }else if (ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_RIGHT)) {
                    this.fish.transform.localPosition = this.bottomRightPosition[Random.Range(0, this.bottomRightPosition.Length)];
                }else if (ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_LEFT)) {
                    this.fish.transform.localPosition = this.bottomLeftPosition[Random.Range(0, this.bottomLeftPosition.Length)];
                }
            break;
            case 1:
                this.mole.SetActive(true);
                this.mole.transform.localScale = new Vector3(scale, mole.transform.localScale.y, mole.transform.localScale.z);
                if (ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_CENTER)){
                    this.mole.transform.localPosition = this.bottomCenterPosition[Random.Range(0, this.bottomCenterPosition.Length)];
                }else if (ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_RIGHT)) {
                    this.mole.transform.localPosition = this.bottomRightPosition[Random.Range(0, this.bottomRightPosition.Length)];
                }else if (ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.BOTTOM_LEFT)) {
                    this.mole.transform.localPosition = this.bottomLeftPosition[Random.Range(0, this.bottomLeftPosition.Length)];
                }
            break;
            default:
            break;
        }
    }

    private void DeactivateObjects() {
        this.isActivateObjects = false;
        if (this.fish.activeSelf) {
            this.fish.SetActive(false);
        }

        if (this.mole.activeSelf) {
            this.mole.SetActive(false);
        }
    }

}
