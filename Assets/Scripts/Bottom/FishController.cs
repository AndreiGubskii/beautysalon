﻿using UnityEngine;
using System.Collections;

public class FishController : MonoBehaviour {
    [SerializeField] private GameObject fish;
    private Animator _animator;
    private int outHash;
    private int backHash;
    private bool fishIsGotOut;
    private bool blocked;
    private void OnEnable() {
        this.fishIsGotOut = true;
        this.blocked = true;
        this.outHash = Animator.StringToHash("out");
        this.backHash = Animator.StringToHash("back");
        if (this.fish != null) {
            this._animator = this.fish.GetComponent<Animator>();
        }
        StartCoroutine(this.StartAnimation());
    }

    private void PlayAnimationOut() {
        if (this._animator != null) {
            this._animator.SetTrigger(this.outHash);
        }
        this.fishIsGotOut = true;
    }

    private void PlayAnimationBack() {
        if (this._animator != null) {
            this._animator.SetTrigger(this.backHash);
        }
        this.fishIsGotOut = false;
    }

    private void OnMouseDown() {
        if (this.blocked) return;
        if (this.fishIsGotOut) {
            this.PlayAnimationBack();
        }
        else {
            this.PlayAnimationOut();
        }
    }

    private IEnumerator StartAnimation() {
        yield return new WaitForSeconds(0.5f);
        this.PlayAnimationOut();
        yield return new WaitForSeconds(1f);
        this.PlayAnimationBack();
        yield return new WaitForSeconds(1f);
        this.blocked = false;
    }
}
