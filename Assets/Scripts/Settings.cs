﻿using System;
using UnityEngine;
using System.Collections;

public class Settings : MonoBehaviour {
    [System.Serializable]
    public class SpriteForWishCloud{
        public GameObject sharpenNailsMale;
        public GameObject sharpenNailsFamale;

        public GameObject cleanFace;
        public GameObject nose;
        public GameObject lipstick;
        public GameObject parfume;
        public GameObject mask;
    }

    [SerializeField] private GameObject dirtySprite;

    public static Settings instance;

    private void Awake() {
        instance = this;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private void Start() {
        AudioManager.instance.PlayMusic("gamePlay");
    }

    [SerializeField]
    private SpriteForWishCloud wishCloudSprite;

    public SpriteForWishCloud GetWishCloudSprite() {
        return this.wishCloudSprite;
    }

    public GameObject GetDirtySprite() {
        return this.dirtySprite;
    }
}
