﻿using UnityEngine;
using System.Collections;

public class CompletionInteractionController : MonoBehaviour {
    [SerializeField] private Transform[] spawns;
    [SerializeField] private GameObject[] objects;

    private void Start() {
        if (this.gameObject.activeSelf) {
            this.gameObject.SetActive(false);
        }
    }

    private void OnEnable() {
        InvokeRepeating("Creat",0.1f,0.1f);
    }

    private void OnDisable() {
        CancelInvoke("Creat");
    }

    private void Creat() {
        GameObject obj = (GameObject)Instantiate(this.objects[Rnd(this.objects.Length)], 
            this.spawns[this.Rnd(this.spawns.Length)].position, Quaternion.identity);
        obj.transform.SetParent(this.transform);

        Canvas canvas = obj.GetComponent<Canvas>();
        if (canvas == null) {
            canvas = obj.AddComponent<Canvas>();
            canvas.overrideSorting = true;
            canvas.sortingOrder = 30;
        }else {
            canvas.overrideSorting = true;
            canvas.sortingOrder = 30;
        }
        Destroy(obj,5f);
    }

    private int Rnd(int length) {
        return Random.Range(0, length);
    }
}
