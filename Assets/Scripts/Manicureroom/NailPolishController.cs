﻿using UnityEngine;
using System.Collections;

public class NailPolishController : MonoBehaviour {
    public delegate void NailPolishEvents();
    public event NailPolishEvents NailPainted;
    private Vector3 target;
    private bool move;
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private Vector3 endPosition;
    [SerializeField] private float speed;
    private bool isActive;
    private bool painted, holdBrush, nailPainted;
    public static NailPolishController instance;
    private Canvas[] canvases;

    private void OnEnable() {
        if (SystemInfo.deviceModel.Contains("iPad")) {
            endPosition = new Vector3(30f,540f,1f);
        }
        this.transform.localPosition = this.startPosition;
        this.nailPainted = false;
        this.SetIsPainted(false);
        this.SetIsHoldBrush(false);
        instance = this;
        StartCoroutine(ResetOverrideSorting());
    }

    private Canvas[] GetChildCanvases() {
        return canvases != null ? canvases : (canvases = transform.GetComponentsInChildren<Canvas>());
    }

    //Костыль. При переходе на версию Unity 5.5 возникла проблема с объектами на которых повешен canvas 
    //данный костыль решает данную проблему
    private IEnumerator ResetOverrideSorting() {
        Canvas[] canvases = GetChildCanvases();
        foreach (Canvas canvas in canvases) {
            canvas.overrideSorting = false;
        }
        yield return new WaitForSeconds(0.1f);
        foreach (Canvas canvas in canvases){
            canvas.overrideSorting = true;
        }
    }

    private void Update() {
        if (move) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                speed * Time.deltaTime);
        }

        if (this.transform.localPosition.Equals(this.target) && this.move) {
            this.move = false;
        }

        if (!this.GetIsHoldBrush() && this.GetIsPainted() && !nailPainted) {
            if (this.NailPainted != null) {
                this.NailPainted();
            }
            this.nailPainted = true;
        }
    }

    public void MoveTo(Vector3 v) {
        this.move = true;
        this.target = v;
    }

    public void Activate() {
        this.SetIsActive(true);
        this.MoveTo(this.endPosition);
    }

    public void Deactivate() {
        this.SetIsActive(false);
        this.MoveTo(this.startPosition);
    }

    public bool GetIsActive() {
        return this.isActive;
    }

    private void SetIsActive(bool var) {
        this.isActive = var;
    }

    public bool GetIsHoldBrush() {
        return this.holdBrush;
    }

    public void SetIsHoldBrush(bool var) {
        this.holdBrush = var;
    }

    public bool GetIsPainted() {
        return this.painted;
    }

    public void SetIsPainted(bool var) {
        this.painted = var;
    }
}
