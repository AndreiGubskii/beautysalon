﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DuplicateStickers : MonoBehaviour {

    private bool press;
    private bool inNail;
    private GameObject nail;
    private bool scale;
    private Vector3 scaleTarget = new Vector3(0.5f, 0.5f, 0.5f);
    private Vector3 target;
    private NailScript nailScript;
    private void OnEnable() {
        scale = false;
    }

    private void OnDisable() {
        Destroy(this.gameObject);
    }

    void Update () {
	    if (!Input.GetMouseButton(0) && this.inNail && this.transform.parent != this.nail.transform) {
	        this.transform.SetParent(this.nail.transform);
	        if (nailScript != null) {
                Vector3 targ = nailScript.Target();
	            if (targ == Vector3.zero) {
                    Destroy(gameObject);
	                return;
	            }
	            this.transform.localPosition = targ;
	        }
	        else {
	            Destroy(gameObject);
            }
	        this.scale = true;
            GetComponent<Stickers>().enabled = false;
	    }
	    else if(!Input.GetMouseButton(0) && !this.inNail){
	        Destroy(gameObject);
        }
	    if (this.scale) {
            this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, this.scaleTarget, 5 * Time.deltaTime);
	    }

        if (this.transform.localScale.x <= this.scaleTarget.x && this.scale){
            this.scale = false;
        }
	}

    private void OnTriggerEnter(Collider col) {
        Image img = col.gameObject.GetComponent<Image>();
        if (img!=null && img.sprite.name.Equals("nail")) {
            this.nailScript = col.gameObject.GetComponent<NailScript>();
            if (this.nailScript == null) {
                this.nailScript = col.gameObject.AddComponent<NailScript>();
            }
            this.inNail = true;
            this.nail = col.gameObject;
        }
    }

    private void OnTriggerExit(Collider col) {
        SpriteRenderer sp = col.gameObject.GetComponent<SpriteRenderer>();
        if (sp!=null && sp.sprite.name.Equals("nail")) {
            this.inNail = false;
            this.nail = null;
        }
    }
}
