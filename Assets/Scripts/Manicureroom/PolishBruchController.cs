﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PolishBruchController : MonoBehaviour,IDragHandler,IPointerUpHandler,IPointerDownHandler {
    private bool pressed;

    private Vector3 startPosition;
    [SerializeField] private GameObject polishTexture;
    [SerializeField] private Color polishColor;
    private Vector3 defaultRotation = new Vector3(0f,0f,-60f);
    private Vector3 tapRotation = new Vector3(0f, 0f, 60f);
    private RectTransform rectTransform;
    private void Start() {
        this.rectTransform = GetComponent<RectTransform>();
        this.startPosition = transform.localPosition;
    }

    private void Update() {
        if (this.transform.localPosition != this.startPosition && !this.pressed){
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.startPosition, 2000 * Time.deltaTime);
        }
    }

    public void OnDrag(PointerEventData eventData) {
        transform.position = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData) {
        this.pressed = false;
        this.rectTransform.Rotate(this.defaultRotation);
        if (NailPolishController.instance.GetIsPainted() && NailPolishController.instance.GetIsHoldBrush()) {
            NailPolishController.instance.SetIsHoldBrush(false);
        }
    }

    public void OnPointerDown(PointerEventData eventData){
        this.rectTransform.Rotate(this.tapRotation);
        this.pressed = true;
        if (!NailPolishController.instance.GetIsPainted() && !NailPolishController.instance.GetIsHoldBrush()) {
            NailPolishController.instance.SetIsHoldBrush(true);
        }
    }

    public Color GetPolishColor() {
        return this.polishColor;
    }

    public GameObject GetPolish() {
        return this.polishTexture;
    }
}
