﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NailFile : MonoBehaviour,IPointerDownHandler,IPointerUpHandler,IDragHandler {
    public delegate void NailFileEvent();

    public event NailFileEvent SharpenedNails;
    private List<GameObject> finished;
    private Vector3 target;
    private int finishedCount;
    private bool move;
    private bool press;
    private bool caneTake;
    private Vector3 endPosition = new Vector3(1400f, 2.93f, 0f);
    public static NailFile instance;
    [SerializeField] private GameObject shavings;
    private void OnEnable() {
        this.caneTake = true;
        this.move = false;
        this.finishedCount = 0;
        instance = this;
        finished = new List<GameObject>();
        this.transform.localPosition = this.endPosition;
    }

    void Update () {
        if (this.finished.Count >= this.finishedCount && this.finishedCount != 0){
            this.finished = new List<GameObject>();
            MoveTo(this.endPosition);
            this.caneTake = false;
            if (this.SharpenedNails != null) {
                this.SharpenedNails();
            }
        }
        if (this.press && !this.move){
//            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//            RaycastHit hit;
//            if (Physics.Raycast(ray, out hit, 100f)){
//                transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
//            }
        }

        if (move) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                2000f * Time.deltaTime);
        }

        if (this.transform.localPosition.Equals(this.target) && this.move) {
            this.move = false;
        }
	}

    private void OnTriggerEnter(Collider col) {
        if (!this.press) return;
        Image img = col.gameObject.GetComponent<Image>();
        if (img != null && img.sprite!= null && img.sprite.name.Equals("nail")) {
            if (img.color.r < 0.95/*!img.color.Equals(Color.white)*/){
                img.color = Color.Lerp(img.color, Color.white, 20f*Time.deltaTime);
                for (int i = 0; i < 5; i++) {
                    GameObject obj = Instantiate(shavings);
                    obj.transform.SetParent(img.gameObject.transform);
                    obj.transform.localScale = Vector3.one;
                    obj.transform.localPosition = new Vector3(Random.Range(-80,80),Random.Range(80,-20),0);
                }
            }

            if (/*img.color == Color.white*/img.color.r > 0.95) {
                if (finished.Find(mc => mc.Equals(img.gameObject)) != img.gameObject) {
                    finished.Add(img.gameObject);
                }
            }

            if (!InteractionManager.instance.GetInteractionWas()) {
                InteractionManager.instance.SetInteractionWas(true);
            }
        }
    }

    public void MoveTo(Vector3 v) {
        this.move = true;
        this.target = v;
    }

    public void MoveTo(Vector3 v, float delay) {
        this.move = true;
        this.target = v;
    }

    private void ResetParent() {
        if (this.transform.parent.Equals(ManicureroomController.instance.gameObject.transform)) {
            return;
        }
        this.transform.SetParent(ManicureroomController.instance.gameObject.transform);
    }

    public void SetFinishedCount(int i) {
        this.finishedCount = i;
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (this.finishedCount == 0) {
            this.finishedCount = ManicureroomController.instance.GetCountNails();
        }
        ResetParent();
        this.press = true;
    }

    public void OnPointerUp(PointerEventData eventData) {
        this.press = false;
    }

    public void OnDrag(PointerEventData eventData) {
        if (!this.caneTake) return;
        this.transform.position = eventData.position;
    }
}
