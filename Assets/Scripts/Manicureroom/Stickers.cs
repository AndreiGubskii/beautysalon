﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Stickers : MonoBehaviour,IDragHandler,IPointerDownHandler {
    [SerializeField] private GameObject duplicateSticker;
    private Image spriteRenderer;
    //private SpriteRenderer duplicateStickerSpriteRenderer;
    private void OnEnable() {
        this.spriteRenderer = this.GetComponent<Image>();
        //this.duplicateStickerSpriteRenderer = this.duplicateSticker.GetComponent<SpriteRenderer>();
    }

    public void OnDrag(PointerEventData eventData) {
        this.transform.position = eventData.position;
    }

    public void OnPointerDown(PointerEventData eventData) {
        GameObject obj = Instantiate(this.duplicateSticker);
        obj.GetComponent<Image>().sprite = this.spriteRenderer.sprite;
        //obj.GetComponent<SpriteRenderer>().sortingOrder = this.spriteRenderer;

        obj.transform.SetParent(this.transform.parent);
        obj.transform.localScale = new Vector3(1f, 1f, 1f);
        obj.GetComponent<Image>().SetNativeSize();
        obj.transform.localPosition = this.transform.localPosition;
        obj.AddComponent<Stickers>().duplicateSticker = this.duplicateSticker;
        this.gameObject.AddComponent<DuplicateStickers>();
        this.gameObject.transform.SetParent(transform.parent.parent.parent);
        BoxCollider box = this.gameObject.AddComponent<BoxCollider>();
        box.isTrigger = true;
        box.size = new Vector3(150f, 100f, 10f);
        this.gameObject.AddComponent<Rigidbody>().isKinematic = true;
    }
}
