﻿using UnityEngine;
using System.Collections;

public class HandManicureController : MonoBehaviour {
    [SerializeField] private GameObject[] nails;
    [SerializeField] private NailOutgrowth[] nailsOutgrowth;
    public bool ismale;
    private void Start() {
        foreach (GameObject nail in nails) {
            if (nail.GetComponent<BoxCollider>() == null) {
                BoxCollider boxcol = nail.AddComponent<BoxCollider>();
                boxcol.size = new Vector3(150f,100f,10f);
            }
            if (nail.GetComponent<Canvas>() == null && this.GetIsMaleHand()) {
                Canvas canvas = nail.AddComponent<Canvas>();
                canvas.overrideSorting = true;
                canvas.sortingOrder = 2;
            }
        }
        foreach (NailOutgrowth nailOutgrowth in nailsOutgrowth) {
            GameObject obj = nailOutgrowth.gameObject;
            if (obj.GetComponent<Canvas>() == null && this.GetIsMaleHand()){
                Canvas canvas = obj.AddComponent<Canvas>();
                canvas.overrideSorting = true;
                canvas.sortingOrder = 3;
            }
        }
    }

    public GameObject[] GetNails() {
        return this.nails;
    }

    public NailOutgrowth[] GetNailsOutgrowth() {
        return this.nailsOutgrowth;
    }

    public bool GetIsMaleHand() {
        return this.ismale;
    }

    public void SetIsMaleHand(bool var) {
        this.ismale = var;
    }
}
