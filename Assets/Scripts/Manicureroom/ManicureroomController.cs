﻿using UnityEngine;
using UnityEngine.UI;

public class ManicureroomController : MonoBehaviour {
    [System.Serializable]public class Hands {
        public Vector3 leftPosition;
        public Vector3 rightPosition;
        public Vector3 scale;
        public CurlingNailsController.Positions[] positions;
    }
    public static ManicureroomController instance;
    private VisitorController visitorController;
    [SerializeField] private GameObject[] nails;
    [SerializeField] private NailOutgrowth[] nailOutgrowth;
    [SerializeField] private HandController hand;
    [SerializeField] private Hands kroshHands;
    [SerializeField] private Hands steshaHands;
    [SerializeField] private Hands losyashHands;
    [SerializeField] private Hands sovunyaHands;
    [SerializeField] private Hands kopatichHands;
    [SerializeField] private Hands igogoshaHands;
    private int countNails = 0;
    private GameObject leftHand;
    private GameObject rightHand;
    private void OnEnable() {
        GetComponent<Image>().sprite = Resources.Load<Sprite>("Art/ManicureRoom/Background_nails");
        Physics.gravity = new Vector3(0f,-100f,0);
        instance = this;
    }

    private void SetNailOutgrowth(GameObject rightHands,GameObject leftHands) {
        for (int i = 0; i < leftHands.GetComponent<HandManicureController>().GetNailsOutgrowth().Length; i++){
            this.nailOutgrowth[i] = leftHands.GetComponent<HandManicureController>().GetNailsOutgrowth()[i];
        }
        int j = 0;
        for (int i = leftHands.GetComponent<HandManicureController>().GetNailsOutgrowth().Length; 
            i < rightHands.GetComponent<HandManicureController>().GetNailsOutgrowth().Length * 2; i++){
                this.nailOutgrowth[i] = rightHands.GetComponent<HandManicureController>().GetNailsOutgrowth()[j];
                j++;
        }
        
        this.NailOutgrowthActiveSelf(false);
    }

    private void SetNail(GameObject rightHands,GameObject leftHands) {
        for (int i = 0; i < leftHands.GetComponent<HandManicureController>().GetNails().Length; i++){
            this.nails[i] = leftHands.GetComponent<HandManicureController>().GetNails()[i];
        }
        int j = 0;
        for (int i = leftHands.GetComponent<HandManicureController>().GetNails().Length;
            i < rightHands.GetComponent<HandManicureController>().GetNails().Length * 2; i++){
                this.nails[i] = rightHands.GetComponent<HandManicureController>().GetNails()[j];
            j++;
        }
    }

    private void OnDisable() {
        GetComponent<Image>().sprite = null;
        Physics.gravity = new Vector3(0f, -9.81f, 0);
        if (this.rightHand != null && this.leftHand != null) {
            Destroy(this.leftHand);
            Destroy(this.rightHand);
        }
        Resources.UnloadUnusedAssets();
    }


    public void ActivateService(VisitorController visitor) {
        this.visitorController = visitor;
        this.CreatHands(visitor);

        switch (visitor.GetVisitorSex()) {
                case VisitorController.VisitorSex.FEMALE:
                this.ActivateNailFile();
                break;
                case VisitorController.VisitorSex.MALE:
                //ActivateNailSticker();        
                this.ActivateNailClippers();
                break;
        }
    }

    public NailOutgrowth[] GetNailOutgrowths() {
        return this.nailOutgrowth;
    }

    public void ActivateNailClippers() {
        CurlingNailsController.instance.TrimmedNails += TrimmedNailsListener;
        this.NailChangeColor(new Color(0.65f, 0.65f, 0.65f));
        this.NailOutgrowthActiveSelf(true);
        this.hand.BringCurlingNails();
    }

    public void ActivateNailFile() {
        NailFile.instance.SharpenedNails += this.SharpenedNailsListener;
        this.NailChangeColor(new Color(0.65f, 0.65f, 0.65f));
        this.hand.BringNailFile();
        foreach (GameObject nail in nails) {
            nail.GetComponent<Mask>().enabled = false;
        }
    }

    public void ActivateNailSticker() {
        NailStickerController.instance.Activate();
    }

    public void ActivateNailPolish() {
        NailPolishController.instance.NailPainted += this.PaintedNailsListener;
        NailPolishController.instance.Activate();
        foreach (GameObject nail in nails) {
            nail.GetComponent<Mask>().enabled = true;
        }
    }

    private void NailOutgrowthActiveSelf(bool b) {
        foreach (NailOutgrowth nail in nailOutgrowth){
            if (nail != null) {
                nail.gameObject.SetActive(b);
            }
        }
    }

    private void NailChangeColor(Color c) {
        foreach (GameObject nail in nails){
            if (nail != null) {
                nail.GetComponent<Image>().color = c;
            }
        }
    }

    private void TrimmedNailsListener() {
        Invoke("ActivateNailFile",1.5f);
        CurlingNailsController.instance.TrimmedNails -= TrimmedNailsListener;
    }

    private void SharpenedNailsListener() {
        NailFile.instance.SharpenedNails -= this.SharpenedNailsListener;
        if (this.visitorController.GetVisitorSex().Equals(VisitorController.VisitorSex.MALE)) {
            InteractionManager.instance.DeactivateInteraction();
        }
        else if (this.visitorController.GetVisitorSex().Equals(VisitorController.VisitorSex.FEMALE)) {
            //Invoke("ActivateNailSticker",1f);
            Invoke("ActivateNailPolish", 1f);
        }
    }

    private void PaintedNailsListener() {
        NailPolishController.instance.NailPainted -= this.PaintedNailsListener;
        CanvasController.instance.ActivateNextInteractionBtn(InteractionManager.InteractionName.NAILS_STICKER,1f);
    }

    public int GetCountNails() {
        return this.countNails;
    }

    public void ActivateInteractionWithDelay(string interactionName, float delay = 1f) {
        Invoke(interactionName,delay);
    }

    private void CreatHands(VisitorController v) {
        Hands hands = new Hands();
        VisitorController.VisitorType type = v.GetVisitorType();
        string handsPath = "Prefabs/Hands/";
        switch (type) {
            case VisitorController.VisitorType.KROSH:
                hands = this.kroshHands;
                this.SetHands(Resources.Load<GameObject>(handsPath+"Krosh/right"), 
                    Resources.Load<GameObject>(handsPath + "Krosh/left"));
            break;
            case VisitorController.VisitorType.STEPANIDA:
                this.SetHands(Resources.Load<GameObject>(handsPath + "Stesha/right"),
                    Resources.Load<GameObject>(handsPath + "Stesha/left"));
                hands = this.steshaHands;
                if (SystemInfo.deviceModel.Contains("iPad")) {
                    hands.scale = new Vector3(1f, 1f, 1f);
                }
                break;
            case VisitorController.VisitorType.LOSYASH:
                this.SetHands(Resources.Load<GameObject>(handsPath + "Losyash/right"),
                    Resources.Load<GameObject>(handsPath + "Losyash/left"));
                hands = this.losyashHands;
            break;
            case VisitorController.VisitorType.SOVUNYA:
                this.SetHands(Resources.Load<GameObject>(handsPath + "Sovunya/right"),
                    Resources.Load<GameObject>(handsPath + "Sovunya/left"));
                hands = this.sovunyaHands;
            break;
            case VisitorController.VisitorType.KOPATICH:
                this.SetHands(Resources.Load<GameObject>(handsPath + "Kopatich/right"),
                    Resources.Load<GameObject>(handsPath + "Kopatich/left"));
                hands = this.kopatichHands;
            break;
            case VisitorController.VisitorType.IGOGOSHA:
                this.SetHands(Resources.Load<GameObject>(handsPath + "Igogosha/right"),
                    Resources.Load<GameObject>(handsPath + "Igogosha/left"));
                hands = this.igogoshaHands;
            break;
            default:
                this.SetHands(Resources.Load<GameObject>(handsPath + "Stesha/right"),
                    Resources.Load<GameObject>(handsPath + "Stesha/left"));
                hands = this.steshaHands;
            break;
        }
        CurlingNailsController.instance.SetPositions(hands.positions);
        this.countNails = hands.positions.Length;
        this.nailOutgrowth = new NailOutgrowth[this.countNails];
        this.nails = new GameObject[this.countNails];

        this.SetNail(this.rightHand,this.leftHand);
        this.SetNailOutgrowth(this.rightHand, this.leftHand);
        this.rightHand.transform.SetParent(this.transform.GetChild(0).transform);
        this.leftHand.transform.SetParent(this.transform.GetChild(0).transform);
        this.rightHand.transform.localPosition = hands.rightPosition;
        this.leftHand.transform.localPosition = hands.leftPosition;
        this.rightHand.transform.localScale = hands.scale;
        this.leftHand.transform.localScale = hands.scale;
    }

    private void SetHands(GameObject right, GameObject left) {
        this.leftHand = Instantiate(left);
        this.rightHand = Instantiate(right);
    }
}
