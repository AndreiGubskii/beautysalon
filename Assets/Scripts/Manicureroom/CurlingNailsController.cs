﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CurlingNailsController : MonoBehaviour,IPointerDownHandler {
    public delegate void CurlingNailsEvent();

    public event CurlingNailsEvent TrimmedNails;
    [Serializable]
    public class Positions {
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;
    }

    public static CurlingNailsController instance;
    private Animator _animator;
    private Vector3 target;
    private bool move;
    private float delayBeforeMove = 0f;
    private float timeBeforeMove = 0f;
    private int nailid = 0;
    private Vector3 endPosition = new Vector3(1400f,-2f,0f);
    [SerializeField] private Positions[] positions;

    public static CurlingNailsController GetInstance() {
        return instance;
    }

    private void Update() {
        if (move && (this.timeBeforeMove + this.delayBeforeMove) <= Time.time) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                2000*Time.deltaTime);
        }

        if (this.transform.localPosition.Equals(this.target) && this.move) {
            this.move = false;
            this.delayBeforeMove = 0f;
        }
    }

    public Animator GetAnimator() {
        return _animator ?? (_animator = GetComponent<Animator>());
    }

    void OnEnable () {
        instance = this;
        this.nailid = 0;
        this.transform.localPosition = this.endPosition;
    }

    public void Play() {
        this.GetAnimator().SetTrigger("cut");
        AudioManager.instance.GetSoundSource().
            PlayOneShot(AudioManager.instance.GetSoundByName("cut"));
    }

    private IEnumerator ChangeIEnumerator(int i,float delay) {
        yield return new WaitForSeconds(delay);
        if (i >= ManicureroomController.instance.GetNailOutgrowths().Length) yield break;
        if (this.positions[i] != null) {
            MoveTo(this.positions[i].position);
            this.transform.localRotation = Quaternion.Euler(this.positions[i].rotation);
            this.transform.localScale = this.positions[i].scale;
        }
    }

    public void MoveTo(Vector3 v) {
        this.move = true;
        this.target = v;
    }

    public void MoveTo(Vector3 v, float delay){
        this.move = true;
        this.target = v;
        this.delayBeforeMove = delay;
        this.timeBeforeMove = Time.time;
    }

    private void ResetParent() {
        if (this.transform.parent.Equals(ManicureroomController.instance.gameObject.transform)) {
            return;
        }
        this.transform.SetParent(ManicureroomController.instance.gameObject.transform);
    }

    public void SetPositions(Positions[] p) {
        this.positions = p;
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (this.move) return;
        if (this.nailid == (ManicureroomController.instance.GetNailOutgrowths().Length - 1)) {
            this.MoveTo(this.endPosition,0.5f);
            if (this.TrimmedNails != null) {
                this.TrimmedNails();
            }
        }
        ResetParent();
        if (this.transform.localPosition != this.positions[nailid].position) {
            StartCoroutine(ChangeIEnumerator((this.nailid),0.1f));
            return;
        }
        ManicureroomController.instance.GetNailOutgrowths()[this.nailid].TrimmedNails();
        //this.Play();
        StartCoroutine(ChangeIEnumerator((this.nailid+1),0.5f));
        this.nailid++;
        if (!InteractionManager.instance.GetInteractionWas()) {
            InteractionManager.instance.SetInteractionWas(true);
        }
    }
}
