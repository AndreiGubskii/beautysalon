﻿using UnityEngine;
using System.Collections;
using System.Linq.Expressions;
using UnityEngine.UI;

public class NailOutgrowth : MonoBehaviour {

    private Vector3 startPosition;
    private Vector3 startRotation;
    public bool cut;
    private Rigidbody rb;
    private float _y;
    private float y_speed;
    private float x_speed = 400f;
    private int z_rotation;
    private int[] rotationValue = new[] {-300, -400, -500, 500, 400, 300};
    private void Awake() {
        this.rb = null;
    }

	void OnEnable () {
	    this._y = this.transform.localPosition.y;
	    z_rotation = rotationValue[Random.Range(0,rotationValue.Length)];
        
	    gameObject.GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f);
        this.startPosition = transform.localPosition;
	    this.startRotation = transform.localRotation.eulerAngles;
        this.cut = false;
        this.GetRB().velocity = Vector3.zero;
        this.GetRB().useGravity = false;
    }

    private void OnDisable() {
        //this.GetRB().useGravity = false;
        this.transform.localPosition = this.startPosition;
        this.transform.localRotation = Quaternion.Euler(startRotation);
    }
    void Update () {
	    if (transform.localPosition.y < -700 && this.gameObject.activeSelf) {
            //this.GetRB().useGravity = false;
            this.transform.localPosition = this.startPosition;
            this.cut = false;
            this.gameObject.SetActive(false);
	    }

        if (this.cut) {
            //this.cut = false;
            if (this.transform.localPosition.y < (_y + 200) && y_speed>=0) {
                y_speed = 800;
            }
            else {
                y_speed = -1000;
            }
            this.transform.localPosition = 
                new Vector3(this.transform.localPosition.x + x_speed*Time.deltaTime,this.transform.localPosition.y+y_speed * Time.deltaTime);
            this.transform.localRotation = 
                Quaternion.Euler(0f,0f, this.transform.localRotation.eulerAngles.z+z_rotation * Time.deltaTime);

            //this.GetRB().AddForce((Vector3.up * 2000f + Vector3.right * 500f));
            //this.GetRB().useGravity = true;
	    }
    }

    public void TrimmedNails() {
        this.cut = true;
        CurlingNailsController.GetInstance().Play();
    }

    private Rigidbody GetRB() {
        return this.rb ?? (this.rb = this.GetComponent<Rigidbody>());
    }
}
