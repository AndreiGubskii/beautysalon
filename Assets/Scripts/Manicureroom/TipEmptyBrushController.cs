﻿using UnityEngine;
using System.Collections;

public class TipEmptyBrushController : MonoBehaviour {
    private void OnTriggerEnter(Collider col) {
        if (col.gameObject.name.Equals("nailPolishBubble(Clone)")) {
            Destroy(col.gameObject);
        }
    }
}
