﻿using UnityEngine;
using UnityEngine.UI;

public class paint : MonoBehaviour {
    private GameObject polishTexture;

    public void MouseDrag() {
        GameObject obj = Instantiate(polishTexture);
        obj.transform.SetParent(this.transform);
        obj.transform.localScale = this.polishTexture.transform.localScale;
        obj.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
    }

    private void OnTriggerEnter(Collider col) {
        if(!col.name.Equals("tipBrush")) return;
        this.polishTexture = col.GetComponentInParent<PolishBruchController>().GetPolish();
    }

    private Vector3 tipPosition = new Vector3(0f,0f,0f);
    
    private void OnTriggerStay(Collider col) {
        if (this.tipPosition.Equals(col.transform.position) || !col.name.Equals("tipBrush")) return;
        this.tipPosition = col.transform.position;
        GameObject obj = Instantiate(polishTexture);
        obj.GetComponent<Image>().color = col.GetComponentInParent<PolishBruchController>().GetPolishColor();
        obj.transform.SetParent(this.transform);
        obj.transform.localScale = this.polishTexture.transform.localScale;
        obj.transform.position = tipPosition;
        if (!NailPolishController.instance.GetIsPainted()) {
            NailPolishController.instance.SetIsPainted(true);
        }
    }
}
