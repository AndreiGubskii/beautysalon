﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class NailStickerController : MonoBehaviour {
    [SerializeField] private Vector3[] targetsDuplicateStickers;
    private Vector3 target;
    private bool move;
    private Vector3 startPosition = new Vector3(-1900f,390f,0f);
    private Vector3 endPosition = new Vector3(100f, 390f, 0f);
    public static NailStickerController instance;
    
    private void OnEnable() {
        if (SystemInfo.deviceModel.Contains("iPad"))
        {
            startPosition = new Vector3(-1900f, 540f, 0f);
            endPosition = new Vector3(100f, 540f, 1f);
        }
        this.transform.localPosition = this.startPosition;
        instance = this;
    }

    private void Update() {
        
        if (move) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                2000 * Time.deltaTime);
        }

        if (this.transform.localPosition.Equals(this.target) && this.move) {
            this.move = false;
            if (this.target.Equals(this.endPosition)) {
                CanvasController.instance.SetScaleTimer();
            }
        }
    }

    public void MoveTo(Vector3 v) {
        this.move = true;
        this.target = v;
    }

    public void Activate() {
        this.MoveTo(this.endPosition);
    }

    public void Deactivate() {
        this.MoveTo(this.startPosition);
    }

    public Vector3[] GetTargetsDuplicateStickers() {
        return this.targetsDuplicateStickers;
    }

}
