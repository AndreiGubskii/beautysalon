﻿using UnityEngine;
using System.Collections;

public class Loading : MonoBehaviour {
    [SerializeField] private float delay;
	IEnumerator Start () {
        AudioManager.instance.PlayMusic("logo");
	    yield return new WaitForSeconds(this.delay);
	    Application.LoadLevelAsync("main");
	}

}
