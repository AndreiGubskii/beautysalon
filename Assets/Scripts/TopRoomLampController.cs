﻿using UnityEngine;
using System.Collections;

public class TopRoomLampController : MonoBehaviour {
    [SerializeField] private SpriteRenderer light;
    [SerializeField] private SpriteRenderer shadow;

    private void FixedUpdate() {
        if (this.light != null && this.light.enabled && 
            ChangingRoom.instance.GetInRoom() == ChangingRoom.Rooms.CENTER){
            this.OnMouseDown();
        }
    }

    private void OnMouseDown() {
        AudioManager.instance.GetSoundSource()
            .PlayOneShot(AudioManager.instance.GetSoundByName("click"));
        if (this.light != null){
            if (this.light.enabled) {
                this.light.enabled = false;
                this.light.sprite = null;
            }
            else {
                this.light.sprite = Resources.Load<Sprite>("Art/TopRoom/room_4_slifht");
                this.light.enabled = true;
            }
        }

        if (this.shadow != null){
            if (this.shadow.enabled) {
                this.shadow.enabled = false;
                this.shadow.sprite = null;
                Resources.UnloadUnusedAssets();
            }
            else {
                this.shadow.sprite = Resources.Load<Sprite>("Art/TopRoom/room_4_shadow");
                this.shadow.enabled = true;
            }
        }
    }
}
