﻿using UnityEngine;
using System.Collections;

public class TreeController : MonoBehaviour {
    private Animator _animator;

    private void Start() {
        this._animator = GetComponent<Animator>();
    }

    private void OnMouseDown() {
        this.PlayAnimation();
    }

    private void PlayAnimation() {
        this._animator.SetTrigger("play");
    }
}
