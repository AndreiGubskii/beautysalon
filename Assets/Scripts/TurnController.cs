﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using System.Collections;
using System.Linq;
using Random = UnityEngine.Random;

public class TurnController : MonoBehaviour {
    public enum WishInThisRoom {
        DEFAULT,
        RIGHT,
        LEFT,
        TOP
    }
    [System.Serializable]
    public class HoldingPoint {
        public Vector3 target;
        public Vector3[] targets;
        public bool busy;
    }

    public class RightRoom {
        public WishController.WishType[] wishTypes = {
            WishController.WishType.LIPSTICK,
            WishController.WishType.SHARPEN_NAILS
        };
        public bool busy = false;
    }

    public class LeftRoom {
        public WishController.WishType[] wishTypes = {
            WishController.WishType.HAIR_PULLED,
            WishController.WishType.CLEAN_FACE
        };
        public bool busy = false;
    }

    public class TopRoom {
        public WishController.WishType[] wishTypes = {
            WishController.WishType.PARFUM_DRESS_UP,
            WishController.WishType.PARFUM_ROUGE,
            WishController.WishType.MASK
        };
        public bool busy = false;
    }
    private Dictionary<VisitorController.VisitorType, WishController.WishType> visitorWishesDictionary =
        new Dictionary<VisitorController.VisitorType, WishController.WishType>();

    private WishController.WishType[] activeWishType = {WishController.WishType.DEFAULT,WishController.WishType.DEFAULT};
    private WishInThisRoom[] activeWishInRoom = { WishInThisRoom.DEFAULT, WishInThisRoom.DEFAULT};

    private RightRoom rightRoom = new RightRoom();
    private LeftRoom leftRoom = new LeftRoom();
    private TopRoom topRoom = new TopRoom();

    [SerializeField] private GameObject[] visitors;
    [SerializeField] private VisitorsTypes[] iconVisitors;
    [SerializeField] private GameObject nusha;
    private VisitorController.VisitorType visitorTypeInLeftRoom;
    private VisitorController.VisitorType visitorTypeInRightRoom;
    private VisitorController.VisitorType visitorTypeInTopRoom;
    private NushaAnimationController nushaAnimatorController;
    private List<GameObject> visitorList = new List<GameObject>();
    private float timerCreator;
    private float timeCreator = 5f;
    public static TurnController instance;
    public HoldingPoint[] holdingPoint;
    public Vector3 startPosition;
    private bool activeSelfObject;

    private int counterVisitors;
    private float doorbellTimer = 5f;
    private float doorbellTime;
    private bool doorbellTimerOn;
    private List<VisitorController.VisitorType> visitorsTypeList = new List<VisitorController.VisitorType>();
    private VisitorController.VisitorType previousVisitor;
    private WishController.WishType previousWish = WishController.WishType.DEFAULT;

    private GameObject obj;
    private GameObject rndObj;

    private void Awake() {
        instance = this;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }


    public void SetPreviousWish(WishController.WishType wish) {
        this.previousWish = wish;
    }

    public WishController.WishType GetPreviousWish() {
        return this.previousWish;
    }

    private void AddVisitorWishesDictionary() {
        foreach (VisitorController.VisitorType type in Enum.GetValues(typeof(VisitorController.VisitorType))){
            this.visitorWishesDictionary.Add(type,WishController.WishType.DEFAULT);
        }
    }

    public void AddVisitorWishesDictionary(VisitorController.VisitorType keyType,WishController.WishType wishType) {
        if (GetWishTypeInDictionary(keyType) != wishType) {
            this.visitorWishesDictionary[keyType] = wishType;
        }
    }
    public WishController.WishType GetWishTypeInDictionary(VisitorController.VisitorType keyType) {
        foreach (VisitorController.VisitorType type in Enum.GetValues(typeof(VisitorController.VisitorType))){
            if (keyType == type) {
                return visitorWishesDictionary[type];
            }
        }
        return WishController.WishType.DEFAULT;
    }

    public WishController.WishType[] GetActiveWishesTypes() {
        return this.activeWishType;
    }

    public bool IsActiveWishType(WishController.WishType type) {
        for (int i = 0; i < this.GetActiveWishesTypes().Length; i++) {
            if (this.GetActiveWishesTypes()[i] == type) {
                return true;
            }
        }
        return false;
    }

    private void SetActiveWishType(WishController.WishType type) {
        for (int i = 0; i<activeWishType.Length;i++) {
            if (this.activeWishType[i] == WishController.WishType.DEFAULT) {
                this.activeWishType[i] = type;
                break;
            }
        }
    }

    private void ResetActiveWishType(WishController.WishType type) {
        for (int i = 0; i<activeWishType.Length;i++) {
            if (this.activeWishType[i] == type) {
                this.activeWishType[i] = WishController.WishType.DEFAULT;
                break;
            }
        }
    }

    public bool IsActiveRoomWish(WishInThisRoom type) {
        for (int i = 0; i < this.activeWishInRoom.Length; i++) {
            if (this.activeWishInRoom[i] == type) {
                return true;
            }
        }
        return false;
    }

    public void SetActiveWishInRoom(WishController.WishType type) {
        for (int i = 0; i < this.activeWishInRoom.Length; i++) {
            if (this.activeWishInRoom[i] == WishInThisRoom.DEFAULT) {
                this.activeWishInRoom[i] = this.GetActiveRoomByWish(type);
                break;
            }
        }
    }

    private void ResetActiveWishInRoom(WishController.WishType type) {
        for (int i = 0; i < this.activeWishInRoom.Length; i++) {
            if (this.activeWishInRoom[i] == this.GetActiveRoomByWish(type)) {
                this.activeWishInRoom[i] = WishInThisRoom.DEFAULT;
                return;
            }
        }
    }

    public WishInThisRoom GetActiveRoomByWish(WishController.WishType type) {
        if (type == WishController.WishType.CLEAN_FACE || type == WishController.WishType.HAIR_PULLED) {
            return WishInThisRoom.LEFT;
        }else if (type == WishController.WishType.LIPSTICK || type == WishController.WishType.SHARPEN_NAILS) {
            return WishInThisRoom.RIGHT;
        }else if (type == WishController.WishType.PARFUM_DRESS_UP || type == WishController.WishType.PARFUM_ROUGE
            || type == WishController.WishType.MASK) {

            return WishInThisRoom.TOP;
        }
        else {
            return WishInThisRoom.DEFAULT;
        }
    }

    private void Start() {
        this.AddVisitorWishesDictionary();
        this.doorbellTimerOn = false;
        this.timerCreator = this.timeCreator;
        this.nushaAnimatorController = nusha.GetComponent<NushaAnimationController>();
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update() {
        if (InteractionManager.instance.InteractionActivated()) return;
         this.TimerCreator();
        if (this.doorbellTimerOn) {
            if (this.doorbellTime > 0){
                this.doorbellTime -= 1 * Time.deltaTime;
            }
            if (this.doorbellTime <= 0){
                Doorbell();
            }
        }
    }

    private void TimerCreator() {
        if (this.visitorList.Count == 2 || this.obj != null) return;
        if (this.timerCreator > 0){
            this.timerCreator-=Time.deltaTime;
        }
        if (this.timerCreator <= 0) {
            this.timerCreator = 0f;
            // Создаем нового посетителя
            if (this.timerCreator == 0f && this.obj == null && !visitorsTypeList.Count.Equals(visitors.Length)) {
                this.timerCreator = this.timeCreator;
                ChoiseVisitors();
                this.doorbellTimerOn = true;
                this.doorbellTime = this.doorbellTimer;
                AudioManager.instance.GetSoundSource()
                    .PlayOneShot(AudioManager.instance.GetSoundByName("doorbell"));
            }
        }
    }

    public void FreeSpace(Vector3 v,WishController.WishType wishType) {
        for (int i = 0; i<this.holdingPoint.Length;i++) {
            if (this.holdingPoint[i].target.Equals(v)) {
                this.holdingPoint[i].busy = false;
                this.ResetActiveWishType(wishType);
                this.ResetActiveWishInRoom(wishType);
            }
        }
    }

    public void TakeThePlaceOf() {
        for (int i = 0; i < holdingPoint.Length; i++){
            if (!holdingPoint[i].busy && this.obj!=null) {
                VisitorController visitor = this.obj.GetComponent<VisitorController>();
                visitor.MoveTo(holdingPoint[i].targets);
                this.SetActiveWishType(visitor.GetWishController().GetWishType());
                this.SetActiveWishInRoom(visitor.GetWishController().GetWishType());
                holdingPoint[i].busy = true;
                this.obj = null;
                this.doorbellTimerOn = false;
                this.nushaAnimatorController.PlayHiOrHappy();
                break;
            }
        }
    }

    public void RemoveVisitorList(GameObject visitor) {
        this.visitorList.Remove(visitor);
    }

    public void RemoveVisitorTypeList(VisitorController.VisitorType type){
        this.previousVisitor = type;
        this.visitorsTypeList.Remove(type);
    }

    private void Doorbell() {
        this.doorbellTime = this.doorbellTimer;
        this.ShowIcon(this.obj.GetComponent<VisitorController>().GetVisitorType(),ChangingRoom.instance.GetInRoom());
        AudioManager.instance.GetSoundSource()
            .PlayOneShot(AudioManager.instance.GetSoundByName("doorbell"));
    }

    public void SetTimerCreator(float value) {
        this.timerCreator = value;
    }

    public void ChoiseVisitors() {
        int yes = 0;
        this.rndObj = visitors[Random.Range(0, visitors.Length)];
        foreach (VisitorController.VisitorType visitorType in visitorsTypeList) {
            if (visitorType.Equals(this.rndObj.GetComponent<VisitorController>().GetVisitorType())){
                yes++;
            }
        }
        if (yes > 0 || rndObj.GetComponent<VisitorController>().GetVisitorType().Equals(this.previousVisitor)) {
            ChoiseVisitors();
            return;
        }
        this.obj = Instantiate(this.rndObj, this.startPosition, Quaternion.identity) as GameObject;
        this.visitorList.Add(this.obj);
        this.visitorsTypeList.Add(this.obj.GetComponent<VisitorController>().GetVisitorType());
        this.previousVisitor = new VisitorController.VisitorType();
        this.CounterVisitors();
    }

    public VisitorController.VisitorType GetVisitorTypeInLeftRoom() {
        return this.visitorTypeInLeftRoom;
    }

    public void SetVisitorTypeInLeftRoom(VisitorController.VisitorType type) {
        this.visitorTypeInLeftRoom = type;
    }
    
    public VisitorController.VisitorType GetVisitorTypeInRightRoom() {
        return this.visitorTypeInRightRoom;
    }

    public void SetVisitorTypeInRightRoom(VisitorController.VisitorType type) {
        this.visitorTypeInRightRoom = type;
    }

    public VisitorController.VisitorType GetVisitorTypeInTopRoom() {
        return this.visitorTypeInTopRoom;
    }

    public void SetVisitorTypeInTopRoom(VisitorController.VisitorType type) {
        this.visitorTypeInTopRoom = type;
    }

    public void ShowIcon(VisitorController.VisitorType type, bool rightRoom,bool topRoom = false) {
        //Если rightRoom = falce значит занята левая комната и наоборот
        GameObject obj = null;
        VisitorsTypes icon = this.iconVisitors.First(m => m.GetVisitorType().Equals(type));
        if (icon != null)
        {
            if (rightRoom) {
                icon.transform.position = SystemInfo.deviceModel.Contains("iPad") ? new Vector3(1.3f, 1.5f, 0f) : new Vector3(2.5f, 1.5f, 0f);

                icon.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            }else {
                icon.transform.position = SystemInfo.deviceModel.Contains("iPad") ? new Vector3(-9.5f, 1.5f, 0f) : new Vector3(-11, 1.5f, 0f);

                icon.transform.rotation = Quaternion.Euler(new Vector3(0f, -180f, 0f));
            }
            if (topRoom) {
                icon.transform.position = new Vector3(-4.2f, 2.8f, 0f);
                icon.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            }
            obj = icon.gameObject;
            obj.SetActive(true);
            if (null == obj.GetComponent<VisitorsIconController>()) {
                obj.AddComponent<VisitorsIconController>();
            }
            obj.GetComponent<VisitorsIconController>().SetRoom(rightRoom,topRoom);
        }
   }

    public void ShowIcon(VisitorController.VisitorType type,ChangingRoom.Rooms rooms) {
        GameObject obj = null;
        VisitorsTypes icon = this.iconVisitors.First(m => m.GetVisitorType().Equals(type));
        if (icon != null) {
            
                if (rooms.Equals(ChangingRoom.Rooms.RIGHT)) {
                    icon.transform.position = SystemInfo.deviceModel.Contains("iPad") ? new Vector3(9.8f, 1.5f, 0f) : new Vector3(8f, 1.5f, 0f);

                    icon.transform.localScale = new Vector3(-1f, 1f, 1f);
                }
                else if(rooms.Equals(ChangingRoom.Rooms.LEFT)) {
                    icon.transform.position = SystemInfo.deviceModel.Contains("iPad") ? new Vector3(-17.8f, 1.5f, 0f) : new Vector3(-16f, 1.5f, 0f);

                    icon.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                else if(rooms.Equals(ChangingRoom.Rooms.TOP)) {
                    icon.transform.position = SystemInfo.deviceModel.Contains("iPad") ? new Vector3(-9.3f, 12f, 0f) : new Vector3(-10.5f, 12f, 0f);

                    icon.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                else {
                    return;
                }
                obj = icon.gameObject;
                obj.SetActive(true);
                if (null == obj.GetComponent<VisitorsIconController>()) {
                   obj.AddComponent<VisitorsIconController>();
                }
                obj.GetComponent<VisitorsIconController>().ShowBell();
        }
   }

    private void CounterVisitors() {
        this.counterVisitors ++;
    }

    public void ResetCounterVisitors() {
        this.counterVisitors = 0;
    }

    public int GetCountVisitors() {
        return this.counterVisitors;
    }

    public RightRoom GetRightRoom() {
        return this.rightRoom;
    }

    public LeftRoom GetLeftRoom() {
        return this.leftRoom;
    }

    public TopRoom GetTopRoom() {
        return this.topRoom;
    }

    public bool VisitorIsCame() {
        if (obj == null) {
            return false;
        }
        else {
            return true;
        }
    }
}
