﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper{

    public static string GetAspectRatio(Vector2 resolution) {
        float v = (resolution.y/resolution.x)*16;
        
        if (Mathf.CeilToInt(v) >= 10) {
            return "16:10";
        }
        return "other";
    }
}
