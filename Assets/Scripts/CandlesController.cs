﻿using UnityEngine;
using System.Collections;

public class CandlesController : MonoBehaviour {
    private Animator[] _animators;
    private bool IsIgnite;
    private int hashIgnite;
    private int hashPutOut;
	void OnEnable () {
	    this.IsIgnite = false;
	    this.hashIgnite = Animator.StringToHash("ignite");
	    this.hashPutOut = Animator.StringToHash("putOut");
	    this._animators = GetComponentsInChildren<Animator>();
	}

    private void OnDisable() {
        if (this.IsIgnite) {
            this.PutOut();
        }
    }

    private void FixedUpdate() {
        if (this.IsIgnite && ChangingRoom.instance.GetInRoom() == ChangingRoom.Rooms.CENTER) {
            this.PutOut();
        }
    }

    private void Ignite() {
        foreach (Animator animator in this._animators) {
            animator.SetTrigger(this.hashIgnite);
            this.IsIgnite = true;
        }
    }

    private void PutOut() {
        foreach (Animator animator in this._animators) {
            animator.SetTrigger(this.hashPutOut);
            this.IsIgnite = false;
        }
    }

    private void OnMouseDown() {
        AudioManager.instance.GetSoundSource()
            .PlayOneShot(AudioManager.instance.GetSoundByName("click"));
        if (this.IsIgnite) {
            this.PutOut();
        }
        else {
            this.Ignite();
        }
    }

}
