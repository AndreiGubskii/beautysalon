﻿using UnityEngine;
using System.Collections;

public class LouversController : MonoBehaviour {
    [SerializeField] private SpriteRenderer darkSprite;
    [SerializeField] private SpriteRenderer lightLamp;
    private Animator[] _animators;
    private int hashOpen;
    private int hashClose;
    private bool isOpen;

    private float speed = 2f;
    private float progress;
    public static LouversController instance;
    private void Start() {
        instance = this;
        this._animators = GetComponentsInChildren<Animator>();
        this.hashClose = Animator.StringToHash("close");
        this.hashOpen = Animator.StringToHash("open");
        this.isOpen = true;
    }

    private void OnEnable() {
        this.isOpen = true;
    }

    private void Update() {
         if (!this.isOpen && !ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.LEFT) && 
             ChairController.instance.GetChair().childCount.Equals(0)) {
            MouseDown();
        }
    }

    private void OpenLouvers() {
        this.progress = 0;
        foreach (Animator animator in this._animators) {
            animator.SetTrigger(this.hashOpen);
        }
        this.isOpen = true;
        this.darkSprite.enabled = false;
        this.lightLamp.enabled = false;

        this.darkSprite.sprite = null;
        this.lightLamp.sprite = null;
        AudioManager.instance.GetSoundSource().PlayOneShot(
            AudioManager.instance.GetSoundByName("switch"));
        AudioManager.instance.PlaySound("lovers");
        Resources.UnloadUnusedAssets();
    }

    private void CloseLouvers() {
        this.progress = 0;
        foreach (Animator animator in this._animators) {
            animator.SetTrigger(this.hashClose);
        }
        this.isOpen = false;
        this.darkSprite.enabled = true;
        this.lightLamp.enabled = true;
        AudioManager.instance.GetSoundSource().PlayOneShot(
            AudioManager.instance.GetSoundByName("switch"));
        AudioManager.instance.PlaySound("lovers");
    }

    private void OnMouseDown() {
        this.MouseDown();
    }

    public void MouseDown() {
        if (this.darkSprite.sprite == null) {
            this.darkSprite.sprite = Resources.Load<Sprite>("Art/LeftRoom/DarkRoom_1");
        }
        if (this.lightLamp.sprite == null) {
            this.lightLamp.sprite = Resources.Load<Sprite>("Art/TopRoom/room_4_slifht");
        }
        if (this.isOpen) {
            this.CloseLouvers();
        }
        else {
            this.OpenLouvers();
        }
    }
}
