﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class LipstickController : MonoBehaviour ,IDragHandler,IPointerUpHandler,IPointerDownHandler {
    private bool pressed;

    private Vector3 startPosition;
    [SerializeField] private GameObject lipstickTexture;
    [SerializeField] private Color lipstickColor;
    private Vector3 defaultRotation = new Vector3(0f,0f,0f);
    private Vector3 tapRotation = new Vector3(0f, 0f, 60f);
    private RectTransform rectTransform;
    private void Start() {
        this.rectTransform = GetComponent<RectTransform>();
        this.startPosition = transform.localPosition;
    }

    private void Update() {
        if (this.transform.localPosition != this.startPosition && !this.pressed){
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.startPosition, 2000 * Time.deltaTime);
        }
    }

    public void OnDrag(PointerEventData eventData) {
        transform.position = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData) {
        this.pressed = false;
        if (this.rectTransform != null) {
            this.rectTransform.rotation = Quaternion.Euler(this.defaultRotation);
        }
        if (LipsInteractionController.instance.GetIsPainted() && LipsInteractionController.instance.GetIsHoldLipstick()) {
            LipsInteractionController.instance.SetIsHoldLipstick(false);
        }
    }

    public void OnPointerDown(PointerEventData eventData){
        if (this.rectTransform != null) {
            this.rectTransform.rotation = Quaternion.Euler(this.tapRotation);
        }
        this.pressed = true;
        if (!LipsInteractionController.instance.GetIsPainted() && !LipsInteractionController.instance.GetIsHoldLipstick()) {
            LipsInteractionController.instance.SetIsHoldLipstick(true);
        }
    }

    public Color GetPolishColor() {
        return this.lipstickColor;
    }

    public GameObject GetPolish() {
        return this.lipstickTexture;
    }

    public bool IsPressed() {
        return this.pressed;
    }
}
