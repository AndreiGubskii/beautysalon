﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LipsPaint : MonoBehaviour {

    public static LipsPaint instance;
    private GameObject lipstickTexture;
    private string coliderName;
    private LipstickController lipstickController;
    //private bool isPointerEnter;
    private Vector3 tipPosition = new Vector3(0f, 0f, 0f);
    private List<GameObject> objects; 
    private void OnEnable() {
        if (instance == null) {
            instance = this;
        }
        this.objects = new List<GameObject>();
    }

    private void OnDisable() {
        foreach (GameObject obj in this.objects) {
            Destroy(obj);
        }
    }

    private void OnTriggerEnter(Collider col) {
        this.coliderName = col.name;
        if (!this.coliderName.Equals("tipLipstick")) return;
        
        lipstickController = col.GetComponentInParent<LipstickController>();
        if (lipstickController != null) {
            this.lipstickTexture = lipstickController.GetPolish();
        }

        if (!InteractionManager.instance.GetInteractionWas()) {
            InteractionManager.instance.SetInteractionWas(true);
        }
        CanvasController.instance.SetScaleTimer();
    }

    private void OnTriggerExit() {
        if (this.lipstickController.Equals(null)) {
            this.lipstickController = null;
        }
    }

    private void OnTriggerStay(Collider col) {
        if (this.tipPosition.Equals(col.transform.position) || !this.coliderName.Equals("tipLipstick")
            || !this.lipstickController.IsPressed()) return;
        this.tipPosition = col.transform.position;
        GameObject obj = Instantiate(this.lipstickTexture);
        obj.GetComponent<Image>().color = this.lipstickController.GetPolishColor();
        obj.transform.SetParent(this.transform);
        obj.transform.position = tipPosition;
        obj.transform.localScale = Vector3.one;
        objects.Add(obj);
        if (!LipsInteractionController.instance.GetIsPainted()) {
            LipsInteractionController.instance.SetIsPainted(true);
        }
    }
}
