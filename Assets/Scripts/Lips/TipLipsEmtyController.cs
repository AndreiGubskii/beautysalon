﻿using UnityEngine;
using System.Collections;

public class TipLipsEmtyController : MonoBehaviour {
    public bool isActive;
    private void OnTriggerEnter(Collider col) {
        if (!isActive) return;
        if (col.gameObject.name == "lipstickTexture(Clone)") {
            Destroy(col.gameObject);
        }
    }
}
