﻿using UnityEngine;

public class LipsInteractionController : MonoBehaviour {
    [System.Serializable]
    public class Lips {
        public GameObject lips;
        public GameObject lipsAlfa;

        public void PlayAnimation() {
            lips.GetComponent<Animator>().SetTrigger("play");
//            lipsAlfa.GetComponent<Animator>().SetTrigger("play");
        }
    }

    private bool isHold ;
    private bool isPainted;
    private bool lipsPainted;
    private GameObject obj;
    [SerializeField] private Lips lips;
    public static LipsInteractionController instance;

    private void OnEnable() {
        if (instance == null) {
            instance = this;
        }
        this.SetIsPainted(false);
        this.SetIsHoldLipstick(false);
        this.lipsPainted = false;
//        lips.lipsAlfa.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-16f,-247f,0);
//        lips.lipsAlfa.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);

    }

    private void OnDisable() {
        if (this.obj != null) {
            Destroy(this.obj);
        }

        Resources.UnloadUnusedAssets();
    }

    public void Activate(VisitorController v) {
        this.obj = (GameObject)
            Instantiate(GetVisitorByTypeInLipstickInteraction(v.GetVisitorType()));
        this.obj.transform.SetParent(this.transform.GetChild(0));
        this.obj.transform.localScale = new Vector3(1f,1f,1f);
        this.obj.transform.localPosition = Vector3.zero;
    }

    private void Update() {
        if (!this.GetIsHoldLipstick() && this.GetIsPainted() && !this.lipsPainted) {
            this.PaintedListener();
            this.lipsPainted = true;
        }
    }

    public void SetIsHoldLipstick(bool hold) {
        this.isHold = hold;
    }

    public bool GetIsHoldLipstick() {
        return this.isHold;
    }

    public void SetIsPainted(bool painted) {
        this.isPainted = painted;
    }

    public bool GetIsPainted() {
        return this.isPainted;
    }

    private GameObject GetVisitorByTypeInLipstickInteraction(VisitorController.VisitorType type) {
        switch (type) {
                case VisitorController.VisitorType.IGOGOSHA:
                return Resources.Load<GameObject>("Prefabs/Lips/Lips_Igogo");
                case VisitorController.VisitorType.SOVUNYA:
                return Resources.Load<GameObject>("Prefabs/Lips/Lips_Sov");
                case VisitorController.VisitorType.STEPANIDA:
                return Resources.Load<GameObject>("Prefabs/Lips/Lips_Panda");
        }
        return null;
    }

    public Lips GetLips() {
        return this.lips;
    }

    // **** Listeners ****
    private void PaintedListener() {

    }
}
