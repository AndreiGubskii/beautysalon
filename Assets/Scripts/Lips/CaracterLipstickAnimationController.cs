﻿using UnityEngine;
using System.Collections;

public class CaracterLipstickAnimationController : MonoBehaviour {
    private Animator _animator;
    private int hashBlink;
    private int hashNouse;
    private float blinkTime;
    private float blinkTimer;
    [SerializeField] private bool availableNouseAnimation;
    void Start () {
        this._animator = GetComponentInChildren<Animator>();
        this.hashBlink = Animator.StringToHash("blink");
        this.hashNouse = Animator.StringToHash("nouse");
        this.blinkTime = 5f;
        this.blinkTimer = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
	    if ((this.blinkTimer + this.blinkTime) < Time.time) {
	        this.blinkTimer = Time.time;
	        this.blinkTime = Random.Range(3f, 8f);
            this.PlayBlinkAnimation();
	        if (this.availableNouseAnimation) {
                Invoke("PlayNouseAnimation", this.blinkTime / 2);
	        }
	    }
	}

    private void PlayBlinkAnimation() {
        this._animator.SetTrigger(this.hashBlink);
    }

    private void PlayNouseAnimation() {
        this._animator.SetTrigger(this.hashNouse);
    }
}
