﻿using UnityEngine;
using System.Collections;

public class ChangingRoom : MonoBehaviour
{
    public enum Rooms {
        LEFT,
        RIGHT,
        CENTER,
        TOP,
        ROOF,
        BOTTOM_CENTER,
        BOTTOM_LEFT,
        BOTTOM_RIGHT
    }
    private Vector3 startPosition;
    private Rooms inRoom;
    [SerializeField]private Vector3 leftPosition;
    [SerializeField]private Vector3 rightPosition;
    [SerializeField]private Vector3 topPosition;
    [SerializeField]private Vector3 roofPosition;
    [SerializeField]private Vector3 bottomCenterPosition;
    [SerializeField]private Vector3 bottomRightPosition;
    [SerializeField]private Vector3 bottomLeftPosition;
    [SerializeField]private float fastSpeed = 100f;
    [SerializeField]private float slowSpeed = 10f;
    private float speed;
    private Vector3 target;
    private float saveY;
    private float y;
    private bool move;

    public static ChangingRoom instance;
	void Start () {
	    CamSize();

        instance = this;
	    this.move = false;
	    SwipeManager.instance.SwipeLeft += this.SwipeToLeft;
	    SwipeManager.instance.SwipeRight += this.SwipeToRight;
        SwipeManager.instance.SwipeUp += this.SwipeToTop;
        SwipeManager.instance.SwipeDown += this.SwipeToDown;
        this.startPosition = transform.position;
	    MoveTo(this.startPosition,this.fastSpeed);
	}
	void Update () {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
	        SwipeToLeft();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            SwipeToRight();
        }
        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            SwipeToTop();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow)) {
            SwipeToDown();
        }
	    if (this.target.Equals(this.startPosition) && this.inRoom != Rooms.CENTER) {
	        this.inRoom = Rooms.CENTER;
        }else if(this.target.Equals(this.rightPosition) && this.inRoom != Rooms.RIGHT) {
	        this.inRoom = Rooms.RIGHT;
	    }else if (this.target.Equals(this.leftPosition) && this.inRoom != Rooms.LEFT) {
	        this.inRoom = Rooms.LEFT;
        }else if (this.target.Equals(this.topPosition) && this.inRoom != Rooms.TOP) {
            this.inRoom = Rooms.TOP;
        }else if (this.target.Equals(this.bottomCenterPosition) && this.inRoom != Rooms.BOTTOM_CENTER) {
            this.inRoom = Rooms.BOTTOM_CENTER;
        }else if (this.target.Equals(this.bottomLeftPosition) && this.inRoom != Rooms.BOTTOM_LEFT) {
            this.inRoom = Rooms.BOTTOM_LEFT;
        }else if (this.target.Equals(this.bottomRightPosition) && this.inRoom != Rooms.BOTTOM_RIGHT) {
            this.inRoom = Rooms.BOTTOM_RIGHT;
        }else if (this.target.Equals(this.roofPosition) && this.inRoom != Rooms.ROOF) {
            this.inRoom = Rooms.ROOF;
        }

	    if (this.inRoom.Equals(Rooms.TOP) && !InteractionManager.instance.InteractionActivated()) {
	        if (Input.GetMouseButtonDown(0)) {
	            this.saveY = Input.mousePosition.y;
	            this.y = transform.position.y;
	        }
            if (Input.GetMouseButton(0)) {
                if (Input.mousePosition.y < this.saveY-25f) {
                    this.saveY = Input.mousePosition.y;
                    if (this.transform.position.y < 17.1f) {
                        this.y = this.y + 5f* Time.deltaTime;
                    }
                }
                this.transform.position = new Vector3(this.transform.position.x, this.y, this.transform.position.z);
            }
        }

        if (!InteractionManager.instance.InteractionActivated() && 
            (this.inRoom.Equals(Rooms.CENTER) || this.inRoom.Equals(Rooms.RIGHT) || this.inRoom.Equals(Rooms.LEFT))) {
	        if (Input.GetMouseButtonDown(0)) {
	            this.saveY = Input.mousePosition.y;
	            this.y = transform.position.y;
	        }
            if (Input.GetMouseButton(0)) {
                if (Input.mousePosition.y > this.saveY+1f) {
                    this.saveY = Input.mousePosition.y;
                    if (this.transform.position.y > -6f) {
                        this.y = this.y - 5f* Time.deltaTime;
                    }
                }
                this.transform.position = new Vector3(this.transform.position.x, this.y, this.transform.position.z);
            }
        }
        if (this.move) {
	        if (this.transform.position != this.target) {
	            this.transform.position = Vector3.MoveTowards(this.transform.position, this.target,
	                this.speed*Time.deltaTime);
	        } else {
	            this.move = false;
	        }
	    }
	}

    public void SwipeToLeft() {
        if (this == null || this.inRoom.Equals(Rooms.TOP) || this.inRoom.Equals(Rooms.BOTTOM_CENTER)
            || this.inRoom.Equals(Rooms.BOTTOM_LEFT) || this.inRoom.Equals(Rooms.BOTTOM_RIGHT) || 
            InteractionManager.instance.InteractionActivated()) return;

        if (this.inRoom.Equals(Rooms.RIGHT)) {
            this.MoveTo(this.startPosition, this.fastSpeed);
        } else if(this.inRoom.Equals(Rooms.CENTER)){
            this.MoveTo(this.leftPosition, this.fastSpeed);
        }
    }

    public void SwipeToRight() {
        if (this == null || this.inRoom.Equals(Rooms.TOP) || this.inRoom.Equals(Rooms.BOTTOM_CENTER)
            || this.inRoom.Equals(Rooms.BOTTOM_LEFT) || this.inRoom.Equals(Rooms.BOTTOM_RIGHT) ||
            InteractionManager.instance.InteractionActivated()) return;
        
        if (this.inRoom.Equals(Rooms.LEFT)){
            this.MoveTo(this.startPosition, this.fastSpeed);
        } else if (this.inRoom.Equals(Rooms.CENTER)) {
            this.MoveTo(this.rightPosition, this.fastSpeed);
        }
    }

    public void SwipeToTop() {
        if (this == null || this.inRoom.Equals(Rooms.RIGHT) || this.inRoom.Equals(Rooms.LEFT) ||
            InteractionManager.instance.InteractionActivated()) return;
        if (this.inRoom.Equals(Rooms.BOTTOM_CENTER)) {
            this.MoveTo(this.startPosition, this.slowSpeed);
        }else if (this.inRoom.Equals(Rooms.BOTTOM_LEFT)) {
            this.MoveTo(this.leftPosition, this.slowSpeed);
        }else if(this.inRoom.Equals(Rooms.BOTTOM_RIGHT)) {
            this.MoveTo(this.rightPosition, this.slowSpeed);
        }else if (this.inRoom.Equals(Rooms.CENTER)) {
            this.MoveTo(this.topPosition, this.fastSpeed);
        }else if (this.inRoom.Equals(Rooms.TOP)) {
            this.MoveTo(this.roofPosition,this.slowSpeed);
        }
    }

    public void SwipeToDown() {
        if (this == null || InteractionManager.instance.InteractionActivated()) return;
        if (this.inRoom.Equals(Rooms.TOP)) {
            this.MoveTo(this.startPosition, this.fastSpeed);
        } else if (this.inRoom.Equals(Rooms.CENTER)) {
            this.MoveTo(this.bottomCenterPosition, this.slowSpeed);
        } else if (this.inRoom.Equals(Rooms.LEFT)){
            this.MoveTo(this.bottomLeftPosition, this.slowSpeed);
        } else if (this.inRoom.Equals(Rooms.RIGHT)){
            this.MoveTo(this.bottomRightPosition, this.slowSpeed);
        } else if (this.inRoom.Equals(Rooms.ROOF)) {
            this.MoveTo(this.topPosition,this.slowSpeed);
        }
    }

    public void SetPosition(Vector3 pos) {
        this.transform.position = pos;
        this.target = pos;
    }

    public void SetStartPosition() {
        this.transform.position = this.startPosition;
        this.target = this.startPosition;
    }

    private void MoveTo(Vector3 pos,float speed) {
        this.target = pos;
        this.move = true;
        this.speed = speed;
    }

    public void MoveToWaitingRoom() {
        this.target = this.startPosition;
        this.move = true;
        this.speed = this.fastSpeed;
    }

    public void MoveToTopRoom() {
        this.target = this.topPosition;
        this.move = true;
        this.speed = this.fastSpeed;
    }

    public void MoveToRightRoom() {
        this.target = this.rightPosition;
        this.move = true;
        this.speed = this.fastSpeed;
    }

    public void MoveToLeftRoom() {
        this.target = this.leftPosition;
        this.move = true;
        this.speed = this.fastSpeed;
    }

    public Rooms GetInRoom() {
        return this.inRoom;
    }

    public void SetTarget(Vector3 target,bool fast = false) {
        if (fast) {
            this.transform.position = target;
        }
        this.target = target;
        this.move = true;
    }

    private void CamSize() {
        if (Camera.main.aspect >= 1.9f) {
            GetComponent<Camera>().orthographicSize = 4.5f;
        }
    }
}
