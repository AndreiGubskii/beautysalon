﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour {
    private Animator _animator;
    private int openHash;
    private int closeHash;
    [SerializeField] private GameObject doorjamb;
    public static DoorController instance;
	private void Awake () {
	    instance = this;
	    this._animator = GetComponent<Animator>();
	}

    private void Start() {
        this.openHash = Animator.StringToHash("open");
        this.closeHash = Animator.StringToHash("close");
    }

    public void OpenDoor() {
        if(!TurnController.instance.VisitorIsCame()) return;
        StartCoroutine(ChangeLayer());
        this._animator.SetTrigger(this.openHash);
    }

    public void closeDoor() {
        this._animator.SetTrigger(this.closeHash);
    }

    private void OnMouseDown() {
        if (InteractionManager.instance.InteractionActivated()) return;
        this.OpenDoor();
        TurnController.instance.TakeThePlaceOf();
    }

    private IEnumerator ChangeLayer() {
        this.GetComponent<SpriteRenderer>().sortingLayerName = "Default";
        if (this.doorjamb != null) {
            this.doorjamb.GetComponent<SpriteRenderer>().sortingLayerName = "Default";
        }
        yield return new WaitForSeconds(2f);
        this.GetComponent<SpriteRenderer>().sortingLayerName = "Door";
        if (this.doorjamb != null) {
            this.doorjamb.GetComponent<SpriteRenderer>().sortingLayerName = "Door";
        }
    }
}
