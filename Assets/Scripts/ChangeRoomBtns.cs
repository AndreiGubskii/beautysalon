﻿using UnityEngine;
using System.Collections;

public class ChangeRoomBtns : MonoBehaviour {
    public enum BtnsTag {
        NONE,
        RIGHT,
        LEFT,
        CENTER
    }

    [SerializeField] private BtnsTag type;

    private void OnMouseDown() {
        switch (type) {
            case BtnsTag.LEFT:
                ChangingRoom.instance.SwipeToLeft();
            break;
            case BtnsTag.RIGHT:
                ChangingRoom.instance.SwipeToRight();
            break;
            case BtnsTag.CENTER:
            ChangingRoom.instance.SwipeToDown();
            break;
            default:
            break;
        }
    }
}
