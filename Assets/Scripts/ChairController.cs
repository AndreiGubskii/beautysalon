﻿using UnityEngine;
using System.Collections;

public class ChairController : MonoBehaviour {
    
    private bool included;
    private Animator _animator;
    [SerializeField] private Transform chairSprite;
    public static ChairController instance;
    private ChairSwichController swich;
    private AudioSource audioSource;
    private void Start() {
        instance = this;
        audioSource = GetComponent<AudioSource>();
        
        this._animator = GetComponent<Animator>();
        this.included = false;
        this.swich = GetComponentInChildren<ChairSwichController>();
    }
    
    private void Update() {
        if (this.included && !ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.LEFT) && this.chairSprite.childCount.Equals(0)) {
            if (this.swich != null) {
                this.swich.MouseDown();
            }
        }

        if (this.included && this.chairSprite.GetComponentInChildren<CharacterAnimationController>()!=null && 
            !this.chairSprite.GetComponentInChildren<CharacterAnimationController>().GetVibratingChair()){
                this.chairSprite.GetComponentInChildren<CharacterAnimationController>().PlayVibratingChairOn();
                this.chairSprite.GetComponentInChildren<VisitorController>().SetVibroChair(true);
            audioSource.clip = AudioManager.instance.GetSoundByName("chair");
            audioSource.Play();
        }
    }

    public void Vibrator(bool b) {
        this.included = b;
        if (this._animator != null) {
            if (b) {
                this._animator.SetTrigger("on");
                audioSource.clip = AudioManager.instance.GetSoundByName("vibrate");
                if (this.chairSprite.GetComponentInChildren<CharacterAnimationController>() != null) {
                    this.chairSprite.GetComponentInChildren<CharacterAnimationController>().PlayVibratingChairOn();
                    this.chairSprite.GetComponentInChildren<VisitorController>().SetVibroChair(true);
                    audioSource.clip = AudioManager.instance.GetSoundByName("chair");
                }
                audioSource.Play();
            }
            else {
                this._animator.SetTrigger("off");
                if (this.chairSprite.GetComponentInChildren<CharacterAnimationController>() != null) {
                    this.chairSprite.GetComponentInChildren<CharacterAnimationController>().PlayVibratingChairOff();
                    this.chairSprite.GetComponentInChildren<VisitorController>().SetVibroChair(false);
                }
                audioSource.Stop();
            }
        }
    }

    public bool IfIncluded() {
        return this.included;
    }

    private void OnTriggerEnter(Collider col) {
        StartCoroutine(TriggerEnterEnumerator(col));
    }

    private IEnumerator TriggerEnterEnumerator(Collider col) {
        VisitorController visitor = col.GetComponent<VisitorController>();
        if (null != visitor) {
            while (visitor.IfMoving()) {
                yield return new WaitForEndOfFrame();
            }
            col.transform.parent = this.chairSprite;
        }
    }

    private void OnTriggerExit(Collider col) {
        if (col.transform.parent == this.chairSprite) {
            col.transform.parent = null;
        }
    }

    public Transform GetChair() {
        return this.chairSprite;
    }
}
