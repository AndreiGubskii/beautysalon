﻿using UnityEngine;
using System.Collections;

public class ChairSwichController : MonoBehaviour {
    private Animator _animator;
    private bool swich;
    private ChairController chairController;
	void OnEnable () {
	    this.swich = false;
	    _animator = GetComponent<Animator>();
	    this.chairController = GetComponentInParent<ChairController>();
	}

    private void OnMouseDown() {
        this.MouseDown();
    }

    public void MouseDown() {
        AudioManager.instance.GetSoundSource().
            PlayOneShot(AudioManager.instance.GetSoundByName("click"));
        if (this.swich) {
            this.SwichOff();
        }
        else {
            this.SwichOn();
        }
    }

    private void SwichOn() {
        this._animator.SetTrigger("on");
        this.chairController.Vibrator(true);
        this.swich = true;
    }

    private void SwichOff() {
        this._animator.SetTrigger("off");
        this.chairController.Vibrator(false);
        this.swich = false;
    }
}
