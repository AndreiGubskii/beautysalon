﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour {
    [System.Serializable]
    public class Buttons {
        public Button deactivateInteractionBtns;
        public Button nextInteractionBtns;
        public Button exitInteractionBtns;
        public Button exitShoesInteractionBtns;
    }

    [SerializeField]private Buttons buttons;
    public static CanvasController instance;
    private InteractionManager.InteractionName interactionName;
    private float delayBeforeNextInteraction;
    private Vector3 targetScale;
    private Vector3 changeScale = new Vector3(1.2f, 1.2f, 1.2f);
    private Vector3 defaultScale = new Vector3(1f,1f,1f);
    private bool scale;
    private int counterScale;
    private int scaleCount = 4;
    private float time = 3f;
    private float saveTime;
    private void Start() {
        instance = this;
        this.scale = false;
        this.targetScale = this.changeScale;
        buttons.deactivateInteractionBtns.onClick.AddListener(DeactivateInteractionBtnClickListener);
        buttons.nextInteractionBtns.onClick.AddListener(NextInteractionBtnClickListener);
        buttons.exitInteractionBtns.onClick.AddListener(ExitInteractionBtnClickListener);
        buttons.exitShoesInteractionBtns.onClick.AddListener(ExitShoesInteractionBtnClickListener);
    }

    private void Update() {
        if (this.scale && this.counterScale!=this.scaleCount) {
            if (this.buttons.exitInteractionBtns.gameObject.transform.localScale != this.targetScale) {
                this.buttons.exitInteractionBtns.gameObject.transform.localScale =
                    Vector3.MoveTowards(this.buttons.exitInteractionBtns.gameObject.transform.localScale,
                        this.targetScale, 2*Time.deltaTime);
            }
            else if(this.buttons.exitInteractionBtns.gameObject.transform.localScale == this.defaultScale) {
                this.targetScale = this.changeScale;
                this.counterScale++;
            }else if (this.buttons.exitInteractionBtns.gameObject.transform.localScale == this.changeScale) {
                this.targetScale = this.defaultScale;
                this.counterScale++;
            }
        }
        if (this.scale && Input.GetMouseButton(0)) {
            this.saveTime = Time.time;
        }
        if (this.scale && !Input.GetMouseButton(0) && (this.saveTime + this.time) < Time.time) {
            this.counterScale = 0;
            this.saveTime = Time.time;
        }
    }

    public void SetScaleTimer() {
        this.scale = true;
        this.counterScale = this.scaleCount;
        this.saveTime = Time.time;
    }

    public void DeactivateInteractionBtnsActiveSelf(bool active) {
        float speed = 1600f;
        ObjectMover objMover = this.buttons.deactivateInteractionBtns.gameObject.GetComponent<ObjectMover>();
        if (objMover == null) {
            objMover = this.buttons.deactivateInteractionBtns.gameObject.AddComponent<ObjectMover>();
        }
        if (active) {
            objMover.MoveTo(new Vector3(-125f, -125f, 0), speed);
        }
        else {
            objMover.MoveTo(new Vector3(-125f, 100f, 0), speed);
        }
    }

    public void ExitInteractionBtnsActiveSelf(bool active) {
        float speed = 1600f;
        ObjectMover objMover = this.buttons.exitInteractionBtns.gameObject.GetComponent<ObjectMover>();
        if (objMover == null) {
            objMover = this.buttons.exitInteractionBtns.gameObject.AddComponent<ObjectMover>();
        }
        if (active) {
            objMover.MoveTo(new Vector3(-1745f, -125f, 0), speed);
        }
        else {
            NextInteractionBtnsActiveSelf(false);
            objMover.MoveTo(new Vector3(-1745f, 100f, 0), speed);
        }
    }

    public void ExitShoesInteractionBtnsActiveSelf(bool active) {
        float speed = 1600f;
        ObjectMover objMover = this.buttons.exitShoesInteractionBtns.gameObject.GetComponent<ObjectMover>();
        if (objMover == null) {
            objMover = this.buttons.exitShoesInteractionBtns.gameObject.AddComponent<ObjectMover>();
        }
        if (active) {
            objMover.MoveTo(new Vector3(-1745f, -125f, 0), speed);
        }
        else {
            objMover.MoveTo(new Vector3(-1745f, 100f, 0), speed);
        }
    }

    public void NextInteractionBtnsActiveSelf(bool active) {
        float speed = 1600f;
        ObjectMover objMover = this.buttons.nextInteractionBtns.gameObject.GetComponent<ObjectMover>();
        if (objMover == null) {
            objMover = this.buttons.nextInteractionBtns.gameObject.AddComponent<ObjectMover>();
        }
        if (active) {
            objMover.MoveTo(new Vector3(-125f, -125f, 0), speed);
        }
        else {
            objMover.MoveTo(new Vector3(-125f, 100f, 0), speed);
        }
    }

    public void ActivateNextInteractionBtn(InteractionManager.InteractionName interactionName, float delay = 1f) {
        this.NextInteractionBtnsActiveSelf(true);
        this.interactionName = interactionName;
        this.delayBeforeNextInteraction = delay;
    }

    // **************** Listeners ********************
    private void DeactivateInteractionBtnClickListener() {
        InteractionManager.instance.DeactivateInteraction();
        AudioManager.instance.GetSoundSource().
            PlayOneShot(AudioManager.instance.GetSoundByName("click"));
    }

    private void NextInteractionBtnClickListener() {
        NextInteractionBtnsActiveSelf(false);
        InteractionManager.instance.ActivateNextInteraction(interactionName, delayBeforeNextInteraction);
        AudioManager.instance.GetSoundSource().
            PlayOneShot(AudioManager.instance.GetSoundByName("click"));
    }

    private void ExitInteractionBtnClickListener() {
        this.scale = false;
        this.buttons.deactivateInteractionBtns.gameObject.transform.localScale = this.defaultScale;
        InteractionManager.instance.DeactivateInteraction();
        AudioManager.instance.GetSoundSource().
            PlayOneShot(AudioManager.instance.GetSoundByName("click"));
    }

    private void ExitShoesInteractionBtnClickListener() {
        InteractionManager.instance.DeactivateShoesGame();
        AudioManager.instance.GetSoundSource().
            PlayOneShot(AudioManager.instance.GetSoundByName("click"));
    }
}
