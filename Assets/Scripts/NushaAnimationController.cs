﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class NushaAnimationController : MonoBehaviour {
    private Animator _animator;
    private int hashGreetHappy;
    private int hashGreetHi;
    private int hashJump;
    private int previousVoice = 0;
    private string[] voices = { "nusha_1", "nusha_2", "nusha_3" };
    private void Start() {
        this._animator = GetComponent<Animator>();
        this.hashGreetHappy = Animator.StringToHash("greet_happy");
        this.hashGreetHi = Animator.StringToHash("greet_hi");
        this.hashJump = Animator.StringToHash("jump");
    }

    public void PlayGreetHappy() {
        this._animator.SetTrigger(this.hashGreetHappy);
    }

    public void PlayGreetHi() {
        this._animator.SetTrigger(this.hashGreetHi);
    }

    public void PlayJump() {
        this._animator.SetTrigger(this.hashJump);
    }

    public void PlayHiOrHappy() {
        int i = Random.Range(0, 2);
        switch (i) {
            case 0:
                this.PlayGreetHi();
                break;
            case 1:
                this.PlayGreetHappy();
                break;
            default:
                this.PlayGreetHi();
                break;
        }
    }

    public void OnMouseDown() {
        if(InteractionManager.instance.InteractionActivated()) return;
        PlayJump();
        int i = Random.Range(0, voices.Length);
        while (i == previousVoice) {
            i = Random.Range(0, voices.Length);
        }
        previousVoice = i;
        AudioManager.instance.PlaySound(voices[i]);
    }
}
