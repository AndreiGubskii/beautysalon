﻿using UnityEngine;
using System.Collections;

public class InteractionCharacterController : MonoBehaviour {
    [System.Serializable]
    public class DirtySpriteValues {
        public Vector3 position;
        public Vector3 scale;
    }

    private InteractionCharacterAnimationController animatorController;
    [SerializeField] private DirtySpriteValues dirtySpriteValues;
    private bool washFoam;
    private bool spreadFoam;
    private GameObject dirty;

	void Start () {
	    this.animatorController = GetComponent<InteractionCharacterAnimationController>();
	}

    private void OnEnable() {
        this.washFoam = false;
        this.spreadFoam = false;

        if (Settings.instance.GetDirtySprite() != null && BathRoomController.instance != null) {
            this.dirty = Instantiate(Settings.instance.GetDirtySprite());
            this.dirty.transform.SetParent(this.transform.GetChild(0).transform.GetChild(0).transform);
            this.dirty.transform.localPosition = this.dirtySpriteValues.position;
            this.dirty.transform.localScale = this.dirtySpriteValues.scale;
        }
        if (BathRoomController.instance != null) {
            BathRoomController.instance.OnActivateBathRoom += this.ActivateBathroomInteractionListener;
            BathRoomController.instance.OnDeactivateBathRoom += this.DeactivateBatroomInteractionListener;
            BathRoomController.instance.OnLathered += this.OnLatheredListener;
            BathRoomController.instance.OnWasheded += this.OnWashededListener;
            BathRoomController.instance.OnWiped += this.OnWipedListener;
            BathRoomController.instance.OnWashFoam += this.OnWashFoamListener;
            BathRoomController.instance.OnSpreadFoam += this.OnSpreadFoamListener;
            BathRoomController.instance.OnSpreadFoamStoped += this.OnSpreadFoamStoppedListener;
            BathRoomController.instance.OnWashFoamStoped += this.OnWashFoamStoppedListener;
        }

        if (ParfumRoomController.instance != null) {
            ParfumRoomController.instance.ParfumRoomMaskDraw += this.OnSpreadFoamListener;
            ParfumRoomController.instance.ParfumRoomMaskDrawStop += this.OnSpreadFoamStoppedListener;
            ParfumRoomController.instance.ParfumeRoomMaskApply += this.OnLatheredListener;

            ParfumRoomController.instance.ParfumRoomPowder += this.OnPowderListener;
            ParfumRoomController.instance.ParfumRoomPowderStop += this.OnPowderEndListener;
            ParfumRoomController.instance.ParfumeRoomPowdered += this.OnPowderedListenerr;
        }
    }

    private void OnDesable() {
        if (BathRoomController.instance != null) {
            BathRoomController.instance.OnActivateBathRoom -= this.ActivateBathroomInteractionListener;
            BathRoomController.instance.OnDeactivateBathRoom -= this.DeactivateBatroomInteractionListener;
            BathRoomController.instance.OnLathered -= this.OnLatheredListener;
            BathRoomController.instance.OnWasheded -= this.OnWashededListener;
            BathRoomController.instance.OnWiped -= this.OnWipedListener;
            BathRoomController.instance.OnWashFoam -= this.OnWashFoamListener;
            BathRoomController.instance.OnSpreadFoam -= this.OnSpreadFoamListener;
            BathRoomController.instance.OnSpreadFoamStoped -= this.OnSpreadFoamStoppedListener;
            BathRoomController.instance.OnWashFoamStoped -= this.OnWashFoamStoppedListener;
        }

        if (ParfumRoomController.instance != null) {
            ParfumRoomController.instance.ParfumRoomMaskDraw -= this.OnSpreadFoamListener;
            ParfumRoomController.instance.ParfumRoomMaskDrawStop -= this.OnSpreadFoamStoppedListener;
            ParfumRoomController.instance.ParfumeRoomMaskApply -= this.OnLatheredListener;

            ParfumRoomController.instance.ParfumRoomPowder -= this.OnPowderListener;
            ParfumRoomController.instance.ParfumRoomPowderStop -= this.OnPowderEndListener;
            ParfumRoomController.instance.ParfumeRoomPowdered -= this.OnPowderedListenerr;
        }
    }

    // ************ Listeners ***************
    private void ActivateBathroomInteractionListener() {
        if (this.animatorController != null) {
            this.animatorController.PlayScratch();
        }
    }

    private void DeactivateBatroomInteractionListener() {
        
    }

    private void OnLatheredListener() {
        this.animatorController.StopHappyWash();
        if (this.dirty != null) {
            Destroy(this.dirty);
        }
    }

    private void OnWashededListener() {
        if (this.animatorController != null) {
            this.animatorController.Play();
            this.animatorController.StopHappySoap();
        }
    }

    private void OnPowderListener() {
        if (this.animatorController != null) {
            this.animatorController.ContinueHappyWashSoap();
        }
    }

    private void OnPowderEndListener() {
        if (this.animatorController != null) {
            this.animatorController.StopHappyWash();
        }
    }

    private void OnPowderedListenerr() {
        
    }

    private void OnWipedListener() {
    }

    private void OnWashFoamListener() {
        if (!this.washFoam && this.animatorController != null){
            if (this.animatorController.GetAnimator().speed == 0) {
                this.animatorController.Play();
                this.animatorController.PlayHappyWashSoap();
            }
            else {
                this.animatorController.PlayHappyWashSoap();
            }
        }
        this.washFoam = true;
    }

    private void OnWashFoamStoppedListener() {
        if (this.washFoam && this.animatorController != null){
            this.animatorController.Pause();
        }
        this.washFoam = false;
    }

    private void OnSpreadFoamListener() {
        if (!this.spreadFoam && this.animatorController != null) {
            if (this.animatorController.GetAnimator().speed == 0) {
                this.animatorController.ContinueHappyWashSoap();
            }
            else {
                this.animatorController.PlayHappyWash();
            }
        }
        this.spreadFoam = true;
    }

    private void OnSpreadFoamStoppedListener() {
        if (this.spreadFoam && this.animatorController != null){
            this.animatorController.Pause();
        } 
        this.spreadFoam = false;
    }
}
