﻿using System;
using TouchScript;
using TouchScript.Gestures;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class SwipeManager : MonoBehaviour {
    private float deltaX;
    private float deltaY;
    public delegate void SwipeDirection();

    public event SwipeDirection SwipeRight;
    public event SwipeDirection SwipeLeft;
    public event SwipeDirection SwipeUp;
    public event SwipeDirection SwipeDown;
    public static SwipeManager instance;

    [SerializeField]private float swipeSensitivity;
    private void OnEnable() {
        if (instance == null) instance = this;
        if (TouchManager.Instance == null) return;
        TouchManager.Instance.TouchesBegan += HandleTouchesBegan;
        TouchManager.Instance.TouchesMoved += HandleTouchesMoved;
        TouchManager.Instance.TouchesEnded += HandleTouchesEndet;
    }

    private void OnDisable() {
        if (instance == this) instance = null;
        if (TouchManager.Instance == null) return;
        TouchManager.Instance.TouchesBegan -= HandleTouchesBegan;
        TouchManager.Instance.TouchesMoved -= HandleTouchesMoved;
        TouchManager.Instance.TouchesEnded -= HandleTouchesEndet;
    }

    private void HandleTouchesBegan(object sender, TouchEventArgs e) {
        deltaX = 0;
        deltaY = 0;
    }

    private void HandleTouchesEndet(object sender, TouchEventArgs e) {
        if (deltaX < -this.swipeSensitivity){
            if (this.SwipeLeft != null) {
                this.SwipeLeft();
            }
        }
        else if(deltaX > this.swipeSensitivity){
            if (this.SwipeRight != null) {
                this.SwipeRight();
            }
        }

        if (deltaY < -this.swipeSensitivity) {
            if (this.SwipeDown != null) {
                this.SwipeDown();
            }
        }
        else if(deltaY > this.swipeSensitivity) {
            if (this.SwipeUp != null) {
                this.SwipeUp();
            }
        }
    }

    private void HandleTouchesMoved(object sender, TouchEventArgs e) {
        deltaX += (e.Touches[0].PreviousPosition.x - e.Touches[0].Position.x);
        deltaY += (e.Touches[0].PreviousPosition.y - e.Touches[0].Position.y);
    }
}