﻿using UnityEngine;
using System.Collections;

public class RagController : MonoBehaviour {
    private Animator _animator;
    private bool animationPlay;
    private void Start() {
        this.animationPlay = false;
        this._animator = GetComponent<Animator>();
    }

    private void Update() {
        if (!ChangingRoom.instance.GetInRoom().Equals(ChangingRoom.Rooms.RIGHT) && this.animationPlay) {
            this.ResetAnimation();
        }
    }

    private void OnMouseDown() {
        this.PlayAnimation();
        AudioManager.instance.GetSoundSource()
            .PlayOneShot(AudioManager.instance.GetSoundByName("click"));
    }

    private void PlayAnimation() {
        if (this.animationPlay) return;
        if (this._animator != null) {
            this._animator.SetTrigger("play");
        }
        this.animationPlay = true;
    }

    private void ResetAnimation() {
        if (this._animator != null) {
            this._animator.SetTrigger("reset");
        }
        this.animationPlay = false;
    }
}
