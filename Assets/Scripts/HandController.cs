﻿using UnityEngine;
using System.Collections;

public class HandController : MonoBehaviour {
    private bool move;
    private Vector3 target;
    private Vector3 startPosition = new Vector3(1400f,-2.5f,0f);
    private Vector3 endPosition = new Vector3(635f,-285f,0);
    public static HandController instance;
    private Transform childObject;
    [SerializeField] private GameObject curlingNails;
    [SerializeField] private GameObject nailFile;

    private void OnEnable() {
        instance = this;
    }

    public void BringCurlingNails() {
        if (!this.curlingNails.activeSelf) {
            this.curlingNails.SetActive(true);
        }
        this.curlingNails.transform.SetParent(this.transform);
        this.curlingNails.transform.localPosition = new Vector3(-260f,80f,0f);
        MoveTo(this.endPosition);
        this.childObject = transform.Find("curlingNails");
    }

    public void BringNailFile() {
        if (!this.nailFile.activeSelf) {
            this.nailFile.SetActive(true);
        }
        this.nailFile.transform.SetParent(this.transform);
        this.nailFile.transform.localPosition = new Vector3(-250f, 0.5f, 0f);
        MoveTo(this.endPosition);
        this.childObject = transform.Find("nailFile");
    }

    private void Update() {
        if (this.childObject != null && !this.childObject.parent.Equals(this.transform) && !this.target.Equals(startPosition)) {
            this.MoveToStartPosition();
        }
        if (this.move) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.target,
                2000f*Time.deltaTime);
        }

        if (this.transform.localPosition.Equals(this.target)) {
            this.move = false;
        }
    }

    public void MoveToStartPosition() {
        this.target = this.startPosition;
        this.move = true;
    }

    public void MoveToEndPosition() {
        this.target = this.endPosition;
        this.move = true;
    }

    public void MoveTo(Vector3 v) {
        this.target = v;
        this.move = true;
    }

    public bool IsMoved() {
        return this.move;
    }
}
