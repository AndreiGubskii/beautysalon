﻿using UnityEngine;
using System.Collections;

public class ShoesMouseDown : MonoBehaviour {
    [SerializeField] private GameObject shoes;
    [SerializeField] private int countVisitors;
    [SerializeField] private bool inTurn;
    private BoxCollider boxCollider;

    private void OnEnable() {
        this.boxCollider = GetComponent<BoxCollider>();
        if (this.inTurn) {
            Activate(false);
        }
    }

    private void Update() {
        if (this.inTurn && TurnController.instance.GetCountVisitors() >= this.countVisitors && !this.shoes.activeSelf) {
            Activate(true);
            TurnController.instance.ResetCounterVisitors();
        }
    }

    private void OnMouseDown() {
        InteractionManager.instance.ActivateShoesGame();
        if (this.inTurn) {
            Activate(false);
        }
    }

    private void Activate(bool activate) {
        this.shoes.SetActive(activate);
        this.boxCollider.enabled = activate;
    }
}
