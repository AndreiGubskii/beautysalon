﻿using UnityEngine;
using System.Collections;

public class PairController : MonoBehaviour {
    [System.Serializable]
    public class Position {
        public Vector3 position;
        public Vector3 rotation;
        public bool isBusy;
    }

    [System.Serializable]
    public class SidePosition {
        public Position right;
        public Position left;
    }

    [SerializeField] private SidePosition sidePosition;
    private ShoesController.ShoesType type;
    private bool complate;

    private void OnEnable() {
        this.complate = false;
        this.GetSidePosition().left.isBusy = false;
        this.GetSidePosition().right.isBusy = false;
    }

    private void Update() {
        if (!this.complate && (this.GetSidePosition().left.isBusy && this.GetSidePosition().right.isBusy)) {
            this.complate = true;
            ShoesPutInPlaceController.instance.Complate();
            if (!InteractionManager.instance.GetInteractionWas()) {
                InteractionManager.instance.SetInteractionWas(true);
            }
        }
    }

    public SidePosition GetSidePosition() {
        return this.sidePosition;
    }

    public ShoesController.ShoesType GetPairType() {
        return this.type;
    }

    public void SetPairType(ShoesController.ShoesType type) {
        this.type = type;
    }

    public void SetPairSideRightIsBusy(bool b) {
        this.GetSidePosition().right.isBusy = b;
    }

    public void SetPairSideLeftIsBusy(bool b) {
        this.GetSidePosition().left.isBusy = b;
    }
}
