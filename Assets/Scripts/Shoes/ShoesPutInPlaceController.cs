﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ShoesPutInPlaceController : MonoBehaviour {
    [System.Serializable]
    private class ShoesPosition {
        public Vector3 position = new Vector3();
        public Vector3 rotation = new Vector3();
    }

    public static ShoesPutInPlaceController instance;
    [SerializeField] private SpriteRenderer back;
    [SerializeField] private GameObject[] shoesObjects;
    [SerializeField] private List<ShoesPosition> shoesTransform;
    private List<ShoesPosition> shoesTransformClone;
    private int complateCounter;
    private bool complate;
    private void OnEnable() {
        if (Helper.GetAspectRatio(new Vector2(Screen.width, Screen.height)) == "16:10") {
            transform.localPosition = new Vector3(-0.4f, transform.localPosition.y, transform.localPosition.z);
            transform.localScale = new Vector3(0.9f, 0.93f, transform.localScale.z);
        }
        if (SystemInfo.deviceModel.Contains("iPad")){
            transform.localPosition = new Vector3(-1f, -3.9f, transform.localPosition.z);
            transform.localScale = new Vector3(0.75f, 0.75f, transform.localScale.z);
        }
        
        this.back.sprite = Resources.Load<Sprite>("Art/Shoes/Salon_shoes_fon");
        instance = this;
        this.complate = false;
        this.complateCounter = 0; 
        this.shoesTransformClone = new List<ShoesPosition>(this.shoesTransform);
        foreach (GameObject shoes in this.shoesObjects) {
            int i = Random.Range(0, shoesTransformClone.Count+1);
            if (i > 0) {
                i--;
            }
            shoes.GetComponent<ShoesController>().SetTarget(this.shoesTransformClone[i].position);
            shoes.transform.localPosition = this.shoesTransformClone[i].position;
            shoes.transform.rotation = Quaternion.Euler(this.shoesTransformClone[i].rotation);
            shoesTransformClone.RemoveAt(i);
        }

    }

    private void OnDisable() {
        back.sprite = null;

        Resources.UnloadUnusedAssets();
    }

    private void Update() {
        if (this.complateCounter == 4 && !this.complate) {
            this.complate = true;
            InteractionManager.instance.DeactivateShoesGame();
        }
    }

    public void Complate() {
        this.complateCounter ++;
    }

    public ShoesController GetShoes(ShoesController.ShoesType shoesType,ShoesController.ShoesSide shoesSide) {
        foreach (GameObject shoes in shoesObjects) {
            ShoesController shoesController = shoes.GetComponent<ShoesController>();
            if (shoesController.GetShoesType().Equals(shoesType) &&
                shoesController.GetShoesSide().Equals(shoesSide)) {
                return shoesController;
            }
        }
        return null;
    }
}
