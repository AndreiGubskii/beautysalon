﻿using UnityEngine;
using System.Collections;

public class ShoesController : MonoBehaviour {
    public enum ShoesSide {
        RIGHT,
        LEFT,
        NONE
    }

    public enum ShoesType {
        NONE,
        TYPE_1,
        TYPE_2,
        TYPE_3,
        TYPE_4
    }

    private bool isClamped;
    private PairController pair;
    private Vector3 targetPosition;
    private SpriteRenderer spriteRenderer;
    [SerializeField] private ShoesType type;
    [SerializeField] private ShoesSide side;

    private void OnEnable() {
        if (this.spriteRenderer == null) {
            this.spriteRenderer = this.GetComponent<SpriteRenderer>();
        }
        this.SetInSitu(false);
    }

    private void Update() {
        if (this.isClamped){
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f)){
                transform.position = new Vector3(hit.point.x, hit.point.y, 0f);
            }
        }

        if (!this.isClamped && this.transform.localPosition!=this.targetPosition) {
            this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, this.targetPosition,20f*Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider col) {
        this.pair = col.GetComponent<PairController>();
        if (null != this.pair) {
            if (this.pair.GetPairType().Equals(ShoesType.NONE) && 
                !ShoesPutInPlaceController.instance.GetShoes(this.GetShoesType(), GetSideCouple()).GetInSitu()) {
                this.pair.SetPairType(this.GetShoesType());
            }
        }
    }

    private ShoesSide GetSideCouple() {
        ShoesSide side = ShoesSide.NONE;
        if (this.side.Equals(ShoesSide.RIGHT)) {
            side = ShoesSide.LEFT;
        }
        else if (this.side.Equals(ShoesSide.LEFT)) {
            side = ShoesSide.RIGHT;
        }
        return side;
    }

    private void OnTriggerExit(Collider col) {
        if (col.GetComponent<PairController>()==null)return;
        if (this.pair != null && !this.pair.GetSidePosition().left.isBusy && !this.pair.GetSidePosition().right.isBusy) {
            this.pair.SetPairType(ShoesType.NONE);
        }
    }

    public ShoesType GetShoesType() {
        return this.type;
    }

    public ShoesSide GetShoesSide() {
        return this.side;
    }

    public void SetTarget(Vector3 v) {
        this.targetPosition = v;
    }

    private void OnMouseDown() {
        this.isClamped = true;
        this.spriteRenderer.sortingOrder++;
    }

    private bool inSitu;

    public bool GetInSitu() {
        return this.inSitu;
    }

    public void SetInSitu(bool b) {
        this.inSitu = b;
    }

    private void OnMouseUp() {
        this.isClamped = false;
        this.spriteRenderer.sortingOrder--;
        if (this.pair != null && this.pair.GetPairType().Equals(this.GetShoesType())) {
            if (!this.pair.GetSidePosition().left.isBusy && this.side.Equals(ShoesSide.LEFT)) {
                this.pair.SetPairSideLeftIsBusy(true);
                this.targetPosition = this.pair.GetSidePosition().left.position;
                this.transform.rotation = Quaternion.Euler(this.pair.GetSidePosition().left.rotation);
                this.SetInSitu(true);
            }
            if (!this.pair.GetSidePosition().right.isBusy && this.side.Equals(ShoesSide.RIGHT)) {
                this.pair.SetPairSideRightIsBusy(true);
                this.targetPosition = this.pair.GetSidePosition().right.position;
                this.transform.rotation = Quaternion.Euler(this.pair.GetSidePosition().right.rotation);
                this.SetInSitu(true);
            }
            AudioManager.instance.GetSoundSource()
                .PlayOneShot(AudioManager.instance.GetSoundByName("shoesclup"));
        }
    }

}
