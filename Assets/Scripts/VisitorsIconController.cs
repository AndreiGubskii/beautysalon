﻿using UnityEngine;
using System.Collections;

public class VisitorsIconController : MonoBehaviour {
    private GameObject bell;
    private bool rightRoom, topRoom;
    private void OnEnable() {
        if (bell == null) {
            this.bell = transform.Find("bell").gameObject;
        }
        if (GetComponent<BoxCollider>() == null) {
            this.gameObject.AddComponent<BoxCollider>().size = new Vector3(2.5f,2.5f,1f);
        }
        this.bell.transform.localPosition = new Vector3(-0.6f,-0.6f,0f);
        this.bell.SetActive(false);
        StartCoroutine(HideIconIenumerator());
    }

    public void ShowBell() {
        this.bell.SetActive(true);
    }

    private void OnDisable() {
        this.bell.SetActive(false);
    }

    public void SetRoom(bool right,bool top) {
        this.rightRoom = right;
        this.topRoom = top;
    }

    private void OnMouseDown() {
        if(InteractionManager.instance.InteractionActivated()) return;
        if (this.bell.activeSelf) {
            ChangingRoom.instance.MoveToWaitingRoom();
        }
        else {
            if (this.rightRoom && !this.topRoom) {
                ChangingRoom.instance.MoveToRightRoom();
            }else if (!this.rightRoom && !this.topRoom) {
                ChangingRoom.instance.MoveToLeftRoom();
            }else if (this.topRoom) {
                ChangingRoom.instance.MoveToTopRoom();
            }
        }
    }

    private IEnumerator HideIconIenumerator() {
        yield return new WaitForSeconds(1.5f);
        gameObject.SetActive(false);
    }
}
