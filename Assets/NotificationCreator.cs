﻿using System;
using UnityEngine;


public class NotificationCreator : MonoBehaviour {
    [Serializable] public class Notifications {
        public Notifications() {
        }

        public Notifications(string title, string message) {
            this.title = title;
            this.message = message;
        }

        public string title;
        public string message;
    }

    private Notifications notification = new Notifications();

    private void Start() {
        switch (Application.systemLanguage) {
            case SystemLanguage.Russian:
                notification = new Notifications("Мне скучно", "Пойдем, поиграем дружочек?");
                break;
            case SystemLanguage.Ukrainian:
                notification = new Notifications("Мені нудно", "Підемо, пограємо любий?");
                break;
            default:
                notification = new Notifications("Мне скучно", "Пойдем, поиграем дружочек?");
                break;
        }
        LocalNotification.CancelNotification(2);
    }
    void OnApplicationPause(bool pauseStatus){
        if (!pauseStatus) {
            LocalNotification.CancelNotification(2);
        }
        else {
            StartRepeatingNotification(DateTime.Now, notification.title,notification.message);
        }
            
    }
    
    public void StartRepeatingNotification(DateTime date,string title,string message) {
        long delayRepeaat = ((date.Ticks + 600000000*1440L) - DateTime.Now.Ticks) / 10000000;
        LocalNotification.SendRepeatingNotification(2, delayRepeaat , delayRepeaat, title,message, new Color32(255, 255, 255, 255));
    }
}
